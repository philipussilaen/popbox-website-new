<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    // set table
    protected $table = 'tb_provinces';

    /* Relationship */
    public function cities(){
        return $this->hasMany(City::class,'id','provinces_id');
    }
}
