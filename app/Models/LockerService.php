<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LockerService extends Model
{
    // set table
    protected $table = 'locker_services';

    /*Relationship*/
    public function locker(){
        return $this->belongsTo(Locker::class,'locker_id','locker_id');
    }
}
