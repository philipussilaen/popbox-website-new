<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VirtualLockers extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'lockers';
    use SoftDeletes;

    public static function getVirtualLocker(){
        $data = self::join('cities','cities.id','=','lockers.cities_id')
            ->join('provinces','provinces.id','=','cities.provinces_id')
            ->where('type','agent')
            ->get();
        return $data;
    }
}
