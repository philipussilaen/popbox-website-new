<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Merchant extends Model
{
    protected $table = 'tb_website_merchants';
    protected $primaryKey = 'id_merchant';

    public static function getMerchantIndex($region=null){
        $data = self::take(12)->whereNotNull('order')->orderBy('order','asc')->get();
        return $data;
    }

    public static function getOnDemandPage(){
        $data = Cache::remember('onDemandMerchant',1440,function (){
           return self::all();
        });
        return $data;
    }
}
