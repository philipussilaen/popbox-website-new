<?php
/**
 * Created by PhpStorm.
 * User: Arief Demiawansyah
 * Date: 29/11/2016
 * Time: 11.36
 */

namespace App\Http\Controllers;

use App\Http\Helper\API;
use App\Http\Helper\Locker;
use Illuminate\Http\Request;
use App\Http\Helper\Helper;
use \App\Library\SiteHelpers;
use App\Models\Merchantpickup;
use App\Models\Merchantreturn;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Mail;
use DB;
use Redirect;
use Session;
use DNS2D;
use App\City;
use Input;


class MerchantpickupController extends Controller
{
	protected $data = array();	
	public $module = 'merchantpickup';
	static $per_page	= '10';

	public function __construct(Request $request){

	
		if(!\Auth::check()) {			
            Redirect::to('')->send();
        } 
        $this->user = \Auth::user();
        $this->email = $this->user->email;
        $this->active = $this->user->active;        

        $path = $request->path();
        $this->merchant = DB::table("tb_merchant_users")->where("email", $this->email)->first();  

		$this->model = new Merchantpickup();

		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'merchantpickup',
			'merchant' =>  $this->merchant,
			'user' => $this->user,
			'return'	=> self::returnUrl()			
		);
		
		
          	
	}

	public function index(Request $request){   

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id_pickup'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );	

		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('merchantpickup');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		 $this->data['tableGrid'] 	= $this->info['config']['grid'];
		 $this->data['tableForm'] 	= $this->info['config']['forms'];
		 $this->data['colspan'] 		= SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		 $this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
	

		// $merchant_id = session()->get('gid');
		$merchant_id = \Auth::user()->group_id;		
		$merchant_query="";

		$sqlmerch = "Select name, description from tb_groups where group_id='".$merchant_id."'";
		$rsmerch = DB::select($sqlmerch);
		$merchant_name = trim($rsmerch[0]->name);
		$posno = trim($rsmerch[0]->description);


		$ckpref = strpos($posno, 'Prefix:');
		$cekuser = strpos($posno, 'Username:');
		$cekmname = strpos($posno, 'Merchantname:');

		do {
			if ($ckpref != 0) {
				$pref = substr($posno, strpos($posno, "Prefix:") + 7);   
				$merchant_query = " tb_merchant_pickup.order_number like '".$pref."%' and ";
				break;
			} 

			if ($cekuser != 0) {
				$username = substr($posno, strpos($posno, "Username:") + 9);   
				$merchant_query = " locker_activities.name like '".$username."%' and ";
				break;
			} 

			if ($cekmname != 0) {
				$merchantname = substr($posno, strpos($posno, "Merchantname:") + 13);
				$merchant_query = "tb_merchant_pickup.merchant_name like '".$merchantname."%' and  ";
				break;  
			}
 		} while (false);
	
		//total need store
		$sqlns =  "SELECT tb_merchant_pickup.order_number FROM tb_merchant_pickup 
		           LEFT JOIN locker_activities ON tb_merchant_pickup.order_number=locker_activities.barcode 
		           where ".$merchant_query." locker_activities.status is NULL ";
		$ns=DB::select($sqlns);
		$this->data['nstotal'] = count($ns);

		//total in store
		$sqlis = "SELECT tb_merchant_pickup.order_number
				    FROM tb_merchant_pickup 
		            LEFT JOIN locker_activities ON tb_merchant_pickup.order_number=locker_activities.barcode
		            where ".$merchant_query." locker_activities.status='IN_STORE'
		            ";
		$is=DB::select($sqlis);
		$this->data['istotal'] = count($is);      

		//total courier taken
		$sqlct = "SELECT tb_merchant_pickup.order_number 
				    FROM tb_merchant_pickup 
		            LEFT JOIN locker_activities ON tb_merchant_pickup.order_number=locker_activities.barcode
		            where ".$merchant_query." locker_activities.status='COURIER_TAKEN'
		            ";
		$ct=DB::select($sqlct);
		$this->data['cttotal'] = count($ct);   

		//total customer taken
		$sqlcm = "SELECT tb_merchant_pickup.order_number
				    FROM tb_merchant_pickup 
		            LEFT JOIN locker_activities ON tb_merchant_pickup.order_number=locker_activities.barcode
		            where ".$merchant_query." locker_activities.status='CUSTOMER_TAKEN'
		            ";
		$cm=DB::select($sqlcm);
		$this->data['cmtotal'] = count($cm);   

		//total home delivery
		$sqlhd = "select order_number from tb_merchant_pickup where ".$merchant_query." status='HOME_DELIVERY'";
		$hd=DB::select($sqlhd);
		$this->data['hdtotal'] = count($hd);   
 		
        return view('dashboard.pickup' ,$this->data);
    }

     public function filter( Request $request){      		
	        $module = $this->module;
	        $sort 	= (!is_null($request->input('sort')) ? $request->input('sort') : '');
	        $order 	= (!is_null($request->input('order')) ? $request->input('order') : '');
	        $rows 	= (!is_null($request->input('rows')) ? $request->input('rows') : '');
	        $md 	= (!is_null($request->input('md')) ? $request->input('md') : '');        
	        $filter = '?';
	        if($sort!='') $filter .= '&sort='.$sort;
	        if($order!='') $filter .= '&order='.$order;
	        if($rows!='') $filter .= '&rows='.$rows;
	        if($md !='') $filter .= '&md='.$md;
	        return Redirect::to("dashboards/".$this->data['pageModule'] . $filter);
	   }

    public function search(){
        $this->data['tableForm'] 	= $this->info['config']['forms'];
        $this->data['tableGrid'] 	= $this->info['config']['grid'];        
        $this->data['pageUrl']		= url("/dashboards/".$this->module);                
        return view('sximo.module.utility.search',$this->data);

    }

    public function myshop(Request $request){    					
		return view('dashboard.myshop', $this->data);    	
    }

    public function myshopupdate(Request $request){
    	$rules = [
		    'owner_name' => 'required',
		    'address' => 'required',		    
		    'website' => 'required',		    
		    'address' => 'required'
		];		

		if (empty($this->merchant)){
			$rules["merchant_name"] = 'required|unique:tb_merchant_users';
			$rules['time'] = 'required';
		}
    	$this->validate($request, $rules);
    	$merchant_name = $request->input("merchant_name");
    	$ownerName = $request->input("owner_name");
    	$email =  $this->user->email;
    	$update = array(
    		"owner_name" => $ownerName,    		
    		"phone" => $request->input("phone",""),
    		"city" => $request->input("city",""),
    		"address" => $request->input("address",""),
    	);
    	
    	if ($request->file('picture')){
    		$file = $request->file('picture');
    		$destinationPath = 'uploads/logomerchant';
    		$file->move($destinationPath, $file->getClientOriginalName());	
    		$update["picture"] = $file->getClientOriginalName();    
    	}

    	$timepickup = "";    	
        if ( $request->input("time") ){
            $timepickup = $request->input("time");            
        }        
    	if (empty($this->merchant)){
    		if ($timepickup!=""){
	        	$update['time_pickup'] = $timepickup; 
	    	}
    		$update["merchant_name"] = $merchant_name;
    		$update["detail_address"] = $request->input("detail_address");
    		$update["website"] = $request->input("website");
    		$update["city"] = $request->input("city");
    		$update["email"] = $this->email;
            $email2 = config('config.email_merchant_reg');
            if (isset($this->active)){
	            if ($this->active=="2"){
		            $note = "CALON MERCHANT TELAH AKTIF";            
		            \Mail::send('user.emails.merchant-report-activation', ['data' => $update, "note" => $note], function ($m) use ($ownerName, $email2, $merchant_name) {
		                $m->from('no-reply@popbox.asia', 'PopBox Asia');
		                $m->to($email2, $ownerName)->subject("[PopBox] Pendaftaran Merchant Baru: ".$merchant_name);
		            }); 
	            }else{
	            	$username =  $this->user->username;
	            	$code =  $this->user->activation;
	            	$this->data['name'] = $username;            
			        $this->data['url'] = config('config.url')."/user/activation?code=".$code;
	        		Mail::send('user.emails.user-activate', ["data"=> $this->data], function ($m) use ($username,$email) {
	                	$m->from('no-reply@popbox.asia', 'PopBox Asia');
	                	$m->to($email, $username)->subject("[PopBox] Aktivasi user Anda: $username");
	        		}); 
	            }
            }else{
            	$username =  $this->user->username;
            	$code =  $this->user->activation;
            	$this->data['name'] = $username;            
		        $this->data['url'] = config('config.url')."/user/activation?code=".$code;
        		Mail::send('user.emails.user-activate', ["data"=> $this->data], function ($m) use ($username,$email) {
                	$m->from('no-reply@popbox.asia', 'PopBox Asia');
                	$m->to($email, $username)->subject("[PopBox] Aktivasi user Anda: $username");
        		}); 

            }
            $merchant_user = 
				\DB::table("tb_merchant_users")
				->insert($update);			

    	}else{
    		if ($timepickup!=""){
	        	$update['time_pickup_new'] = $timepickup; 
	    	}
			$merchant_user = 
				\DB::table("tb_merchant_users")
				->where("email", $email)
				->update($update);
		}
		return redirect('dashboards/myshop')->with('status', 'Profile berhasil di updated!');
    }

    public function testemail(Request $request){		
    	$update = array(
    		"owner_name" => "ownerName",    		
    		"phone" => "phone",
    		"city" => "city",
    		"address" => "address",
    	);
    	$ownerName = "ownerName";
    	$merchant_name ="merchant_name";
    	$update["merchant_name"] = "merchant_name";
    	$update["detail_address"] = "detail_address";
    	$update["website"] = "website";
    	$update["city"] = "city";
    	$update["email"] = "email";    			
    	$update["timepickup"] ="timepickup";
		$email2 = config('config.email_merchant_reg');
		$note = "adfa";
        Mail::send('user.emails.merchant-report-activation', ['data' => $update, "note" => $note], function ($m) use ($ownerName, $email2, $merchant_name) {
            $m->from('no-reply@popbox.asia', 'PopBox Asia');
            $m->to($email2, $ownerName)->subject("[PopBox] Pendaftaran Merchant Baru: ".$merchant_name);
        });   
        $data["data"] = $update;
        $data["note"] = "MERCHANT TELAH AKTIF";
        return view("user.emails.merchant-report-activation",$data );
	}


    public function myshopedit(Request $request){
    	$locker = Helper::getLockerbyCities("id");              
        $cityDb = City::whereIn("city_name",['bandung','bogor','bekasi','depok','tangerang'])
                        ->orWhere(function ($query) {
                              $query->where('city_name', 'like', '%jakarta%');
                        })->get();        

        $province = array();
        $citiesList = array();

        foreach ($cityDb as $Map)
        {
            if (!isset($province[$Map->province->province_name]))
            {
                $i = count($province);
                $province[$Map->province->province_name] = $i;
            }
            $i = $province[$Map->province->province_name];

            $citiesList[$i]['Province'] = utf8_encode($Map->province->province_name);
            $citiesList[$i]['Cities'][] = $Map->city_name;
        }

        // parsing data
        $this->data['citiesList'] = $citiesList;
		$this->data["email"] = $this->email;
		return view('dashboard.myshopedit', $this->data);
    }

    public function tracking(Request $request){
    	return view('dashboard.tracking', $this->data);    		
    }

    public function panduan(Request $request){
    	return view('dashboard.panduan', $this->data);    			
    }


    public function download( Request $request){

        if($this->access['is_excel'] ==0)
            return Redirect::to('')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');

        $info = $this->model->makeInfo( $this->module);
        // Take param master detail if any
        $filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');
        $params = array(
            'params'	=> $filter,
            'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
        );

        $results 	= $this->model->getRows( $params );
        $fields		= $info['config']['grid'];
        $rows		= $results['rows'];

        $content = $this->data['pageTitle'];
        $content .= '<table border="1">';
        $content .= '<tr>';
        foreach($fields as $f )
        {
            if($f['download'] =='1')
            {
                $limited = isset($field['limited']) ? $field['limited'] :'';
                if(SiteHelpers::filterColumn($limited ))
                {
                    $content .= '<th style="background:#f9f9f9;">'. $f['label'] . '</th>';

                }
            }
        }
        $content .= '</tr>';

        foreach ($rows as $row)
        {
            $content .= '<tr>';
            foreach($fields as $f )
            {

                if($f['download'] =='1')
                {
                    $limited = isset($field['limited']) ? $field['limited'] :'';
                    if(SiteHelpers::filterColumn($limited ))
                    {
                        $content .= '<td> '. SiteHelpers::formatRows($row->$f['field'],$f,$row) . '</td>';
                    }
                }

            }
            $content .= '</tr>';
        }
        $content .= '</table>';

        @header('Content-Type: application/ms-excel');
        @header('Content-Length: '.strlen($content));
        @header('Content-disposition: inline; filename="'.$title.' '.date("d/m/Y").'.xls"');

        echo $content;
        exit;

    }

	public function uploadpickup(Request $request){				
		return view('dashboard.uploadpickup',$this->data);
	}

	public function uploadpickupsingle(Request $request){		
		
		$merchant_user = $this->merchant;

		$prefix = "";
		$address = "";
		if (isset($merchant_user)){
			$prefix = $merchant_user->prefix;
			$address = $merchant_user->address."\n".$merchant_user->detail_address;
		}
		$this->data["prefix"] = $prefix;
		
		$locker = Helper::getLockerbyCities("id");              


        $this->data["lockers"] = $locker["lockers"];
        $this->data["lockerBycities"] = $locker["lockerBycities"];

        $lockerJkt = array();
        $lockerList = array();

        $this->data['lockerJkt'] = $lockerJkt;

        $set = 'Indonesia';

        $locker = Locker::getAllLocker($set);

        if ($locker) $lockerList = $locker;

        foreach ($lockerList as $index=>$locker){
            $lockerJkt[$index]['name'] = $locker->name;
            $lockerJkt[$index]['address'] = $locker->address;
            $lockerJkt[$index]['address_2'] = $locker->address_2;
            $lockerJkt[$index]['city'] =  $locker->district;
            $lockerJkt[$index]['province'] =  $locker->province;
            $lockerJkt[$index]['latitude'] =  $locker->latitude;
            $lockerJkt[$index]['longitude'] =  $locker->longitude;
            $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
        }
        $this->data['lockerJkt'] = $lockerJkt;
        $this->data["address"] = $address;
		return view('dashboard.uploadpickupsingle', $this->data);
	}

	public function postpickup(Request $request){
		if (empty($this->merchant)){
			Session::flash('error', "Please lengkapi data merchant anda");
			return redirect('dashboards/uploadpickup');
		}

		if ($this->user->active == 2){
			Session::flash('error', "Terima kasih telah mengikuti proses pendaftaran. Tim kami akan segera menghubungi anda untuk mengatifkan account anda, dan anda bisa melakukan pickup data");
			return redirect('dashboards/uploadpickup');				
		}
		
		$this->validate($request, [
		    'phone' => 'required|min:5',
		    'popbox_location' => 'required',
		    'pickup_location' => 'required',
		    'customer_name' => 'required',
		    'price' => 'numeric',
		]);
		
		$customer_name = $request->input("customer_name","");
		$customer_phone = $request->input("phone","");
		$customer_email = $request->input("customer_email","");
		$popbox_location = $request->input("popbox_location","");
		$pickup_location = $request->input("pickup_location","");
		$detail_items = $request->input("detail_items","");
		$price = $request->input("price","");	
		$weight = $request->input("weight","1");	
		
		$merchant = $this->merchant;		
		$merchant_name = $this->merchant->merchant_name;		
		$order_number = SiteHelpers::rand_sha1();
		
		$cekpref = substr($customer_phone,0, 3);
		if ($cekpref == "+62") {
			$customer_phone = "0".substr($customer_phone, 3);
		}	

        // $pref = substr($order_number, 0, 3);

		$ins = [
        	    'merchant_name' => $merchant_name,
                'order_number' => $order_number,
                'customer_name' => $customer_name,
                'customer_phone' => $customer_phone,
                'popbox_location'  => $popbox_location,
                'pickup_location' => $pickup_location, 
                'customer_email' => $customer_email,
                'detail_items' => $detail_items,
                'price' => $price,
                'last_update' => date("Y-m-d H:i:s")
            ];
       	DB::table('tb_merchant_pickup')->insert($ins);
		self::sendEmailAndLocker($ins, $merchant, $order_number);
		Session::flash('success', "Order Anda berhasil disimpan, mohon cetak label dan tempelkan pada paket");
		return redirect('/dashboards/pickupdetail?order_no='. $order_number);
	}

	public function uploadpickupxls(Request $request){		

		if (empty($this->merchant)){
			Session::flash('error', "Please lengkapi data merchant anda");
			return redirect('dashboards/uploadpickup');
		}

		if ($this->user->active == 2){
			Session::flash('error', "Terima kasih telah mengikuti proses pendaftaran. Tim kami akan segera menghubungi anda untuk mengatifkan account anda, dan anda bisa melakukan pickup data");
			return redirect('dashboards/uploadpickup');
		}	

		if($request->hasFile('import_file')){
			$file = $request->file('import_file');
	        $path = $file->getRealPath();	        	        
	        $data = \Excel::load($path, function($reader) {})->get();

			if(!empty($data) && $data->count()){				
				$email = $this->user->email;
				
				$merchant = $this->merchant;
				$merchant_name = $merchant->merchant_name;				

				$insert = [];
				$isEmptyPhone = false;				
				foreach ($data as $key => $value) {
					if (!empty($value->customer_name)){
						if (strpos($value->customer_phone, 'xxx') !== false)  {						
						}else if (empty($value->customer_phone)){
							$isEmptyPhone = true;
						}else{
							$ins = [
								'merchant_name' => $merchant_name,		
								'order_number' => '',
					            'customer_name' => $value->customer_name,
					            'customer_phone' => $value->customer_phone,
					            'popbox_location'  => $value->popbox_location,
					            'pickup_location' => $value->pickup_location, 
					            'customer_email' => $value->customer_email,
					            'detail_items' => $value->detail_items,
					            'weight' => $value->weight,
				            	'price' => $value->price,
					            'last_update' => date("Y-m-d H:i:s")
							];							
							// DB::table('tb_merchant_pickup')->insert($ins);						
							$insert[] =$ins;						
						}
					}
					
				}
				if ($isEmptyPhone){
					return back()->with('error','File upload terdapat no handphone yang tidak boleh kosong');
				}else				
					foreach ($insert as $key => $ins) {						
						$ins['order_number'] = SiteHelpers::rand_sha1();
						$insert[$key]['order_number'] = $ins['order_number'];
						DB::table('tb_merchant_pickup')->insert($ins);						
					}
					self::sendEmailAndLockerArray($insert, $merchant);
					return back()->with('success','Order Anda berhasil disimpan, mohon cetak label dan tempelkan pada paket. Silakan melihat menu "DATA ORDER" untuk melihat kembali datanya.');
				}
			}else{
				return back()->with('error','Please Check your file, Something is wrong there.');
			}
	    }     	

	

	public function listuploadpickup(Request $request){
		$email = $this->user->email;						
		$this->data["rowData"] = array();
		$status = $request->input("status","");
		
		$arrayWhere 	= array();
		$strWhere 		= "";
		$strValueWhere 	= "";
		if ($request->input("order_number"))
			$arrayWhere["MP.order_number"] 		= "%".trim($request->input("order_number"))."%";
		if ($request->input("customer_name"))
			$arrayWhere["MP.customer_name"] 	= "%".trim($request->input("customer_name"))."%";	
		if ($request->input("customer_phone"))
			$arrayWhere["MP.phone"] 			= "%".trim($request->input("phone"))."%";
		if ($request->input("customer_email"))
			$arrayWhere["MP.customer_email"] 	= "%".trim($request->input("customer_email"))."%";
		if ($request->input("status")){
			if ($status!="1"){
				$arrayWhere["MP.status"] 			= "%".self::getStatus(trim($request->input("status")))."%";
			}
		}
		if (count($arrayWhere)>0){
			$strWhere = " AND ".implode(" like ? AND ", array_keys($arrayWhere))." like ?";		
			$strValueWhere = implode(',', $arrayWhere);								
		}			

		if (isset($this->merchant)){				
			$sql_all_count = "SELECT count(*) as total FROM `tb_merchant_pickup` MP left join locker_activities LA on MP.order_number=LA.barcode where MP.merchant_name=?";
			$sql_need_store_count = "SELECT count(*) as total FROM `tb_merchant_pickup` MP left join locker_activities LA on MP.order_number=LA.barcode where MP.merchant_name=? AND LA.status IS NULL";
			$sql_in_store_count = "SELECT count(*) as total FROM `tb_merchant_pickup` MP left join locker_activities LA on MP.order_number=LA.barcode where MP.merchant_name=? AND LA.status = 'IN_STORE'";
			$sql_courier_taken_count = "SELECT count(*) as total FROM `tb_merchant_pickup` MP left join locker_activities LA on MP.order_number=LA.barcode where MP.merchant_name=? AND LA.status = 'COURIER_TAKEN'";
			$sql_operator_taken_count = "SELECT count(*) as total FROM `tb_merchant_pickup` MP left join locker_activities LA on MP.order_number=LA.barcode where MP.merchant_name=? AND LA.status='OPERATOR_TAKEN'";
			$sql_customer_taken_count = "SELECT count(*) as total FROM `tb_merchant_pickup` MP left join locker_activities LA on MP.order_number=LA.barcode where MP.merchant_name=? AND LA.status='CUSTOMER_TAKEN'";			
			$arr = array(trim($this->merchant->merchant_name));
			if (count($arrayWhere)>0){
				$arr = array_merge(array(trim($this->merchant->merchant_name)), array_values($arrayWhere));			
			}
			$this->data["count_all"] = DB::select($sql_all_count, [trim($this->merchant->merchant_name)])[0]->total;						
			$this->data["count_need_store"] = DB::select($sql_need_store_count , [trim($this->merchant->merchant_name)] )[0]->total;
			$this->data["count_in_store"] = DB::select($sql_in_store_count , [trim($this->merchant->merchant_name)])[0]->total;
			$this->data["count_courier_taken"] = DB::select($sql_courier_taken_count , [trim($this->merchant->merchant_name)] )[0]->total;
			$this->data["count_operator_taken"] = DB::select($sql_operator_taken_count , [trim($this->merchant->merchant_name)] )[0]->total;
			$this->data["count_customer_taken"] = DB::select($sql_customer_taken_count , [trim($this->merchant->merchant_name)] )[0]->total;
			
			if ($status=="1"){	
				$sql = "SELECT MP.*, IF(LA.status IS NULL, 'need store', LA.status) status_locker FROM `tb_merchant_pickup` MP left join locker_activities LA on MP.order_number=LA.barcode where MP.merchant_name=? and LA.status IS NULL".$strWhere;					
			}else if ($status=="2"){	
				$sql = "SELECT MP.*, IF(LA.status IS NULL, 'need store', LA.status) status_locker FROM `tb_merchant_pickup` MP left join locker_activities LA on MP.order_number=LA.barcode where MP.merchant_name=? ".$strWhere;					
			}else if ($status=="3"){	
				$sql = "SELECT MP.*, IF(LA.status IS NULL, 'need store', LA.status) status_locker FROM `tb_merchant_pickup` MP left join locker_activities LA on MP.order_number=LA.barcode where MP.merchant_name=? ".$strWhere;					
			}else if ($status=="4"){	
				$sql = "SELECT MP.*, IF(LA.status IS NULL, 'need store', LA.status) status_locker FROM `tb_merchant_pickup` MP left join locker_activities LA on MP.order_number=LA.barcode where MP.merchant_name=? ".$strWhere;					
			}else if ($status=="5"){	
				$sql = "SELECT MP.*, IF(LA.status IS NULL, 'need store', LA.status) status_locker FROM `tb_merchant_pickup` MP left join locker_activities LA on MP.order_number=LA.barcode where MP.merchant_name=? ".$strWhere;
			}else{
				$sql = "SELECT MP.*, IF(LA.status IS NULL, 'need store', LA.status) status_locker FROM `tb_merchant_pickup` MP left join locker_activities LA on MP.order_number=LA.barcode where MP.merchant_name=?".$strWhere;	
			}
			 // echo $sql;
			// print_r($arr);
			// die();
			
			$this->data["rowData"] = DB::select($sql, $arr);			
		}
		
		return view('dashboard.listpickup', $this->data);
	}

	private function sendEmailAndLocker($ins, $merchant, $order_number){
		$urlupload = config('config.api_ebox')."/ebox/api/v1/express/import";
      	//$this->headers[]="userToken:".$api_token;
      	$this->headers[]="userToken:b26499892a72466298b90cb4f82f0c6f";				

		$varupload = [
        	 'expressNumber' => $order_number,
        	 'takeUserPhoneNumber' => $ins["customer_phone"],
        ];


        $varupload = "[".json_encode($varupload)."]";	
        $upres=$this->post_data($urlupload, $varupload, $this->headers);
        DB::table('companies_response')
           	->insert([
                'api_url' => $urlupload,
            	'api_send_data' => $varupload,
                'api_response'  => json_encode($upres), 
                'response_date'     => date("Y-m-d H:i:s")
            ]);	
            
        //ambil data id dari ebox
        $urlget = "http://eboxapi.popbox.asia:8080/ebox/api/v1/express/query?page=0&maxCount=1&expressType=COURIER_STORE&expressStatus=IMPORTED&expressNumber=".  $order_number;

    	$varupid = "";
        $upres2=$this->post_data($urlget, $varupid, $this->headers);

        $id = $upres2["resultList"][0]["id"];

        //post ke newlocker
        $urlupload = "http://pr0x.popbox.asia/express/import";
        $varupload = [
        	            'id' => $id,
        	            'expressNumber' => $order_number,
         				'takeUserPhoneNumber' =>$ins["customer_phone"],
         				'groupName' => ''
         				];
         $varupload = json_encode($varupload);		 
         $upres=$this->post_data($urlupload, $varupload, $this->headers);

	     // insert partner response to database
        DB::table('companies_response')
        	    ->insert([
                	    'api_url' => $urlupload,
                    	'api_send_data' => $varupload,
                        'api_response'  => json_encode($upres), 
                        'response_date'     => date("Y-m-d H:i:s")
          ]);	    
            

        $pref = substr($order_number, 0, 3);
        
        $sqlmaila = "select admin_email from admin_notification where privilege='ALL'";
		$admins = DB::select($sqlmaila);
		$name = "Admin PopBox";
        $tglsubmit = date("Y-m-d H:i:s");
        $merchant_name = $ins["merchant_name"];
        $popbox_location = $ins["popbox_location"];
        $pickup_location = $ins["pickup_location"];
        $customer_email = $ins["customer_email"];
        $jdlmail = "New Merchant Pickup Request Baru from ".$merchant_name." on ".$tglsubmit ;
	    foreach ($admins as $admin ) {
            $email=$admin->admin_email;
			if ($merchant_name != 'Administrator') {
            	\Mail::send('user.emails.merchantpu', ["data"=> $merchant, "pickup" => $ins], function ($m) use($email,$name,$jdlmail){
					$m->from('admin@popbox.asia', 'Admin PopBox');
					$m->to($email, $name)->subject($jdlmail);					
				});	
			}
		}
	}

	private function sendEmailAndLockerArray($insArr, $merchant){				
			
		foreach ($insArr as $key => $ins) {
			$urlupload = config('config.api_ebox')."/ebox/api/v1/express/import";
      	//$this->headers[]="userToken:".$api_token;
	      	$this->headers[]="userToken:b26499892a72466298b90cb4f82f0c6f";		
			

			$customer_phone = $ins["customer_phone"];
		    $pref = substr( $ins["order_number"], 0, 3);

			$varupload = [
	        	 'expressNumber' => $ins["order_number"],
	        	 'takeUserPhoneNumber' => $customer_phone,
	        ];
	        $varupload = "[".json_encode($varupload)."]";	
	        $upres=$this->post_data($urlupload, $varupload, $this->headers);
	        DB::table('companies_response')
	           	->insert([
	                'api_url' => $urlupload,
	            	'api_send_data' => $varupload,
	                'api_response'  => json_encode($upres), 
	                'response_date'     => date("Y-m-d H:i:s")
	            ]);	
	       
		//ambil data id dari ebox
        $urlget = "http://eboxapi.popbox.asia:8080/ebox/api/v1/express/query?page=0&maxCount=1&expressType=COURIER_STORE&expressStatus=IMPORTED&expressNumber=".  $ins["order_number"];

    	$varupid = "";
        $upres2=$this->post_data($urlget, $varupid, $this->headers);

        $id = $upres2["resultList"][0]["id"];

        //post ke newlocker
        $urlupload = "http://pr0x.popbox.asia/express/import";
        $varupload = [
        	            'id' => $id,
        	            'expressNumber' => $ins["order_number"],
         				'takeUserPhoneNumber' => $customer_phone,
         				'groupName' => ''
         				];
         $varupload = json_encode($varupload);		 
         $upres=$this->post_data($urlupload, $varupload, $this->headers);

	     // insert partner response to database
        DB::table('companies_response')
        	    ->insert([
                	    'api_url' => $urlupload,
                    	'api_send_data' => $varupload,
                        'api_response'  => json_encode($upres), 
                        'response_date'     => date("Y-m-d H:i:s")
          ]);	    	            
	            
	            
		}
		

        $sqlmaila = "select admin_email from admin_notification where privilege='ALL'";
		$admins = DB::select($sqlmaila);
		$name = "Admin PopBox";
        $tglsubmit = date("Y-m-d H:i:s");          
        $merchant_name = $merchant->merchant_name;
	    foreach ($admins as $admin ) {	    	
            $email=$admin->admin_email;
             $jdlmail = "New Merchant Pickup Request Baru from ".$merchant_name." on ".$tglsubmit ;
			if ($merchant_name != 'Administrator') {

            	\Mail::send('user.emails.merchantpuarray', ['data'=> $insArr, "merchant" => $merchant], function ($m) use($email,$name,$jdlmail){
					$m->from('admin@popbox.asia', 'Admin PopBox');
					$m->to($email, $name)->subject($jdlmail);
					$m->bcc(config("config.bcc"));
				});	
			}
		}
	}
	

	public function pickupdetail(Request $request){
		$order_no = $request->input("order_no","");
		$page = $request->input("page","");
		$data["qrcode"] = DNS2D::getBarcodePNG($order_no, "QRCODE", 5,5);		
		$merchant = DB::table('tb_merchant_pickup')->where("order_number", $order_no)->first();				
		$data["merchant"] = $merchant;		
		$data["page"] = $page;
		return view('dashboard.pickupdetail', $data);
	}

	private function getStatus($status){
		if ($status=="2"){
			return "IN_STORE";
		}else if ($status=="3"){
			return "CUSTOMER_TAKEN";
		}else if ($status=="4"){
			return "COURIER_TAKEN";
		} else if ($status=="5"){
			return "OPERATOR_TAKEN";
		}else{
			return "";
		}
	}

  
    protected $headers = array (
			'Content-Type: application/json' 
	);
	protected $is_post = 0;

	public function post_data($url, $post_data = array(), $headers = array(), $options = array()) {
		$result = null;
		$curl = curl_init ();

		if ((is_array ( $options )) && count ( $options ) > 0) {
			$this->options = $options;
		}
		if ((is_array ( $headers )) && count ( $headers ) > 0) {
			$this->headers = $headers;
		}
		if ($this->is_post !== null) {
			$this->is_post = 1;
		}

		curl_setopt ( $curl, CURLOPT_URL, $url );
		curl_setopt ( $curl, CURLOPT_POST, $this->is_post );
		curl_setopt ( $curl, CURLOPT_POSTFIELDS, $post_data );
		curl_setopt ( $curl, CURLOPT_COOKIEJAR, "" );
		curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt ( $curl, CURLOPT_ENCODING, "" );
		curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $curl, CURLOPT_AUTOREFERER, true );
		curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); // required for https urls
		curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 5 );
		curl_setopt ( $curl, CURLOPT_TIMEOUT, 5 );
		curl_setopt ( $curl, CURLOPT_MAXREDIRS, 10 );
		curl_setopt ( $curl, CURLOPT_HTTPHEADER, $this->headers );

		$content = curl_exec ( $curl );
		$response = curl_getinfo ( $curl );
		$result = json_decode ( $content, TRUE );
	
		//debug
		

		curl_close ( $curl );
		return $result;
	}
}