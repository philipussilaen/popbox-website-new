<?php

namespace App\Http\Controllers;

use App\Models\Locker;
use App\Models\LockerService;
use Illuminate\Http\Request;
use App\Http\Helper\API;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use \URL;
use \Mail;


class LandingController extends Controller
{
    public function getGramediaLanding()
    {
        $locker = \App\Http\Helper\Locker::getAllLocker('Indonesia');
        if ($locker) $lockerList = $locker;
        $lockerJkt = [];
        foreach ($lockerList as $index => $locker) {
            $lockerJkt[$index]['name'] = $locker->name;
            $lockerJkt[$index]['address'] = $locker->address;
            $lockerJkt[$index]['address_2'] = $locker->address_2;
            $lockerJkt[$index]['city'] = $locker->district;
            $lockerJkt[$index]['province'] = $locker->province;
            $lockerJkt[$index]['latitude'] = $locker->latitude;
            $lockerJkt[$index]['longitude'] = $locker->longitude;
            $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
        }
        //Get Cities array
        $citiesList = [];
        foreach ($lockerList as $item) {
            if (!in_array($item->district, $citiesList)) {
                $citiesList[] = $item->district;
            }
        }
        // parsing to view
        $data = [];
        $data['lockerJkt'] = $lockerJkt;
        $data['citiesList'] = $citiesList;

        return view('landing.gramedia', $data);
    }

    public function getCopLanding()
    {
        $services = ['cop', 'emoney'];
        $data = self::_getHelperCopLanding($services);
        return view('landing.cop', $data);
    }

    public function getPulsaLanding()
    {
        $services = ['emoney'];
        $data = self::_getHelperCopLanding($services);
        return view('landing.pulsa', $data);
    }

    public function getPromoPulsaLanding()
    {
        $services = ['emoney'];
        $data = self::_getHelperCopLanding($services);
        return view('landing.promo-pulsa', $data);
    }

    public function ramadhan()
    {
        return view('landing.ramadhan');
    }

    public function indosat()
    {
        $services = ['emoney'];
        $data = self::_getHelperCopLanding($services);
        return view('landing.indosat', $data);
    }

    public function popboxagent(Request $req)
    {
        // $api = new API();
        // if ($req->isMethod('post')){            
        //     $provinsi_id = $req->input("province");
        //     $city = $api->getCity($provinsi_id)->data[0]->city_list;    
        //     // print_r($city);
        //     // die();          
        //     return response()->json($city);
        // }else{
        //     $services = ['emoney'];        
        //     $data = self::_getHelperCopLanding($services);
        //     // $data["provincies"] = $api->getProvince()->data;            
        //     $data["services"] = $api->getServiceAgent()->data;             
        //     return view('landing.agent',$data);
        // }
        return view('landing.agent');
    }

    public function getAnniv2()
    {
        return view('landing.anniv2');
    }

    public function getAnniv2cara()
    {
        return view('landing.anniv2cara');
    }

    public function getAnniv2syarat()
    {
        return view('landing.anniv2syarat');
    }

    public function getAnniv2promo()
    {
        return view('landing.anniv2promo');
    }

    public function submit_popbox_agent(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'phone' => 'required',
            'full_name' => 'required',
            "toko_name" => 'required',
            'address' => 'required'
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#daftar")->withInput(Input::all())->withErrors($validator);
        }

        $data = ["email" => $req->input("email", ""),
            "phone" => $req->input("phone", ""),
            "full_name" => $req->input("full_name", ""),
            "toko_name" => $req->input("toko_name", ""),
            "address" => $req->input("address", ""),
            "status" => 0
        ];

        \DB::table("tb_agent_candidate")->insert($data);

        $email = "agent@popbox.asia";
        $name = $data["full_name"];
        $jdlmail = "PopBox Agent Candidate ".$name;
        Mail::send('user.emails.agent', ["data" => $data], function ($m) use ($email, $jdlmail, $name) {
            $m->from('agent@popbox.asia', 'Admin PopBox');
            $m->to('agent@popbox.asia', $name)->subject($jdlmail);
        });

        if (!empty($data['email'])) {
            $email = $data['email'];
            $jdlmail = $data['toko_name'];
            $name = $data["full_name"];
            Mail::send('user.emails.agentcustomer', ["data" => $data], function ($m) use ($email, $jdlmail, $name) {
                $m->from('agent@popbox.asia', 'Admin PopBox');
                $m->to($email, $name)->subject($jdlmail);
            });
        }

        $req->session()->flash('flash_message', 'Terima kasih telah mendaftar menjadi PopBox Agen, tim kami akan segera menghubungi anda dalam waktu 2x24 jam (hari kerja).');
        return Redirect::to(URL::previous() . "#daftar");
    }

    public function _getHelperCopLanding($services)
    {

        // get locker and locker services COP only
        $lockerServiceDb = Locker::join('locker_services', 'locker_locations.locker_id', '=', 'locker_services.locker_id')
            ->join('districts', 'districts.id', '=', 'locker_locations.district_id')
            ->join('buildingtypes', 'buildingtypes.id_building', '=', 'locker_locations.building_type_id')
            ->whereIn('locker_services.service', $services)
            ->groupBy('name')
            ->orderBy('name', 'asc')
            ->get();

        // get cities and building types array
        $cities = [];
        $types = [];
        foreach ($lockerServiceDb as $locker) {
            if (!in_array($locker->district, $cities)) $cities[] = $locker->district;
            if (!in_array($locker->building_type, $types)) $types[] = $locker->building_type;
        }

        // split to 3 array for view purpose
        $countLocker = $lockerServiceDb->count();
        $splitted = ceil($countLocker / 3);

        $i = 0;
        $lockerSplitted = [];
        $index = 0;

        foreach ($lockerServiceDb as $item) {
            if ($i == $splitted) {
                $i = 0;
                $index = $index + 1;
            }
            $lockerSplitted[$index][] = $item;
            $i++;
        }

        $data = [];
        $data['lockerServices'] = $lockerSplitted;
        $data['lockerList'] = $lockerServiceDb;
        $data['services'] = $services;
        $data['cities'] = $cities;
        $data['types'] = $types;
        return $data;
    }

    public function getReturnLandingNew(){
        $set = 'Indonesia';

        $lockerList = array();
        $lockerJkt = array();
        $citiesList = array();

        $locker = Locker::getAllLocker($set);
        if ($locker) $lockerList = $locker;

        foreach ($lockerList as $index=>$locker){
            $lockerJkt[$index]['name'] = $locker->name;
            $lockerJkt[$index]['address'] = $locker->address;
            $lockerJkt[$index]['address_2'] = $locker->address_2;
            $lockerJkt[$index]['city'] =  $locker->district;
            $lockerJkt[$index]['province'] =  $locker->province;
            $lockerJkt[$index]['latitude'] =  $locker->latitude;
            $lockerJkt[$index]['longitude'] =  $locker->longitude;
            $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
            $lockerJkt[$index]['locker_id'] = $locker->locker_id;
        }

        foreach ($lockerJkt as $item) {
            if(!in_array($item['city'],$citiesList)) $citiesList[] = $item['city'];
        }

        $data['lockerJkt'] = $lockerJkt;
        $data['citiesList'] = $citiesList;
        
        return view('return', $data);
    }

    public function getReturnLanding(){
       $lockerServiceDb = Locker::join('locker_services', 'locker_locations.locker_id', '=', 'locker_services.locker_id')
            ->join('districts', 'districts.id', '=', 'locker_locations.district_id')
            ->join('buildingtypes', 'buildingtypes.id_building', '=', 'locker_locations.building_type_id')
            ->whereIn('locker_services.service', ['emoney'])
            ->groupBy('name')
            ->orderBy('name', 'asc')
            ->get();

        // get cities and building types array
        $cities = [];
        $types = [];
        foreach ($lockerServiceDb as $locker) {
            if (!in_array($locker->district, $cities)) $cities[] = $locker->district;
            if (!in_array($locker->building_type, $types)) $types[] = $locker->building_type;
        }

        // split to 3 array for view purpose
        $countLocker = $lockerServiceDb->count();
        $splitted = ceil($countLocker / 3);

        $i = 0;
        $lockerSplitted = [];
        $index = 0;

        foreach ($lockerServiceDb as $item) {
            if ($i == $splitted) {
                $i = 0;
                $index = $index + 1;
            }
            $lockerSplitted[$index][] = $item;
            $i++;
        }

        $data = [];
        $data['lockerServices'] = $lockerSplitted;
        $data['lockerList'] = $lockerServiceDb;
        $data['services'] = 'emoney';
        $data['cities'] = $cities;
        $data['types'] = $types;
        return view('return', $data);
    }
}
