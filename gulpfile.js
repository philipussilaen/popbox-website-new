var elixir = require('laravel-elixir') , gulp = require('gulp');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 elixir(function(mix) {
    mix.version(['css/bootstrap.css',
        'css/style.css',
        'css/swiper.css',
        'css/dark.css',
        'css/app-landing.css',
        'css/font-icons.css',
        'css/animate.css',
        'css/magnific-popup.css',
        'css/css2/fonts.css',
        'css/components/bs-switches.css',
        'css/responsive.css',
        'css/css2/colors.css',,
        'css/magnific-popup.css',
        'css/flag-icon.min.css',
        'css/custom.css',
        'js/plugins.js',
        'js/jquery.vmap.js',
        'js/vmap/jquery.vmap.world.js',
        'js/functions.js',
        'js/jquery.js',
        'js/jquery.gmap.js',
        'js/validation.js',
    ]);
    mix.copy('public/css/fonts','public/build/css/fonts');
    mix.copy('public/css/imports','public/build/css/imports');
    mix.copy('public/images','public/build/images');
    mix.copy('public/flags','public/build/flags');
 });
