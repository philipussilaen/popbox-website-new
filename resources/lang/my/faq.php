<?php

return [

    /*
    |--------------------------------------------------------------------------
    | FAQ Page Lines
    |--------------------------------------------------------------------------
    |
    */
   
    'ambil-btn'=>'Collect Parcel',
    'return-btn'=>'Return Parcel',

    'apaitu-title' => 'What is PopBox?',
    'apaitu-content'=> 'PopBox is an automated parcel locker that allows you to send, receive, and return parcels conveniently. You will receive an SMS notification with a PIN code when the parcel has been dropped in the locker. PopBox helps to minimize the risk of damaged and lost parcels during the delivery process.',

    'kenapa-title'=>'Why use PopBox?',
    'kenapa-sub-conv'=>'Convenient',
    'kenapa-sub-conv-content'=> 'Never miss a delivery again. If you send your order to PopBox, you won’t need to worry about coordinating your time with the parcel’s arrival time. Instead, you can simply go to the locker and collect the parcel at your convenience.',
    'kenapa-sub-1'=>'Easy',
    'kenapa-sub-1-content'=> 'Lockers PopBox, makes it easy to retrieve, restore, or send your package. The whole process should not take more than 5 seconds. Simply come to the lockers and locker access on-screen menus as per the service that you want to use.',
    'kenapa-sub-2'=>'Fast',
    'kenapa-sub-2-content' => 'Parcels will be delivered within THREE (3) working days from a PopBox locker to another PopBox locker via PopSend.',
    'kenapa-sub-3'=> 'Safe',
    'kenapa-sub-3-content'=> 'Collect parcels with a unique PIN code that you will receive via SMS notification or the PopBox app. You won’t need to worry about your parcels being damaged or lost. All PopBox lockers are under CCTV surveillance.',

    'dimana-title'=>'Where are PopBox lockers located?',
    'dimana-content'=>'PopBox lockers can be found in strategic locations such as malls, office buildings, universities, and residential areas in Kuala Lumpur and Selangor. Find the nearest PopBox locker from your location <a href="https://location.popbox.asia/">HERE</a>',

    'ukuran-title'=>'What are the different locker compartment sizes?',
    'ukuran-content'=>'<ol>
                            <li>XS : 18.5 cm (H) x 19.5 cm (L) x 48 cm (W)</li>
                            <li>S : 10 cm (H) x 34.5 cm (L) x 48 cm (W)</li>
                            <li>M : 18 cm (H) x 34.5 cm (L) x 48 cm (W)</li>
                            <li>L : 31 cm (H) x 34.5 cm (L) x 48 cm (W)</li>
                        </ol>
                        <br>
                        * Maximum weight for each parcel is 3kg.',

    'asuransi-title'=> 'Is my parcel in a PopBox locker insured?',
    'asuransi-content'=> 'Our lockers are very safe and secured with an electronic locking system. All activities, deposits and collections, are tracked with our proprietary software system and all lockers have CCTVs equipped. If something happens to your item, we will review the situation on a case-by-case basis to determine the compensation.',

    'aman-title'=> 'How secure are PopBox lockers?',
    'aman-content'=> 'PopBox lockers operate 24/7 and have a secure electronic locking system. During collection, your compartment will only open with the unique PIN code (sent to you via SMS or the app) and all the lockers are under CCTV surveillance.',

    'aman-title-2'=> 'What is the office operating hours?',
    'aman-content-2'=> 'Mon – Fri: 9am – 6pm (Closed on Saturday, Sunday and Public Holidays).',

    'aman-title-3'=> 'What is the customer service operating hours?',
    'aman-content-3'=> '<ol>
                            <li>PopBox Hotline: Mon – Fri: 9am – 6pm (Closed on Saturday, Sunday and Public Holiday)</li>
                            <li>PopBox Malaysia: <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F317461161971113%2F%3Fmessaging_source%3Dsource%253Apages%253Amessage_shortlink">Facebook Messenger</a></li>
                        </ol>
                        <br>
    PopBox team will get back to you as soon as possible.',

    'beli-title'=> 'How do I send my online purchases to PopBox?',
    'beli-content'=>'
    There are two key ways to do this.
    <ol>
        <li><b>Select Locker Location at Checkout:</b> PopBox has integrated with some e-commerce stores. For these online stores, when at the checkout section, you’ll see an option to deliver to a PopBox locker.</li>
        <li><b>Enter Address at Checkout:</b> For e-commerce sites in which we don’t have app integration, you can still deliver to a PopBox locker by entering the locker’s address. You can get the addresses for PopBox lockers here.</li>
        <li>The mailing address should include your (1) full name, (2) PopBox locker designation, and (3)full address.<br>
        Mr. Hamid Mustapha<br>
        PopBox @ Menara Sunway<br>
        Menara Sunway, Jalan Lagoon Timur,<br>
        Bandar Sunway, 47500 Subang Jaya,<br>
        Selangor Darul Ehsan, Malaysia.
        </li>
        <li>Make sure your phone number is correct for security purposes and for you to receive the
        unique pin code to open the locker’s compartment.</li>
    </ol>',

    'status-title'=> 'How will I know when my parcel is ready for collection from a PopBox locker?',
    'status-content'=> 'Once the parcel has been placed in the PopBox locker, you will be notified via SMS and receive a 6-digit PIN code. You can then collect your parcel from the locker at your convenience.',

    'lama-title'=> 'How much time do I have to collect my parcel at PopBox?',
    'lama-content'=> 'You have 72 hours (3 days) to collect your parcel after receiving the SMS notification. If you have not collected your parcel within the timeframe, our customer service will contact you for the next step or else the parcel will be designated to the courier centre accordingly.',

    'parcel-bigger-title'=> 'What can I do if the parcel size is bigger than the L sized locker or the locker has no available compartments?',
    'parcel-bigger-content'=> 'You can either arrange for door-to-door delivery with the courier or we will arrange our dispatch to collect your parcel from the courier service centre. For some buildings, your mailroom will keep the parcel for you, if the parcel does not fit into the locker.',

    'pin-code-sms-title'=> 'What can I do if I don’t receive a PIN code via SMS?',
    'pin-code-sms-content'=> 'You can contact us at PopBox Malaysia’s <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F317461161971113%2F%3Fmessaging_source%3Dsource%253Apages%253Amessage_shortlink">Facebook Messenger</a> or Customer Service Hotline: 011-1060 6011. You are encouraged to download the PopBox app to check your PIN code too.',


    'wrong-code-title'=> 'What should I do if I entered the wrong phone number or change to new phone number?',
    'wrong-code-content'=> 'You can contact us at PopBox Malaysia’s <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F317461161971113%2F%3Fmessaging_source%3Dsource%253Apages%253Amessage_shortlink">Facebook Messenger</a> or Customer Service Hotline:011-10606011 including your parcel tracking number, wrong phone number, correct phone number for verification.',

    'why-notpin-title'=> 'Why did I not receive a PIN code via SMS if the parcel tracking history shows that my parcel has arrived in my university already?',
    'why-notpin-content'=> 'For universities, your parcels will take at least 24 hours from when it arrives in the mail room to a PopBox locker. It is required by the university management to record all the parcels received for safety reasons.',

    'merchant-return-title'=> 'Are there online merchants who partnered with PopBox for the return of goods?',
    'merchant-return-content'=> 'Yes. Currently, you can use PopBox to return items purchased from Lazada Malaysia. Please follow the instructions of the Lazada Return Policy.',

    'cara-return-title'=> 'How do I return purchased goods to Lazada?',
    'cara-return-content'=> '<ol>
                            <li>Check the validity of Lazada’s Return Policy on the official Lazada website.</li>
                            <li>Fill in the ‘Online Returns Form’ and print the shipping label. Check the information here.</li>
                            <li>Pack the item(s) and attach the shipping label onto the box./li>
                            <li>Proceed to your nearest PopBox locker and select “Return Goods” on the screen.</li>
                            <li>Select the online merchant and enter the return code provided by the merchant.</li>
                            <li>Enter your phone number. Please make sure the information is correct in order to receive
                            SMS notifications.</li>
                            <li>Select the size of locker and place the parcel inside it.</li>
                        </ol>',

    'biaya-return-title'=> 'How much does it cost to collect or return goods using PopBox?',
    'biaya-return-content'=> 'It’s FREE! There are no charges to collect items from PopBox or to return items using PopBox.',

    'status-return-title'=> 'How do I find out whether my returned goods has reached the online merchant or sell?',
    'status-return-content'=> 'PopBox will send an SMS notification to your phone number.',

    'deliver-parcel-title-1'=> 'Can PopBox be used as deliver service?',
    'deliver-parcel-content-1'=> 'Yes, you can download the PopBox app and use the PopSend feature to deliver items from locker to locker.',

    'deliver-parcel-title-2'=> 'How to use PopSend?',
    'deliver-parcel-content-2'=> 'Just download the PopBox app and top up a minimum of RM 20 credit (No expiry date for the credit reloaded). Select the ‘PopSend’ feature in the app. Fill in the details required and select which locker the parcel will be shipped from and sent to.',

];
