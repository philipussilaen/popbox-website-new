<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	
	<body>
		<p>Hi Admin, </p>
		<p> New Merchant Pickup Request from {{ $merchant->merchant_name }} on {{date("Y-m-d H:i:s")}} yang harus segera diproses</p>
		<p>			
			Merchant info :<br/>
		
			Owner Name : {{ $merchant->owner_name }}<br />
			Contact No : {{ $merchant->phone }}<br />
			Pickup Time : {{ $merchant->time_pickup }}<br /><br />
		<table style="border-spacing: 0px">
			<tr>
				<td cellpadding="0" cellspacing="0" width="20px" style="border: 1px solid black;margin: 0px;">No</td>
				<td cellpadding="0" cellspacing="0" width="20px" style="border: 1px solid black;margin: 0px;">Order No</td>
				<td cellpadding="0" cellspacing="0" width="100px" style="border: 1px solid black;margin: 0px;">Pickup Location</td>
				<td cellpadding="0" cellspacing="0" width="100px" style="border: 1px solid black;margin: 0px;">Popbox Location</td>
				<td cellpadding="0" cellspacing="0" width="100px" style="border: 1px solid black;margin: 0px;">Customer Phone</td>
				<td cellpadding="0" cellspacing="0" width="100px" style="border: 1px solid black;margin: 0px;">Customer Email</td>	
			</tr>
			<?php $i=1 ?>
			@foreach ($data as $key => $value) 
				<tr>
					<td cellpadding="0" cellspacing="0" style="border: 1px solid black;margin: 0px;">{{$i++}}</td>
					<td cellpadding="0" cellspacing="0" style="border: 1px solid black;margin: 0px;">{{ $value["order_number"] }}</td>
					<td cellpadding="0" cellspacing="0" style="border: 1px solid black;margin: 0px;">{{ $value["pickup_location"] }}</td>
					<td cellpadding="0" cellspacing="0" style="border: 1px solid black;margin: 0px;">{{ $value["popbox_location"] }}</td>
					<td cellpadding="0" cellspacing="0" style="border: 1px solid black;margin: 0px;">{{ $value["customer_phone"] }}</td>
					<td cellpadding="0" cellspacing="0" style="border: 1px solid black;margin: 0px;">{{ $value["customer_email"] }}</td>
				</tr>
			@endforeach
		</table>		
	</body>
</html>