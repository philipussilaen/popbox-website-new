@extends('layout.main-article')
@section('content')
	<section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('img/bg/bg-pod.jpg'); padding: 120px 0;" data-stellar-background-ratio="0.3">
        <div class="container clearfix">
            <h1>POPBOX ON DEMAND</h1>
            <span>Laundry.Repair.Cleaning</span>
        </div>
    </section>
    <style type="text/css">
    	@media only screen and (min-width:480px){
    		.merchant{
    			margin-right: 0px; 
    			width: 33.3333%; 
    		}
    	}
    </style>
    <section id="content">
	    <div class="content-wrap">
			<div class="container clearfix">
				<div class="nobottommargin">
					<div class="col_full clearfix" align="center">
						<h4>
							Melakukan laundry baju dan sepatu kamu menjadi sangat mudah dengan adanya PopBox On Demand, caranya : <br>
							1. Buat Order ke partner PopBox, klik pada gambar di bawah. <br>
							2. Letakkan baju/sepatu kamu ke loker dengan nomor order kamu. <br>
							3. Baju/sepatu kamu akan diproses oleh partner PopBox. <br>
							4. Tunggu notifikasi kode PIN untuk mengambil barang. <br>
							5. Ambil baju/sepatu bersihmu di loker PopBox. <br>
						</h4>
					</div>
					@foreach (array_chunk($merchant, 3) as $chunked)
						<div class="col_full clearfix" style="">
							@foreach ($chunked as $element)
								<div class="col_one_third merchant">
									 <div class="feature-box media-box">
				                        <div class="fbox-media">
				                            <a href="{{ $element->url }}"><img src="{{\App\Http\Helper\Helper::createImgUrl('merchant',$element->image)}}"></a>
				                        </div>
				                        {{-- <div class="emphasis-title bottommargin-sm">
				                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center"> {{ trans('index.services-1') }}</h3>
				                        </div> --}}
				                        <p style="font-size: 16px; color: #444;" class="ls1 center t300">
				                            {{ $element->description }}
				                        </p>
				                    </div>
								</div>
							@endforeach
						</div>
					@endforeach
					<div class="col_full clearfix" align="center">
						<h3>
							<strong>Menjadi Partner PopBox</strong>
						</h3>
						<h4>
							Ingin bekerjasama dengan PopBox untuk layanan kamu, <br>silahkan hubungi tim kami di <a href="mailto:info@popbox.asia">info@popbox.asia</a> dengan subject "Partnership (Nama Brand) <> PopBox"
						</h4>
					</div>
				</div>
			</div>
		</div>	
    </section>
@stop