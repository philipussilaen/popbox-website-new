@extends('layout.main')
@section('content')
<section id="slider" class="slider-parallax full-screen dark error404-wrap" style="background: url({{ asset('img/landing1.jpg') }}) center;">
    <div class="slider-parallax-inner">

        <div class="container vertical-middle center clearfix">

            <div class="error404">500</div>

            <div class="heading-block nobottomborder">
                <h4>Ooopps.! We are under maintenance.</h4>
            </div>
        </div>

    </div>
</section>
@stop