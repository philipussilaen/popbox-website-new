<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>
    <meta charset="utf-8">
   
    <meta property="og:url" content="<?php echo $_SERVER['SERVER_NAME']; ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="PopBox 2nd Anniversary" />
    <meta property="og:description" content="Dapatkan GRAND PRIZE diakhir bulan tiket pesawat + hotel menginap 4H3M di Bali untuk 2 orang. Ikuti tantangannya!" />
    <meta property="og:image" content="{{ URL::asset('landing/img/anniv2/meta.png')}}" />

    <title>PopBox 2nd Anniversary Syarat Mengikuti PopBox Quiz</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="PopBox Asia Team" />
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('img/favico.ico') }}">
    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/style.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/dark.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/font-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/magnific-popup.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/responsive.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/flag-icon.min.css') }}">    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

     <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-72128831-1', 'auto');
            ga('send', 'pageview'); 
        </script>

</head>


<link rel="stylesheet" href="/css/anniv2.css">
<body  class="stretched">
  <div id="wrapper" class="clearfix">
        <!-- Header
        ============================================= -->
       <!-- <header id="header" class="full-header"> -->
       	<header class="anniv-header">
            <div id="header-wrap">
                <div class="container clearfix menu-head">
                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
     
                    <nav id="primary-menu" class="dark">
                        <ul class="sf-js-enabled" style="touch-action: pan-y;">                            
                           <li><a href="/popboxanniv">Home</a></li>
                             <li><a href="/popboxanniv/cara">Cara</a></li>
                             <li><a href="/popboxanniv/syarat">Syarat & Ketentuan</a></li>
                             <li><a href="/popboxanniv/promo">Promo Lain</a></li>
                            <!-- <li><a href="#contact-us"><div>Contact Us</div></a></li>-->
                        </ul>

                    </nav>
                    <!-- #primary-menu end -->
                </div>
                <div class="logoanniv2"><img src="../img/anniv2/logo-popbox-2nd-anniv_03.png"></div>
            </div>
        </header>
        <!-- #header end -->       
    </div>
 	
 
    <section id="content" style="max-height:4350px;">
        <div class="container clearfix" id="home-ikuti">

              <div class="row fitur syaratpos" id="syarat">
            	<div class="row">
                	<div class="col-md-12">
                	    <div><img src="../img/anniv2/bg_syarat.png"></div>
                        <div class="syarathead">
                            Syarat & Ketentuan PopBox Quiz
                        </div>
                	</div>
            	</div>
            	<div class="row">
            	    <div class="col-md-1"></div>
            		<div class="col-md-8 syaratpad">
            			<p class="syaratcont" style="font-size:20px">Hello Popers!</p>
            			<p class="syaratcont">Dalam rangka 2nd Anniversary Popbox Asia, kami akan memberikan 4 tantangan seru dengan <br>hadiah utama <strong>LIBURAN KE BALI !</strong> PopBox akan memberikan grand prize hadiah tiket pesawat JKT-BALI dan <br>penginapan di Bali selama 4H3M kepada 1 pemenang yang beruntung.<br> Join kontesnya setiap hari Senin dan menangkan hadiahnya!  </p>  
            			<p class="syaratcont">Sebelum mengikuti kontesnya, yuk baca ketentuan ini dengan seksama. Dengan mengikuti kontes ini, kamu dianggap telah menyetujui semua syarat dan ketentuan yang berlaku </p>
            			<p class="syaratcont" style="margin-bottom:10px" id="cara-syarat"><strong>Syarat dan ketentuan :</strong></p>  
            			<ol type="1" class="syaratcont" style="margin-left:20px">
            				<li>Follow dan Like Facebook, Instagram, dan Twitter PopBox Asia
            				<li>Ikuti semua tantangan yang telah diberikan selama 1 bulan (total ada 4 tantangan)
            				<li>Tantangan akan diadakan setiap hari Senin selama bulan Juli 2017
            				<li>Tantangan yang diminta akan berbeda setiap minggunya.
            				<li>PopBox berhak menggunakan dan mengedit konten yang dikirimkan oleh peserta kuis.
            				<li>Akan ada total 3 orang pemenang setiap minggunya yang akan mendapatkan paket eksklusif dari PopBox
            				<li>Pemenang mingguan yang telah menang sebelumnya tidak bisa menang lagi di minggu-minggu berikutnya
            				<li>Hadiah mingguan akan dikirimkan ke loker PopBox JABODETABEK	
            				<li>Pemenang yang terpilih wajib mengkonfirmasi data diri dengan mengirimkan Nama Lengkap, Lokasi Loker PopBox (Untuk Pengiriman Hadiah), No. Telepon, & E-mail melalui messages Facebook atau Instagram PopBox Asia
            				<li>Konfirmasi data diri dari pemenang ditunggu maksimal seminggu setelah pengumuman pemenang. Apabila melebihi tanggal tersebut, hadiah akan kami batalkan secara otomatis
            				<li>Apabila peserta tidak mematuhi salah satu syarat dan ketentuan, dapat membuat peserta diguggurkan. Hadiah dapat diberikan kepada pemenang lain yang ditentukan oleh PopBox
            				<li>PopBox Asia berhak untuk mengubah Syarat dan Ketentuan Kuis ini setiap saat, tanpa memberi pemberitahuan sebelumnya kepada pihak manapun
            				<li>Dengan mengikuti kuis ini, peserta setuju untuk terikat dengan Syarat dan Ketentuan, dan bahwa keputusan PopBox Asia adalah mengikat
            				<li>Keputusan pemenang PopBox Asia adalah mutlak dan tidak dapat diganggu gugat
            				<li>Periode kompetisi dari 3 Juli - 30 Juli 2017
            				<li>Pengumuman pemenang pada tanggal 31 Juli 2017 di social media PopBox Asia
            			</ol>      		
            			<p class="syaratcont" style="margin-bottom:10px;margin-top:20px" id="cara-kriteria"><strong>Kriteria Pemenang :</strong></p>
            			<ol type="1" class="syaratcont" style="margin-left:20px">
            				<li>Warga Negara Indonesia dan berdomisili di Indonesia.
            				<li>Berusia minimal 18 tahun.
            				<li>Memiliki alamat email aktif dan nomor telepon yang dapat dihubungi.
            			</ol>
            			<p class="syaratcont" style="margin-bottom:10px;margin-top:20px" id="cara-pemenang"><strong>Pemenang :</strong></p>
            			<ol type="1" class="syaratcont" style="margin-left:20px">
            				<li>Pemenang ditentukan oleh penjurian tertutup berdasarkan posting yang paling menarik
            				<li>Penyelenggara akan meminta para pemenang untuk mengkonfirmasi kesediaan untuk beserta data diri sesuai KTP. Jika pemenang tidak melakukan konfirmasi dalam waktu 24 jam, atau tidak bisa mengikuti kesediaan tanggal yang telah ditentukan maka pemberian hadiah akan dianggp gugur dan PopBox berhak memilih peserta lainnya.
            				<li>1 pemenang akan medapatkan 2 (dua) tiket pesawat JKT-Bali dan hotel menginap di Bali 4H3M (hari kerja)
            				<li>Penyelenggara tidak menyediakan transport dan akomodasi bagi pemenang yang datang dari luar Jakarta.
            				<li>Peserta dapat digugurkan apabila peserta tidak mematuhi salah satu syarat dan ketentuan. Hadiah dapat diberikan kepada pemenang lain yang ditentukan oleh pihak PopBox Asia.
            				<li>Keputusan pemenang PopBox ASia bersifat mutlak dan tidak dapat diganggu gugat
            				<li>Dengan mengikuti kompetisi ini, peserta dianggap setuju untuk terikat dengan Syarat dan Ketentuan dan bahwa keputusan PopBox Asia bersifat mengikat
            			</ol>			
            			<p class="syaratcont" style="margin-bottom:10px;margin-top:20px;font-size:20px"">Good Luck!</p>
            		</div>
            		<div class="col-md-3">
            		     <img src="../img/anniv2/bg_sidebarsk.png" class="sidebarskbg">
            		     <div class="sidebarsk">
            		     		<i class="fa fa-caret-right" aria-hidden="true" style="color:red"></i> <a href="#cara-syarat" style="color:black">Syarat dan ketentuan</a><br>
            		     		<i class="fa fa-caret-right" aria-hidden="true" style="color:red" ></i> <a href="#cara-kriteria" style="color:black">Kriteria Pemenang</a><br>
            		     		<i class="fa fa-caret-right" aria-hidden="true" style="color:red"></i> <a href="#cara-pemenang" style="color:black">Pemenang</a>
            		     </div>
            		</div>
            	</div>
        	</div>
    

    		<div class="row popline" id="contact-us">
    			<div class="col-md-12">
    				<div class="popline-logo"><img src="../img/anniv2/popline.png"></div>
    				<div class="popline-tanya">Untuk <strong>Pertanyaan</strong> dan <strong>Keluhan</strong> bisa hubungi</div>
    				<div class="popline-contact"><strong>LINE</strong> <span style="color:#fcbf30;background-color:#fff;border-radius:12px;border:1px solid #fcbf30"> @popboxasia </span></div>
    			</div>
    				
    		</div>
    </section>
	
	
<footer id="footer" class="dark">
    <div class="container">
        <!-- Footer Widgets
        ============================================= -->
        <div class="footer-widgets-wrap clearfix">
            <div class="row clearfix">
                <div class="col-md-5">
                    <div class="widget widget_links clearfix">
                        <div class="row clearfix">
                            <div class="col-md-8 bottommargin-sm clearfix" style="color:#888;">
                                <img src="{{ asset('img/footer-logo.png') }}" alt="PopBox Logo" style="display: block;" class="bottommargin-sm">
                                <p>{{ trans('layout.tag') }}</p>
                                <a href="https://www.facebook.com/pboxasia" class="social-icon si-small si-borderless si-colored si-rounded si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="https://twitter.com/popbox_asia" class="social-icon si-small si-borderless si-colored si-rounded si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="https://instagram.com/popbox_asia" class="social-icon si-small si-borderless si-colored si-rounded si-instagram">
                                    <i class="icon-instagram"></i>
                                    <i class="icon-instagram"></i>
                                </a>
                                <a href="https://www.youtube.com/channel/UCYGHLg4Xdc_IHD5DHdDIRTA" class="social-icon si-small si-borderless si-colored si-rounded si-youtube">
                                    <i class="icon-youtube"></i>
                                    <i class="icon-youtube"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/pt-popbox-asia-services" class="social-icon si-small si-borderless si-colored si-rounded si-linkedin">
                                    <i class="icon-linkedin"></i>
                                    <i class="icon-linkedin"></i>
                                </a>
                                <a href="#" class="social-icon si-small si-borderless si-colored si-rounded si-line">
                                    <i class="icon-line"></i>
                                    <i class="icon-line"></i>
                                </a>
                            </div>
                        </div>
                        <div>
                            {{ trans('layout.callus') }}<br>
                            <a href="tel:{{ trans('contact.contact-phone') }}"><i class="icon-call i-alt"></i>&nbsp; {{ trans('contact.contact-phone') }}</a><br>
                            <a href="mailto:info@popbox.asia"><i class="icon-mail i-alt"></i>&nbsp; info@popbox.asia</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row clearfix">
                        <div class="col-md-4 bottommargin-sm">
                            <div class="widget widget_links app_landing_widget_link clearfix">
                            </div>
                        </div>
                        <div class="col-md-4 bottommargin-sm">
                            <div class="widget widget_links clearfix">
                                <h4>{{ trans('layout.our-service') }}</h4>
                                <ul>
                                    <li><a href="https://popsend.popbox.asia">PopSend</a></li>
                                    <li><a href="http://shop.popbox.asia/">PopShop</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 bottommargin-sm">
                            <div class="widget widget_links clearfix">
                                <h4>PopBox</h4>
                                <ul>
                                    <li><a href="{{ url('blog') }}">{{ trans('layout.blog-url') }}</a></li>
                                    <li><a href="{{ url('contact') }}">{{ trans('layout.contact-url') }}</a></li>
                                    <li><a href="{{ url('career') }}">{{ trans('layout.career-url') }}</a></li>
                                    <li><a href="{{ url('faq') }}">{{ trans('layout.faq-url') }}</a></li>
                                    <li><a href="{{ url('merchant') }}">{{ trans('layout.merchant-url') }}</a></li>
                                    {{-- <li><a href="#">{{ trans('layout.partner-url') }}</a></li> --}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Copyrights
    ============================================= -->
    <div id="copyrights" class="nobg notoppadding">
        <div class="container clearfix">
            <div class="col_half">
                &copy; 2016 All Rights Reserved by PopBox Asia Services
                <br>
                {{-- <div class="copyright-links"><a href="#">{{ trans('layout.terms') }}</a> / <a href="#">{{ trans('layout.privacy') }}</a></div> --}}
                <div class="copyright-links">
                    <a href="http://www.geoplugin.com/geolocation/" target="_new">IP Geolocation</a> by <a href="http://www.geoplugin.com/" target="_new">geoPlugin</a>
                </div>
            </div>
        </div>
    </div>
    <!-- #copyrights end -->
</footer>
	
	
    
<!-- #footer end -->

</body>
<script type="text/javascript" src="{{ elixir('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ elixir('js/plugins.js') }}"></script>
    <!-- Footer Scripts
    ============================================= -->
<script type="text/javascript" src="{{ elixir('js/functions.js') }}"></script>
    <!-- Go to www.addthis.com/dashboard to customize your tools --> 
{{-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5823e82fc2a9cb90"></script>  --}}
<script type="text/javascript" src="{{ elixir('js/validation.js') }}"></script>


<script type="text/javascript">


    jQuery(document).ready(function($) {
        
            @if(Session::has('flash_message'))
                $('#myModal').modal('toggle');
            @endif
        
          });

    function onGoto(idelement){
        $('html,body').animate({ scrollTop: $(idelement).offset().top }, 'slow');
    }
</script>



