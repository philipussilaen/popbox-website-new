<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Payday By PopBox</title>
    <meta name="description" content="Payday By PopBox" />
    <meta name="keywords" content="payday, popbox" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ URL::asset('harbolnas2017/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('harbolnas2017/css/ionicons.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('harbolnas2017/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('harbolnas2017/css/style.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/payday.css') }}" />
	<link rel="icon" type="image/png" href="img/payday/favico128.ico">

    <script src="{{ URL::asset('harbolnas2017/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ URL::asset('harbolnas2017/js/tether.min.js') }}"></script>
    <script src="{{ URL::asset('harbolnas2017/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('harbolnas2017/js/jquery.easing.min.js') }}"></script>
    <script src="{{ URL::asset('harbolnas2017/js/wow.js') }}"></script>
    <script src="{{ URL::asset('harbolnas2017/js/scripts.js') }}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBf2v8vfQazUaI2egWcoYvwK7ljsAPh6Uk&libraries=places&callback=loadMap"></script>
    <script type="text/javascript">

        var lockerList = {!! json_encode($lockerJkt) !!};
        var markersFilterDefault = [];
        var defaultMarkers = lockerList.filter(function(item){
            markersFilterDefault.push([item.name + ", " + item.address, item.latitude, item.longitude]);
        });

        function getDistrict() {
            var district = $('#district').val();
            var districtText = $("#district option:selected").text();
            markersFilter = [];
            $('#locker').empty();
            $('#locker').append('<option value="">SELECT</option>');
            var filtered= lockerList.filter(function(item){
                if(item.city==districtText) {
                    markersFilter.push([item.name + ", " + item.address, item.latitude, item.longitude]);
                    $('#locker').append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });

            var districtIndex = $("#district option:selected").index();
            var arrDistrict = [1,2,3,4,5,6,7,8];
            var checkDistrict = $.inArray( districtIndex, arrDistrict );

            if(checkDistrict >= 0) {
                var centerMap = new google.maps.LatLng(-6.21, 106.81);
                var zoomMap = 10;
            } else {
                var centerMap = new google.maps.LatLng(-2.99, 114.84);
                var zoomMap = 5;
            }

            var markers = markersFilter;

            if(districtText == "SELECT") {
                var markers = markersFilterDefault;
            }

            var mapOptions = {
                center: centerMap,
                zoom: zoomMap,
                mapTypeId: 'roadmap'
            };

            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            map.setTilt(45);
            var infoWindow = new google.maps.InfoWindow(), marker, i;
            var url_img = '{{ asset('img/map/marker-locker.png') }}';
            var image = {
                url: url_img,
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(20, 33)
            };

            for (i = 0; i < markers.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(markers[i][1], markers[i][2]),
                    map: map,
                    icon: image,
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        var content = '<div><strong>'+markers[i][0]+'</strong></div>';
                        infoWindow.setContent(content);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

        function getLocker() {
            var locker = $("#locker").val();
            markersFilter = [];
            var lat = 0;
            var long = 0;
            var filtered = lockerList.filter(function(item){
                if(item.id==locker) {
                    markersFilter.push([item.name + ", " + item.address, item.latitude, item.longitude]);
                    lat = item.latitude;
                    long = item.longitude;
                }
            });
            var markers = markersFilter;

            var mapOptions = {
                center:new google.maps.LatLng(lat, long),
                zoom: 13,
                mapTypeId: 'roadmap'
            };

            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            map.setTilt(45);
            var infoWindow = new google.maps.InfoWindow(), marker, i;
            var url_img = '{{ asset('img/map/marker-locker.png') }}';
            var image = {
                url: url_img,
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(20, 33)
            };

            for (i = 0; i < markers.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(markers[i][1], markers[i][2]),
                    map: map,
                    icon: image,
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        var content = '<div><strong>'+markers[i][0]+'</strong></div>';
                        infoWindow.setContent(content);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

        function loadMap() {

            var markers = markersFilterDefault;

            var mapOptions = {
                center:new google.maps.LatLng(-6.21, 106.81),
                zoom: 10,
                mapTypeId: 'roadmap'
            };

            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            map.setTilt(45);
            var infoWindow = new google.maps.InfoWindow(), marker, i;
            var url_img = '{{ asset('img/map/marker-locker.png') }}';
            var image = {
                url: url_img,
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(20, 33)
            };

            for (i = 0; i < markers.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(markers[i][1], markers[i][2]),
                    map: map,
                    icon: image,
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        var content = '<div><strong>'+markers[i][0]+'</strong></div>';
                        infoWindow.setContent(content);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72128831-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body onload="loadMap()">
<!--header-->
<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top sticky-navigation">
    <button class="navbar-toggler navbar-toggler-right sticknav" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <img src="{{ URL::asset('harbolnas2017/images/sticknav.png') }}" class="img-responsive">
    </button>
    <a class="navbar-brand hero-heading" href="https://www.popbox.asia/" target="_blank">
        <img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_61.png') }}" class="img-responsive">
    </a>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item mr-3">
                <a class="nav-link page-scroll" href="#main"> Pay Day<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item mr-3">
                <a class="nav-link page-scroll" href="#merchant">Cara Pakai PopSend</a>
            </li>
            <li class="nav-item mr-3">
                <a class="nav-link page-scroll" href="#location">Lokasi PopBox</a>
            </li>
        </ul>
    </div>
</nav>

<!--main-->
<section class="border-bottom margin-top-header" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-4" style="padding-top: 10px;">
                <a href="https://paydaysale.id/?utm_source=Ecommerce%2C%20Source"><img src="{{ URL::asset('/img/payday/lockebanner2.png') }}" class="img-responsive"></a>
            </div>
            <div class="col-md-8">
                <a href="https://paydaysale.id/?utm_source=Ecommerce%2C%20Source"><img src="{{ URL::asset('/img/payday/banner.png') }}" class="img-responsive"></a>
            </div>
        </div>
    </div>
</section>

<section class="" id="topup">
    <div class="container">
	<a href="https://popsend.popbox.asia/">
        <div class="row bg-topup">
            <div class="col-md-6 col-sm-12 col-xs-12 topup-content">
                <div>
                    <span class="topup-white-label">Top Up</span> <span class="topup-point">100.000</span><br>
                    <span class="topup-white-label">dapat bonus</span> <span class="topup-point">25.000</span><br>
                    <img src="{{ URL::asset('/img/payday/15_promo-code-top-up.png') }}" class="img-responsive">
                    <span class="topup-white-term hidden-lg hidden-md">* Syarat & ketentuan berlaku </span>
                </div>
            </div>
            <div class="col-md-3 term hidden-xs hidden-sm">
                * Syarat & ketentuan berlaku
            </div>
        </div>
	</a>
    </div>
</section>

<!--harbolnas-->
<section class="" id="syarat">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-part-title">
                <h4>Syarat & Ketentuan Disc. 90%</h4>
                <hr class="hr-border">
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 harbolnas-pad-bot-10">
            </div>
            <div class="col-md-6 harbolnas-pad-bot-10">
                <p class="text-center">
                    - Periode 25 Maret - 2 April 2018<br>
                    - Maksimal potongan 20.000<br>
                    - 1 akun hanya bisa menggunakan 5x<br>
                    - Khusus transaksi menggunakan popbox aplikasi<br>
                    - Berlaku untuk pengiriman ke locker dan alamat<br>
                </p>
            </div>
        </div>
    </div>
</section>

<!--merchant-->
<section class="bg-faded bg-gray" id="merchant">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4 bg-hand">

                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12 section-title">
                                <h4>Cara Pakai PopSend</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-1 col-sm-2 col-xs-2"><span class="bullet-detail active">1</span></div>
                                    <div class="col-md-11 col-sm-10 col-xs-10"><strong>Isi detail order & pilih lokasi alamat atau loker terdekatmu</strong></div>
                                </div>
                                <div class="row hidden-xs">
                                    <div class="col-md-1 hidden-xs"><span style="border-left: 1px solid #000000; margin-left: 15px; min-height: 150px;"> &nbsp; </span></div>
                                    <div class="col-md-11 hidden-xs"></div>
                                </div>
                                <div class="row mar-top-15">
                                    <div class="col-md-1 col-sm-2 col-xs-2"><span class="bullet-detail">2</span></div>
                                    <div class="col-md-11 col-sm-10 col-xs-10">Isi detail penerima & detail barang</div>
                                </div>
                                <div class="row hidden-xs">
                                    <div class="col-md-1 hidden-xs"><span style="border-left: 1px solid #000000; margin-left: 15px; min-height: 150px;"> &nbsp; </span></div>
                                    <div class="col-md-11 hidden-xs"></div>
                                </div>
                                <div class="row mar-top-15">
                                    <div class="col-md-1 col-sm-2 col-xs-2"><span class="bullet-detail">3</span></div>
                                    <div class="col-md-11 col-sm-10 col-xs-10"><strong>Masukkan paket ke loker & paket siap dikirim</strong></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-top: 50px;">
                                <h4>Download Now</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-center">
                                <a href="https://play.google.com/store/apps/details?id=asia.popbox.app"><img src="{{ url('/img/payday/11_google-play.png') }}" class="img-responsive"></a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="https://itunes.apple.com/us/app/popbox-asia/id1196265583?ls=1&mt=8"><img src="{{ url('/img/payday/10_app-store.png') }}" class="img-responsive"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!--location-->
<section class="bg-white" id="location">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-part-title">
                <h4>Lokasi PopBox</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 popsend-middle align-center">
                <div id="map" style="width: 100%; height: 300px; margin-bottom: 20px;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <div class="row">
                    <div class="col-md-6">
                        <select name="district" class="form-control" id="district" onchange="getDistrict()">
                            <option value="">SELECT</option>
                            @foreach ($citiesList as $district)
                                <option value="{{ $district }}">{{ $district }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <select name="locker" class="form-control" id="locker" onchange="getLocker()">
                            <option value="">SELECT</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--contact-->
<section id="contact" style="background: #f2f3f5;">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xs-12 align-center-xs">
                <a href="https://www.popbox.asia/" target="_blank" style="cursor: pointer;"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_61.png') }}" class="img-responsive"></a>
            </div>
            <div class="col-md-6 hidden-md hidden-xs hidden-xm">

            </div>
            <div class="col-md-4 col-xs-12 align-right align-center-xs">
                <a href="https://www.facebook.com/pboxasia/" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_64.png') }}"></a>
                <a href="https://www.instagram.com/popbox_asia/" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_66.png') }}"></a>
                <a href="https://twitter.com/PopBox_Asia" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_68.png') }}"></a>
                <a href="https://www.youtube.com/watch?v=0T-SvaXWlBw" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/logo-youtube.png') }}"></a>
            </div>
        </div>
    </div>
</section>

</body>
</html>
