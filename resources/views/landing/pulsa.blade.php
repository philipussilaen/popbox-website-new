@extends('layout.main-article')
@section('meta')
@section('css')
<style type="text/css">
    .divBorder{
       border: 1px solid #000;
    }
    .col_one_third{
        margin: 1%;
    }
    .location-popbox{
        width: 20%;
        cursor: pointer;
    }
    .col_one_third img{
        width: 60%;
    }
    @media (max-width: 450px){
        .col_one_third{
            width: 30%;
            float: left;
        }

        .location-popbox{
            width: 55%;
        }
    }
</style>
@stop
@section('content')
	<section id="content">
		<div class="container clearfix">           
        	<div class="col-full">        		
                <img src="{{ asset('img/promopulsa/banner.png') }}">                
        	</div>
            <div class="col-full">              
                <img src="{{ asset('img/promopulsa/banner2.png') }}">                
            </div>
            <div class="col-full">              
                <img src="{{ asset('img/promopulsa/banner3.png') }}">
            </div>
    	</div>
	</section>
    <section id="content">
        <div class="container clearfix">
            <div class="center" style="margin:4%">                
                <img src="{{ asset('img/promopulsa/lokasi.png') }}" class="location-popbox">
            </div>
            <div class="row location">
               {{--  <div class="col-md-2">
                    <button class="button button-border button-rounded">Urutkan</button>
                </div> --}}
                <div class="col-md-12" id="div-form">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <form id="filter-form">                               
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Kota</label>
                                            <select class="form-control" name="city" id="city">
                                                <option value="all">Semua Kota</option>
                                                @foreach ($cities as $element)
                                                    <option value="{{ $element }}">{{ $element }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tipe Lokasi</label>
                                            <select class="form-control" name="type" id="type">
                                                <option value="all">Semua Tipe</option>
                                                @foreach ($types as $element)
                                                    <option value="{{ $element }}">{{ $element }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>
                            <span class="pull-right">Total Lokasi : <span id="counter">{{ count($lockerList) }}</span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default location">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            @if (!empty($lockerServices[0]))
                                @foreach ($lockerServices[0] as $element)
                                    <?php 
                                        $title = '<div align="center">';
                                        $title.= '<strong style="color:#FF6300;font-size:15px;">'.$element->name.'</strong><br>';
                                        $title.= '<strong>'.$element->address.'</strong><br>';
                                        $title.= $element->address_2+'<br>';
                                        $title.= $element->operational_hours;
                                        $title.= '</div>';
                                        $service = '';
                                        foreach ($element->services as $item) {
                                            if(empty($service)) $service = $item->service;
                                            else $service .= '-'.$item->service;
                                        }
                                    ?>
                                    <h4 class="locker" data-keyword="{{ $element->name }}-{{ $element->address }}-{{ $element->district }}-{{ $element->building_type }}" data-city="{{ $element->district }}" data-type="{{ $element->building_type }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}" data-html="true" data-service = "{{ $service }}">
                                        {{ $element->name }}
                                        @foreach ($element->services as $item)
                                            @if ($item->service=='cop')
                                                <small class="label label-info" style="font-size: 60%;"> {{ $item->service }}</small>
                                            @elseif ($item->service=='emoney')
                                                <small class="label label-warning" style="font-size: 60%;">{{ $item->service }}</small>
                                            @endif
                                        @endforeach
                                    </h4>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-4">
                            @if (!empty($lockerServices[1]))
                                @foreach ($lockerServices[1] as $element)
                                    <?php 
                                        $title = '<div align="center">';
                                        $title.= '<strong style="color:#FF6300;font-size:15px;">'.$element->name.'</strong><br>';
                                        $title.= '<strong>'.$element->address.'</strong><br>';
                                        $title.= $element->address_2+'<br>';
                                        $title.= $element->operational_hours;
                                        $title.= '</div>';
                                        $service = '';
                                        foreach ($element->services as $item) {
                                            if(empty($service)) $service = $item->service;
                                            else $service .= '-'.$item->service;
                                        }
                                    ?>
                                    <h4 class="locker" data-keyword="{{ $element->name }}-{{ $element->address }}-{{ $element->district }}-{{ $element->building_type }}" data-city="{{ $element->district }}" data-type="{{ $element->building_type }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}" data-html="true" data-service = "{{ $service }}">
                                        {{ $element->name }}
                                        @foreach ($element->services as $item)
                                            @if ($item->service=='cop')
                                                <small class="label label-info" style="font-size: 60%;"> {{ $item->service }}</small>
                                            @elseif ($item->service=='emoney')
                                                <small class="label label-warning" style="font-size: 60%;">{{ $item->service }}</small>
                                            @endif
                                        @endforeach
                                    </h4>
                                @endforeach
                            @endif
                        </div>
                         <div class="col-md-4">
                            @if (!empty($lockerServices[2]))
                                @foreach ($lockerServices[2] as $element)
                                    <?php 
                                        $title = '<div align="center">';
                                        $title.= '<strong style="color:#FF6300;font-size:15px;">'.$element->name.'</strong><br>';
                                        $title.= '<strong>'.$element->address.'</strong><br>';
                                        $title.= $element->address_2+'<br>';
                                        $title.= $element->operational_hours;
                                        $title.= '</div>';
                                        $service = '';
                                        foreach ($element->services as $item) {
                                            if(empty($service)) $service = $item->service;
                                            else $service .= '-'.$item->service;
                                        }
                                    ?>
                                    <h4 class="locker" data-keyword="{{ $element->name }}-{{ $element->address }}-{{ $element->district }}-{{ $element->building_type }}" data-city="{{ $element->district }}" data-type="{{ $element->building_type }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}" data-html="true" data-service = "{{ $service }}">
                                        {{ $element->name }}
                                        @foreach ($element->services as $item)
                                            @if ($item->service=='cop')
                                                <small class="label label-info" style="font-size: 60%;"> {{ $item->service }}</small>
                                            @elseif ($item->service=='emoney')
                                                <small class="label label-warning" style="font-size: 60%;">{{ $item->service }}</small>
                                            @endif
                                        @endforeach
                                    </h4>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container clearfix">
            <div class="row col-full center" style="margin-right: 10%;margin-left: 10%">       
                 <span class="col_one_third">                
                    <img src="{{ asset('img/promopulsa/telkomsel1.png') }}">
                 </span>
                 <span class="col_one_third">                
                    <img src="{{ asset('img/promopulsa/indosat1.png') }}">
                 </span>
                 <span class="col_one_third">                
                    <img src="{{ asset('img/promopulsa/xl1.png') }}">
                 </span>
            </div>          
            <div class="row col-full center" style="margin-right: 10%;margin-left: 10%">       
                 <span class="col_one_third">                
                    <img src="{{ asset('img/promopulsa/tri1.png') }}">
                 </span>
                 <span class="col_one_third">                
                    <img src="{{ asset('img/promopulsa/axis1.png') }}">
                 </span>
                 <span class="col_one_third">                
                    <img src="{{ asset('img/promopulsa/smartfren1.png') }}">
                 </span>
            </div>
        </div>
    </section>

@stop
@section('js')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.location').hide();
        $('#filter-form').submit(function(event) {
            event.preventDefault();
        });
        var delay = (function(){
            var timer = 0;
            return function (callback,ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            }
        })();

        $(".location-popbox").click(function(){
            if ($('.location').is(":visible")){
                 $('.location').hide();
            }else{
                 $('.location').show();
            }
            
        })

        $('#filter-form').on('change', function(event) {
            var keyword = $('input[name=keyword]').val();
            var city = $('select[name=city]').val();
            var type = $('select[name=type]').val(); 
            var service =$('select[name=service]').val();

            // reset to show
            $('.locker').hide('fast');

            delay(function(){
               var length = $('.locker:visible').length;
               $('#counter').html(length)
            },1000);
            filterLocker(keyword,city,type,service);
        });

        function filterLocker(keyword,city,type,service){
            // console.log('============');
            $('.locker').filter(function(index) {                
                var dataCity = $(this).data('city');
                var dataType = $(this).data('type');                

                var status = false;                
                var statusCity = false;
                var statusType = false;                
                
                if (city=='all') {
                    statusCity = true;
                } else {
                    if (city == dataCity) statusCity = true;
                }

                if (type=='all') {
                    statusType = true;
                } else {
                    if (type == dataType) statusType = true;
                }

              
                /*console.log(dataKeyword+"=="+dataCity+"=="+dataType);
                console.log(keyword+"=="+city+"=="+type);
                console.log(statusKeyword+"=="+statusCity+"=="+statusType);*/

                if (statusCity == true && statusType == true) status = true;
                return status;
            }).show('fast');
        }

        /*$('input[name=keyword]').on('keyup', function(event) {
            delay(function(){
                $('input[name=keyword]').trigger('change');
            },1000);
        }); */
    });
</script>
@stop