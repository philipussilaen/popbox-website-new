@extends('layout.main-article')
@section('css')

<style type="text/css">
    .divBorder{
       border: 1px solid #000;
    }
    .plus{
        font-size: 59px;
        font-weight: bold;
        line-height: 57px;
    }

    .price{
        margin-top: 47px;
        font-size: 35px;
    }
    .rounded{        
        color: #FFF;
        border-radius: 50%;
        width: 100px;
        height: 100px;
        text-align: center;
        padding-top: 26px;
        position: absolute;
        top: -60px;
        z-index: 1000;
        margin: 0 auto;        
        left: 0;
        right: 0;
        margin-left: auto;
        margin-right: auto;
    } 
    .rounded.blue{
        background: #002dbb;
    }
    .box{
        border-radius: 16px;        
        margin-bottom: 10px;
        height: 200px;
        color:#000;
    }
    .box.blue{
        border: 1px solid #002dbb;
        box-shadow: 0px 6px 0px #002dbb;
    }

    .box.blue .plus{
        color: #002dbb;
    }
    .wrong{
        font-size: 23px;
        line-height: 7px;
        margin-top: 56px;
        color:red;
        text-decoration:line-through;
    }

    .wrong.paket{
        line-height: 37px;
    }

    .wrong .curr{
        top: -9px;
        font-size: 15px;
    }
    .batas-promo{
        background-color: #fff200;
        border-radius: 14px;
        width: 41%;
        margin: 0 auto;
        color: #000;
        font-size: 34px;
        font-weight: bold;
        line-height: 41px;
        padding: 5px;
    }

    .batas-promo .note{
        font-size: 30px;
        font-weight: 100;
    }

    .table-head{
        background-color: #034EA1;
        color: #fff;
        font-size: 125%;
        text-align: center;
    }
    .div-content{
        margin-top: 25px;
    }
</style>
@stop
@section('content')
    {{-- Banner --}}
    <section id="content" class="div-content">
        <div class="container clearfix">
            <div class="row">               
                <div class="col-md-12">
                    <img src="/landing/img/promo-pulsa/promo-feb.png"/>                
                </div>
            </div> 
        </div>
    </section>
    {{-- Promo  --}}
    <section id="content" class="div-content">
        <div class="container clearfix">
            <div class="heading-block center">
                <h2>PAKET XTRA COMBO 12 BULAN</h2>
            </div>
            <div class="center">
                <h3>
                    Kamu biasanya beli paket data bulanan? Yuk coba <strong>BELI SEKALI PAKAI SETAHUN!</strong><br>
                    Selain lebih praktis sekarang lebih hemat, karena diskon hingga 22%.
                </h3>
            </div>
            <div class="center">
                <table class="table table-bordered">
                    <thead>
                        <tr class="center">
                            <th class="table-head">Paket Data</th>
                            <th class="table-head">Harga Normal</th>
                            <th class="table-head">Harga Diskon</th>
                        </tr>
                    </thead>
                    <tbody style="font-size: 125%;">
                        <tr>
                            <td><strong>XTRA Combo M 12X 6GB</strong></td>
                            <td><s>588,000</s></td>
                            <td><strong>460,000</strong></td>
                        </tr>
                        <tr>
                            <td><strong>XTRA Combo L 12X 12GB</strong></td>
                            <td><s>868,000</s></td>
                            <td><strong>690,000</strong></td>
                        </tr>
                        <tr>
                            <td><strong>XTRA Combo XL 12X 18GB</strong></td>
                            <td><s>1,248,000</s></td>
                            <td><strong>1,010,000</strong></td>
                        </tr>
                        <tr>
                            <td><strong>XTRA Combo 2XL 12X 30GB</strong></td>
                            <td><s>1,628,000</s></td>
                            <td><strong>1,350,000</strong></td>
                        </tr>
                        <tr>
                            <td><strong>XTRA Combo 3XL 12X 42GB</strong></td>
                            <td><s>2,168,000</s></td>
                            <td><strong>1,818,000</strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            {{-- <div class="row" style="margin-top: 8%">
                <div class="col-md-3">
                    <div class="rounded blue">
                        XTRA Combo M 12X<br>
                        (6GB)
                    </div>
                    <div class="box blue paket">
                        <div class="center price">
                            <div class="wrong paket">
                                <span style='color:black'><span class="curr">Rp</span><span>588,000</span></span>
                            </div>
                            <span class="curr">Rp</span><span>460,000</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="rounded blue">
                        XL<br/>
                        (18GB)
                    </div>
                    <div class="box blue paket">
                        <div class="center price">
                            <div class="wrong paket">
                                <span style='color:black'><span class="curr">Rp</span><span>129.000</span></span>
                            </div>
                            <span class="curr">Rp</span><span>99.000</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="rounded blue">
                        2XL<br/>
                        30GB
                    </div>
                    <div class="box blue paket">
                        <div class="center price">
                            <div class="wrong paket">
                                <span style='color:black'><span class="curr">Rp</span><span>179.000</span></span>
                            </div>
                            <span class="curr">Rp</span><span>139.000</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="rounded blue">
                        3XL<br/>
                        42GB
                    </div>
                    <div class="box blue paket">
                        <div class="center price">
                            <div class="wrong paket">
                                <span style='color:black'><span class="curr">Rp</span><span>239.000</span></span>
                            </div>
                            <span class="curr">Rp</span><span>189.000</span>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </section>
    {{-- Periode --}}
    {{-- <section id="content" class="div-content">
        <div class="container clearfix">
            <div class="heading-block center">
                <h2>Periode</h2>
            </div>
            <div class="row">
                <div class="col-md-12 center">
                    <div class="batas-promo">
                        8 - 28 Februari 2018
                    </div> 
                    <small>* syarat dan ketentuan berlaku</small>
                </div> 
            </div>
        </div>
    </section> --}}
    {{-- Syarat --}}
    <section id="content" class="div-content">
        <div class="container clearfix">
            <div class="heading-block center">
                <h2>Syarat dan Ketentuan</h2>
            </div>
            <div class="row">
                <div class="col-md-12" style="font-size: 150%;">
                    -Periode promo 8 - 28 Februari 2018 <br>
                    -Transaksi dilakukan di loker PopBox dan pembayaran dengan menggunakan eMoney Mandiri <br>
                    -Pembeli harap memastikan nomor yang dimasukkan benar, kesalahan dalam pengetikkan nomor yang mengakibatkan paket data gagal ditanggung pembeli <br>
                    -PopBox berhak, tanpa pemberitahuan sebelumnya, melakukan tindakan-tindakan yang diperlukan apabila diduga terjadi tindakan kecurangan dari pengguna yang merugikan pihak PopBox <br>
                    -Dengan mengikuti promo ini, pengguna dianggap mengerti dan menyetujui semua syarat & ketentuan berlaku.
                </div> 
            </div>
        </div>
    </section>
    {{-- Cara --}}
    <section id="content" class="div-content">
        <div class="container clearfix">
            <div class="heading-block center">
                <h2>Cara Pembayaran</h2>
            </div>
            <div class="col-full">
                <div class="col_one_third nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('landing/img/promo-pulsa/lp1.png') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Pilih Pembayaran</h3>
                        </div>
                    </div>
                </div>
                <div class="col_one_third nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('landing/img/promo-pulsa/lp2.png') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Pilih isi pulsa</h3>
                        </div>
                    </div>
                </div>
                <div class="col_one_third col_last nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('landing/img/promo-pulsa/lp3.png') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Masukkan No HP</h3>
                        </div>
                    </div>
                </div>

                <div class="col_one_third nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('landing/img/promo-pulsa/lp4.jpg') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Pilih Paket Data</h3>
                        </div>
                    </div>
                </div>
                <div class="col_one_third nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('landing/img/promo-pulsa/lp5.png') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Pilih konfirmasi </h3>
                        </div>
                    </div>
                </div>
                <div class="col_one_third col_last nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('landing/img/promo-pulsa/lp6.png') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Pilih Pembayaran E-Money</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </section>
    {{-- Lokasi --}}
    <section id="content">
        <div class="container clearfix">
            <div class="heading-block center">
                <h2>Lokasi Pembayaran</h2>
            </div>
            <div class="row">
               {{--  <div class="col-md-2">
                    <button class="button button-border button-rounded">Urutkan</button>
                </div> --}}
                <div class="col-md-12" id="div-form">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <form id="filter-form">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Kata Kunci</label>
                                            <input type="text" name="keyword" id="keyword" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Kota</label>
                                            <select class="form-control" name="city" id="city">
                                                <option value="all">Semua Kota</option>
                                                @foreach ($cities as $element)
                                                    <option value="{{ $element }}">{{ $element }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tipe Lokasi</label>
                                            <select class="form-control" name="type" id="type">
                                                <option value="all">Semua Tipe</option>
                                                @foreach ($types as $element)
                                                    <option value="{{ $element }}">{{ $element }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tipe Layanan</label>
                                            <select class="form-control" name="service" id="service">
                                                <option value="all">Semua Layanan</option>
                                                <option value="cop">COP</option>
                                                <option value="emoney">eMoney</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <span class="pull-right">Total Lokasi : <span id="counter">{{ count($lockerList) }}</span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            @if (!empty($lockerServices[0]))
                                @foreach ($lockerServices[0] as $element)
                                    <?php 
                                        $title = '<div align="center">';
                                        $title.= '<strong style="color:#FF6300;font-size:15px;">'.$element->name.'</strong><br>';
                                        $title.= '<strong>'.$element->address.'</strong><br>';
                                        // $title.= $element->address_2+'<br>';
                                        $title.= $element->operational_hours;
                                        $title.= '</div>';
                                        $service = '';
                                        foreach ($element->services as $item) {
                                            if(empty($service)) $service = $item->service;
                                            else $service .= '-'.$item->service;
                                        }
                                    ?>
                                    <h4 class="locker" data-keyword="{{ $element->name }}-{{ $element->address }}-{{ $element->district }}-{{ $element->building_type }}" data-city="{{ $element->district }}" data-type="{{ $element->building_type }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}" data-html="true" data-service = "{{ $service }}">
                                        {{ $element->name }}
                                        @foreach ($element->services as $item)
                                            @if ($item->service=='cop')
                                                <small class="label label-info" style="font-size: 60%;"> {{ $item->service }}</small>
                                            @elseif ($item->service=='emoney')
                                                <small class="label label-warning" style="font-size: 60%;">{{ $item->service }}</small>
                                            @endif
                                        @endforeach
                                    </h4>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-4">
                            @if (!empty($lockerServices[1]))
                                @foreach ($lockerServices[1] as $element)
                                    <?php 
                                        $title = '<div align="center">';
                                        $title.= '<strong style="color:#FF6300;font-size:15px;">'.$element->name.'</strong><br>';
                                        $title.= '<strong>'.$element->address.'</strong><br>';
                                        // $title.= $element->address_2+'<br>';
                                        $title.= $element->operational_hours;
                                        $title.= '</div>';
                                        $service = '';
                                        foreach ($element->services as $item) {
                                            if(empty($service)) $service = $item->service;
                                            else $service .= '-'.$item->service;
                                        }
                                    ?>
                                    <h4 class="locker" data-keyword="{{ $element->name }}-{{ $element->address }}-{{ $element->district }}-{{ $element->building_type }}" data-city="{{ $element->district }}" data-type="{{ $element->building_type }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}" data-html="true" data-service = "{{ $service }}">
                                        {{ $element->name }}
                                        @foreach ($element->services as $item)
                                            @if ($item->service=='cop')
                                                <small class="label label-info" style="font-size: 60%;"> {{ $item->service }}</small>
                                            @elseif ($item->service=='emoney')
                                                <small class="label label-warning" style="font-size: 60%;">{{ $item->service }}</small>
                                            @endif
                                        @endforeach
                                    </h4>
                                @endforeach
                            @endif
                        </div>
                         <div class="col-md-4">
                            @if (!empty($lockerServices[2]))
                                @foreach ($lockerServices[2] as $element)
                                    <?php 
                                        $title = '<div align="center">';
                                        $title.= '<strong style="color:#FF6300;font-size:15px;">'.$element->name.'</strong><br>';
                                        $title.= '<strong>'.$element->address.'</strong><br>';
                                        // $title.= $element->address_2+'<br>';
                                        $title.= $element->operational_hours;
                                        $title.= '</div>';
                                        $service = '';
                                        foreach ($element->services as $item) {
                                            if(empty($service)) $service = $item->service;
                                            else $service .= '-'.$item->service;
                                        }
                                    ?>
                                    <h4 class="locker" data-keyword="{{ $element->name }}-{{ $element->address }}-{{ $element->district }}-{{ $element->building_type }}" data-city="{{ $element->district }}" data-type="{{ $element->building_type }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}" data-html="true" data-service = "{{ $service }}">
                                        {{ $element->name }}
                                        @foreach ($element->services as $item)
                                            @if ($item->service=='cop')
                                                <small class="label label-info" style="font-size: 60%;"> {{ $item->service }}</small>
                                            @elseif ($item->service=='emoney')
                                                <small class="label label-warning" style="font-size: 60%;">{{ $item->service }}</small>
                                            @endif
                                        @endforeach
                                    </h4>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('js')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#filter-form').submit(function(event) {
            event.preventDefault();
        });
        var delay = (function(){
            var timer = 0;
            return function (callback,ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            }
        })();

        $('#filter-form').on('change', function(event) {
            var keyword = $('input[name=keyword]').val();
            var city = $('select[name=city]').val();
            var type = $('select[name=type]').val(); 
            var service =$('select[name=service]').val();

            // reset to show
            $('.locker').hide('fast');

            delay(function(){
               var length = $('.locker:visible').length;
               $('#counter').html(length)
            },1000);
            filterLocker(keyword,city,type,service);
        });

        function filterLocker(keyword,city,type,service){
            // console.log('============');
            $('.locker').filter(function(index) {
                var dataKeyword = $(this).data('keyword');
                var dataCity = $(this).data('city');
                var dataType = $(this).data('type');
                var dataService = $(this).data('service');

                var status = false;
                var statusKeyword = false;
                var statusCity = false;
                var statusType = false;
                var statusService = false;

                if (keyword=='') {
                    statusKeyword = true;
                } else {
                    if (dataKeyword.toLowerCase().indexOf(keyword) >=0) statusKeyword = true;
                }
                if (city=='all') {
                    statusCity = true;
                } else {
                    if (city == dataCity) statusCity = true;
                }

                if (type=='all') {
                    statusType = true;
                } else {
                    if (type == dataType) statusType = true;
                }

                if (service=='all') {
                    statusService = true;
                } else {
                    if (dataService.toLowerCase().indexOf(service) >=0) statusService = true;
                }
                /*console.log(dataKeyword+"=="+dataCity+"=="+dataType);
                console.log(keyword+"=="+city+"=="+type);
                console.log(statusKeyword+"=="+statusCity+"=="+statusType);*/

                if (statusKeyword == true && statusCity == true && statusType == true && statusService == true) status = true;
                return status;
            }).show('fast');
        }

        /*$('input[name=keyword]').on('keyup', function(event) {
            delay(function(){
                $('input[name=keyword]').trigger('change');
            },1000);
        }); */
    });
</script>
@stop