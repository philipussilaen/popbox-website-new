<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="{{ asset('img/favicon.ico') }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>PopBox Gramedia</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link href="{{ asset('landing/css/bootstrap.css') }}" rel="stylesheet" />
        <link href="{{ asset('landing/css/landing-page.css') }}" rel="stylesheet"/>

        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Baloo+Bhaina" rel="stylesheet">
        <link href="{{ asset('landing/css/pe-icon-7-stroke.css') }}" rel="stylesheet" />
         <link rel="stylesheet" href="{{ elixir('css/custom.css') }}">

        <style type="text/css">
        	.landing-page .section-features .card{
                min-height: 350px;
                padding-bottom: 10px;
        	}
        </style>

    </head>
    <body class="landing-page landing-page1">
        <div class="wrapper">
            {{-- <div class="parallax filter-gradient">
                <div class="parallax-background">
                    <img class="parallax-background-image" src="{{ asset('landing/img/gramed/cover.png') }}">
                </div>
                <div class= "container">
                    <div class="row">
                        <div class="col-md-5 hidden-xs">
                            <div class="parallax-image">
                                <img class="phone" src="{{ asset('landing/img/gramed/phone.png') }}" style="margin-top: 20px"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-1">
                            <div class="description">
                                <h3>TAMBAH KOLEKSI BUKU DI</h3>
                                <h1 style="font-family:Baloo Bhaina">Gramedia.com</h1>
                                <h3>KIRIM KE LOKER POPBOX AJA</h3>
                                <a href="http://www.gramedia.com/"><button class="btn" style="background-color: #FFFFFF">Belanja Sekarang</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="section">
                <div class="container">
                    <h4 class="header-text text-center">APA ITU POPBOX?</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center">
                                PopBox adalah sistem loker otomatis dimana Anda bisa mengambil sendiri barang belanjaan Anda, 
                                <br> kapanpun dan dimanapun. <br>
                                {{-- <b>PopBox membantu pengiriman paket Anda dengan mudah, aman, cepat dan nyaman.</b> --}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-features" style="margin-top: 0px;">
                <div class="">
                    <h4 class="header-text text-center">CARA BELANJA DI GRAMEDIA ONLINE KIRIM VIA POPBOX</h4>
                    <div class="row">
                        <div class="col-md-2 col-md-offset-1">
                            <div class="card card-blue">
                                <div class="icon">
                                    <img src="{{ asset('landing/img/gramed/cara-1.png') }}" style="width: 75%;">
                                </div>
                                <div class="text">
                                    <h4>Belanja di Gramedia, pilih PopBox untuk alamat pengiriman</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card card-blue">
                                <div class="icon">
                                    <img src="{{ asset('landing/img/gramed/cara-2.png') }}" style="width: 75%;">
                                </div>
                                <h4>Pilih lokasi PopBox terdekat</h4>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card card-blue">
                                <div class="icon">
                                    <img src="{{ asset('landing/img/gramed/cara-3.png') }}" style="width: 75%;">
                                </div>
                                <h4>Barang Anda akan dikirimkan oleh PopBox</h4>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card card-blue">
                                <div class="icon">
                                    <img src="{{ asset('landing/img/gramed/cara-4.png') }}" style="width: 75%;">
                                </div>
                                <h4>Barang sampai, Anda akan menerima SMS kode untuk membuka loker</h4>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card card-blue">
                                <div class="icon">
                                    <img src="{{ asset('landing/img/gramed/cara-5.png') }}" style="width: 75%;">
                                </div>
                                <h4>Ambil paket Anda dan enjoy!</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="section section-testimonial">
                <div class="container">
                    <h4 class="header-text text-center">CARA PENGIRIMAN VIA POPBOX</h4>
                    <img src="{{ asset('landing/img/gramed/checkout.jpeg') }}" style="width: 100%;">
                </div>
            </div> --}}
            {{-- <footer class="footer">
                <div class="container">
                    <div class="social-area pull-right">
                        <a class="btn btn-social btn-facebook btn-simple" href="https://www.facebook.com/pboxasia" target="_blank">
                        	<i class="fa fa-facebook-square"></i>
                        </a>
                        <a class="btn btn-social btn-twitter btn-simple" href="https://twitter.com/popbox_asia" target="_blank">
                        	<i class="fa fa-twitter"></i>
                        </a>
                        <a class="btn btn-social btn-instagram btn-simple" href="https://www.instagram.com/popbox_asia/" target="_blank">
                        	<i class="fa fa-instagram"></i
>                        </a>
                    </div>
                    <div class="copyright">
                        &copy; 2017 <a href="https://www.popbox.asia/">PopBox Asia Service</a>
                    </div>
                </div>
            </footer> --}}
        </div>

    </body>
    <script src="{{ asset('landing/js/jquery-1.10.2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('landing/js/jquery-ui-1.10.4.custom.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('landing/js/bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('landing/js/awesome-landing-page.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ elixir('js/jquery.gmap.js') }}"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA87ADAAfFte0kA7HqOHz5UFRRiYvWSKk4&libraries=places"></script>
    {{-- <script type="text/javascript">
         $(window).load(function() {
            if ($(window).width() > 480) {
                $('#div-youtube').html('<iframe style="width:560px;height:315px" src="https://www.youtube.com/embed/opMN8fJV7BI" frameborder="0" allowfullscreen></iframe> ');
            };
        });
    </script> --}}
    <script type="text/javascript">
        var lockerList = {!! json_encode($lockerJkt) !!};
    </script>
    <script type="text/javascript">
        var markers = Array();
        var url_img = "{{ asset('img/icon/ic_box_map.svg') }}";
        var newLoc = false;

        $.each(lockerList,function (key,value) {
            var html = '<div align="center">';
            html += '<strong style="color:#FF6300;font-size:15px;">'+value.name+'</strong><br>';
            html += '<strong>'+value.address+'</strong><br>';
            html += value.address_2+'<br>';
            html += value.operational_hours;
            html+='</div>';
            var iconMarker = '{{ asset('img/map/marker-locker.png') }}'

            var locker = {
                latitude : value.latitude,
                longitude : value.longitude,
                html : html,
                icon : {
                    image : iconMarker,
                    iconsize: [30, 49],
                    shadowsize: [37, 34],
                    iconanchor: [9, 34],
                    shadowanchor: [19, 34]

                }
           }
           markers.push(locker);
        });
		$('#nearest_locker').gMap({
            address: '{{ trans('index.maps-location') }}',
            maptype: 'ROADMAP',
            zoom: {{ trans('index.maps-zoom') }},
            markers: markers,
            doubleclickzoom: false,
            controls: {
                panControl: true,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: true,
                streetViewControl: false,
                overviewMapControl: true
            }
        }).gMap('addSearchBox',{
            box : 'pac-input'
        }).gMap('addBtn',{
            location : '{{ trans('index.maps-location') }}',
            text : '{{ trans('index.location-map-btn') }}',
            zoom : {{ trans('index.maps-zoom') }}
        })
        $(document).ready(function() {
            $('button[class*=nearme]').on('click', function(event) {
               nearestLock();
            });

            $(".btn-register").click(function(){
                $('html, body').animate({ scrollTop: $('#registration').offset().top }, 'slow');
             })
        });
        

        function nearestLock(){
            var iconPosition = '{{ asset('img/map/market-youarehere.png') }}'
            if (navigator.geolocation) {
                if (newLoc==false) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        jQuery('#nearest_locker').gMap('addMarker', {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude,
                            icon : {
                                image : iconPosition,
                                iconsize: [40, 64],
                                iconanchor: [9, 34],
                            },
                        })
                    })
                }
                navigator.geolocation.getCurrentPosition(function(position) {
                    jQuery('#nearest_locker').gMap('centerAt', {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        zoom: 14
                    });
                    newLoc = true;
                }, function() {
                    console.log('Failed Geo Location')
                });
            }
        }

        $(document).ready(function() {

            $('#iFindLocker').on('click', function(event) {
                if ($('#divFindLocker').hasClass('hide')) {
                    $('#divFindLocker').removeClass('hide');
                    $('#iFindLocker').removeClass('icon-angle-down');
                    $('#iFindLocker').addClass('icon-angle-up');

                    // $('#iFindLocker').removeClass('fadeInDown');
                    // $('#iFindLocker').addClass('fadeInUp');
                } else {
                    $('#divFindLocker').addClass('hide');
                    $('#iFindLocker').removeClass('icon-angle-up');
                    $('#iFindLocker').addClass('icon-angle-down');

                    // $('#iFindLocker').removeClass('fadeInUp');
                    // $('#iFindLocker').addClass('fadeInDown');
                }
            });

            $('select[id=mapCity]').on('change', function(event) {
                /* Act on the event */
                $('select[name=lockerFind]').html('');
                $.ajax({
                    url: '{{ url('lockerList') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: $('#mapLocker').serialize(),
                    success: function(data){
                        if (data.isSuccess==true) {
                            var lockerList = data.data;
                            var option = '';
                            $.each(lockerList, function(index, val) {
                                 /* iterate through array or object */
                                 var address = val.address;
                                 var address_2 = val.address_2;
                                 var district = val.district+', '+val.province;
                                 option += "<option value='"+val.name+"' lat='"+val.latitude+"' long='"+val.longitude+"'>"+val.name+"</option>"
                            });
                            $('select[name=lockerFind]').html(option);
                            $('select[name=lockerFind]').trigger('change');
                        } else {
                            $('#errorMsg').html(data.errorMsg);
                        }
                    }
                })
                .done(function() {
                    console.log("success");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });         
            });   

             $('select[name=lockerFind]').on('change', function(event) {
                /* Act on the event */
                $('#lockerSize').addClass('hide');
                $('#lockerAddress').addClass('hide');
                var locker = $(this).find(':selected');
                var lat = locker.attr('lat');
                var long = locker.attr('long');
                jQuery('#nearest_locker').gMap('centerAt', {
                    latitude: lat,
                    longitude: long,
                    zoom: 16
                });
            });
        });
	</script>
</html>
