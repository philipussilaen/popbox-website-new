@extends('layout.main')
@section('content')
	{{-- Section Slider One --}}
	<section id="slider" class="slider-parallax full-screen with-header swiper_wrapper clearfix" data-autoplay="7000" data-speed="650" data-loop="true">
        <div class="slider-parallax-inner">
            <div class="swiper-container swiper-parent">
                <div class="swiper-wrapper">
                    @if (!empty($sliders))
                        @foreach ($sliders as $key=>$element)
                            <div class="swiper-slide dark" style="background-image: url('{{\App\Http\Helper\Helper::createImgLocal('image',$element['image'])}}');">
                                @if ($element['type'] =='promo')
                                    <a href="{{ $element['desc'] }}" target="_blank">
                                        <div class="container clearfix">
                                        </div>
                                    </a>
                                @else
                                    <div class="container clearfix">
                                        <div class="slider-caption slider-caption-center">
                                            <div class="row">
                                                <img data-animate="fadeIn" src="img/icon/box.png" class="divcenter bottommargin hidden-xs hidden-sm">
                                            </div>
                                            <p data-animate="fadeInUp" data-delay="50"><span class="backTitle">{{ $element['desc'] }}</span></p>
                                           
                                           @if  ($region == 'id')
                                            <div class="tp-caption customin ltl tp-resizeme"
                                            data-x="0"
                                            data-y="450"
                                            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
                                            data-speed="800"
                                            data-start="1550"
                                            data-easing="easeOutQuad"
                                            data-splitin="none"
                                            data-splitout="none"
                                            data-elementdelay="0.01"
                                            data-endelementdelay="0.1"
                                            data-endspeed="1000"
                                            data-endeasing="Power4.easeIn" style="z-index: 3;" data-animate="fadeInUp" data-delay="50">
                                                <!-- <a href="{{ url('popboxagent') }}" class="button button-3d button-black button-large button-rounded tright nomargin" style="color: #FFF;"><span>Popbox Agent</span> <i class="icon-angle-right"></i></a> -->
                                                {!! trans('index.slider-btn') !!}
                                            </div>
                                          	@endif  
                                            
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    @endif
                    {{-- <div class="swiper-slide dark">
                        <div class="container clearfix">
                            <div class="slider-caption slider-caption-center">
                                <img data-animate="fadeIn" src="img/icon/box.png" class="divcenter bottommargin">
                                <h2 data-animate="fadeInUp">{{ trans('index.tag') }}</h2>
                                <p data-animate="fadeInUp" data-delay="100">{{ trans('index.tag2') }}</p>
                            </div>
                        </div>
                        <div class="video-wrap">
                            <video poster="{{ asset('img/videos/explore.jpg') }}" preload="auto" loop autoplay muted>
                                <source src='{{ asset('img/videos/explore.mp4') }}' type='video/mp4' />
                                <source src='{{ asset('img/videos/explore.webm') }}' type='video/webm' />
                            </video>
                            <div class="video-overlay" style="background-color: rgba(0,0,0,0.2);"></div>
                        </div>
                    </div> --}}
                </div>
                <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
                <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
                <div id="slide-number">
                    <div id="slide-number-current"></div><span>/</span>
                    <div id="slide-number-total"></div>
                </div>
            </div>
        </div>
    </section>
    {{-- Section Slider One End --}}
    {{-- Section About PopBox --}}
    {{-- <section id="content">
        <div class="content-wrap nobottommargin">
            <div class="container clearfix">
                <div id="section-about" class="page-section">
                    <div class="row clearfix">
                        <div class="col-md-6 center">
                            <img src="img/apaitupopbox.png " style="width: 70%" alt="Apa Itu PopBox " data-animate="fadeInLeft">
                        </div>
                        <div class="col-md-6">
                            <div class="topmargin-lg hidden-xs hidden-sm"></div>
                            <div class="emphasis-title bottommargin-sm">
                                <h2 style="font-size: 35px;" class="font-body ls1 t400">{{ trans('index.whats') }}</h2>
                            </div>
                            <p style="color: #000;" class="lead">
                               {{ trans('index.whats2') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
    {{-- Section About PopBox End --}}
    {{-- Section Layanan --}}
    <section id="section-services" class="section-full">
        <div class="container clearfix ">
            <div class="heading-block center">
                <h2>{{ trans('index.services-title') }}</h2>
                <span>{{ trans('index.services-sub') }}</span>
            </div>
            <div class="col_full">
                <!-- <div class="col_one_third nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <img src="{{ asset('img/services/01.png') }}" alt="{{ trans('index.services-1') }}">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center"> {{ trans('index.services-1') }}</h3>
                        </div>
                        <p style="font-size: 16px; color: #444;" class="ls1 center t300">
                            {{ trans('index.services-1-sub') }}
                        </p>
                    </div>
                </div>
                <div class="col_one_third  nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <img src="{{ asset('img/services/02.png') }}" alt="{{ trans('index.services-2') }}">
                        </div>

                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">{{ trans('index.services-2') }}</h3>
                        </div>
                        <p style="font-size: 16px; color: #444;" class="ls1 center t300">
                            {{ trans('index.services-2-sub') }}
                        </p>
                    </div>
                </div>
                <div class="col_one_third col_last nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <a href="{{ url('cop') }}"><img src="{{ asset('img/services/03.png') }}" alt="{{ trans('index.services-2') }}"></a>
                        </div>

                        <div class="emphasis-title bottommargin-sm">
                            <a href="{{ url('cop') }}"><h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">{{ trans('index.services-3') }}</h3></a>
                        </div>
                        <p style="font-size: 16px; color: #444;" class="ls1 center t300">
                            {{ trans('index.services-3-sub') }}
                        </p>
                    </div>
                </div> -->
                <div class="col_one_fourth nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <img src="{{ asset('img/services/01.png') }}" alt="{{ trans('index.services-1') }}">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center"> {{ trans('index.services-1') }}</h3>
                        </div>
                        <p style="font-size: 16px; color: #444; text-align: center;" class="ls1 t300"> <!--class="ls1 center t300"-->
                            {{ trans('index.services-1-sub') }}
                        </p>
                    </div>
                </div>
                <div class="col_one_fourth  nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <a href="{{ url('return') }}"><img src="{{ asset('img/services/02.png') }}" alt="{{ trans('index.services-2') }}"></a>
                        </div>

                        <div class="emphasis-title bottommargin-sm">
                            <a href="{{ url('return') }}"><h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">{{ trans('index.services-2') }}</h3></a>
                        </div>
                        <p style="font-size: 16px; color: #444; text-align: center;" class="ls1 t300">
                            {{ trans('index.services-2-sub') }}
                        </p>
                    </div>
                </div>
                <div class="col_one_fourth  nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <a href="https://popsend.popbox.asia/"><img src="{{ asset('img/services/03.png') }}" alt="{{ trans('index.services-2') }}"></a>
                        </div>

                        <div class="emphasis-title bottommargin-sm">
                            <a href="https://popsend.popbox.asia/"><h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">{{ trans('index.services-3') }}</h3></a>
                        </div>
                        <p style="font-size: 16px; color: #444; text-align: center;" class="ls1 t300">
                            {{ trans('index.services-3-sub') }}
                        </p>
                    </div>
                </div>
                <div class="col_one_fourth col_last nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <a href="https://popsend.popbox.asia/"><img src="{{ asset('img/services/04.png') }}" alt="{{ trans('index.services-2') }}"></a>
                        </div>

                        <div class="emphasis-title bottommargin-sm">
                            <a href="https://popsend.popbox.asia/"><h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">{{ trans('index.services-4') }}</h3></a>
                        </div>
                        <p style="font-size: 16px; color: #444; text-align: center;" class="ls1 t300">
                            {{ trans('index.services-4-sub') }}
                        </p>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="container clearfix" style="padding-left: 20px;padding-right: 20px;">
            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">{{ trans('index.services-sub2') }}</h3>
            <div class="clear"></div>

            <div class="col_full">
                <div class="col_one_third  nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('img/icon/01.png') }}" alt="{{ trans('index.services-4') }}" style="width: 50%; border-radius: 50%;">
                        </div>

                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">{{ trans('index.services-5') }}</h3>
                        </div>
                        <p style="font-size: 16px; color: #444;" class="ls1 center t300">
                            {!! trans('index.services-5-sub') !!}
                        </p>
                    </div>
                </div>

                <div class="col_one_third  nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <a href="https://popsend.popbox.asia/"><img src="{{ asset('img/icon/02.png') }}" alt="{{ trans('index.services-5') }}" style="width: 50%; border-radius: 50%;"></a>
                        </div>

                        <div class="emphasis-title bottommargin-sm">
                            <a href="https://popsend.popbox.asia/"><h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">{{ trans('index.services-6') }}</h3></a>
                        </div>
                        <p style="font-size: 16px; color: #444;" class="ls1 center t300">
                            {{ trans('index.services-6-sub') }}
                        </p>
                    </div>
                </div>

                <!-- <div class="col_one_fourth nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <a href="http://shop.popbox.asia/"><img src="{{ asset('img/icon/02.png') }}" alt="{{ trans('index.services-6') }}" style="width: 50%"></a>
                        </div>

                        <div class="emphasis-title bottommargin-sm">
                            <a href="http://shop.popbox.asia/"><h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">{{ trans('index.services-6') }}</h3></a>
                        </div>
                        <p style="font-size: 16px; color: #444;" class="ls1 center t300">
                            {{ trans('index.services-6-sub') }}
                        </p>
                    </div>
                </div> -->

                <div class="col_one_third col_last  nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <a href="{{ url('ondemand') }}"><img src="{{ asset('img/icon/03.png') }}" alt="{{ trans('index.services-7') }}" style="width: 50%; border-radius: 50%;"></a>
                        </div>

                        <div class="emphasis-title bottommargin-sm">
                            <a href="{{ url('ondemand') }}"><h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">{{ trans('index.services-7') }}</h3></a>
                        </div>
                        <p style="font-size: 16px; color: #444;" class="ls1 center t300">
                            {{ trans('index.services-7-sub') }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="container clearfix">
            <div class="heading-block center" id="div-youtube">
                
            </div>
        </div>
    </section>
    {{-- Section Locker Nearest --}}
    <section id="section-location" class="section-full">
        <div class="heading-block center">
            <h2>{{ trans('index.location-title') }}</h2>
            {{-- <span>{{ trans('index.location-sub') }}</span> --}}
            {{-- <a href="https://location.popbox.asia/"> --}}
            <button class="button button-circle nearme">{{ trans('index.location-btn') }}</button><br>
            <span id="btnFindLocker"><i class="icon-angle-down infinite animated" id="iFindLocker" style="font-size: 150%"></i></span>
            {{-- <button class="btn btn-sm" id="btnFindLocker"></button> <br> <br> --}}
            {{-- </a> --}}
            <div class="row hide" id="divFindLocker">
                <form id="mapLocker">
                    {{ csrf_field() }}
                    <div class="hidden-xs hidden-sm col-md-2 col-lg-2"></div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <select class="form-control" id="mapCity" name="cityName">
                            <option value="">{{ trans('layout.city-select') }}</option>
                            @foreach ($citiesList as $element)
                                <option value="{{ $element }}">{{ $element }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <select class="form-control" name="lockerFind">
                                                
                        </select>
                    </div>
                    <div class="hidden-xs hidden-sm col-md-2 col-lg-2"></div>
                </form>
            </div>
        </div>
        <input id="pac-input" class="gMap-controls" type="text" placeholder="{{ trans('index.location-search') }}">
        <div id="nearest_locker" style="height: 500px; margin-bottom: 20px;" class="gmap"></div>
    </section>

    {{-- Locker Nearest --}}
    {{-- Section Galery  --}}
    <section id="section-gallery" class="section-full">
        <div class="heading-block center" id="registration">
            <h2>{{ trans('index.locker-title') }}</h2>
            <span>{{ trans('index.locker-sub') }}</span>
        </div>
        <div class="page-section nopadding nomargin">
            <div class="owl-carousel owl-carousel-full image-carousel carousel-widget bottommargin" data-margin="20" data-center="true" data-loop="true" data-nav="false" data-pagi="true" data-items-xxs="2" data-items-xs="2" data-items-sm="4" data-items-md="4" data-items-lg="4" data-stage-padding="30" data-lightbox="gallery" data-autoplay="5000" data-speed="650" data-loop="true">
                @foreach ($gallery as $element)
                    <div class="oc-item">
                        <a data-lightbox="gallery-item" href="{{\App\Http\Helper\Helper::createImgLocal('locker/id/new',$element['image'])}}" title="{{ $element['desc'] }}"><img src="{{\App\Http\Helper\Helper::createImgLocal('locker/id/new',$element['image'])}}" alt="locker"></a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    {{-- Section Highlight --}}
    {{-- Section Client  --}}
    <section id="client" class="section-full">
        <div class="heading-block center">
            <h2>{{ trans('index.partner-title') }}</h2>
            <span>{{ trans('index.partner-sub') }}</span>
             <a href="{{ url('merchant') }}">
                <button class="button button-circle">{{ trans('index.partner-btn') }}</button>
            </a>
            @if ($region == 'id')
                <!-- <a href="{{ url('merchant/registration') }}">
                    <button class="button button-circle">Daftar Sebagai Mitra</button>
                </a> -->
            @endif
        </div>
        <div class="content-wrap">
            <div class="container clearfix">
                <ul class="clients-grid grid-6 nobottommargin clearfix">
                    @if (!empty($merchantList))
                        @foreach ($merchantList as $merchant)
                            @if (empty($merchant->url))
								<li><img src="{{\App\Http\Helper\Helper::createImgLocal('merchant',$merchant->image)}}" alt="{{ $merchant->name }}"></li>
							@else
                                <li><a href="{{ $merchant->url }}" target="_blank"><img src="{{\App\Http\Helper\Helper::createImgLocal('merchant',$merchant->image)}}" alt="{{ $merchant->name }}" alt="{{ $merchant->name }}"></a></li>
							@endif
                        @endforeach
                    @endif
                </ul>
        </div>

    </section><!-- #content end -->
    {{-- Section Highlight --}}
    <section id="section-count">
        <div class="section dark parallax section-full" style="padding:50px 0; background-image: url('img/parallax/counter.jpg');" data-stellar-background-ratio="0.3">
            <div class="container clearfix">
                <div class="col_one_third nobottommargin center" data-animate="bounceIn">
                    <i class="i-plain i-xlarge divcenter nobottommargin icon-globe"></i>
                    <div class="counter counter-lined"><span data-from="1" data-to="{{$countDistrict}}" data-refresh-interval="50" data-speed="2000"></span></div>
                    <h5>{{ trans('index.count-city') }}</h5>
                </div>
                <div class="col_one_third nobottommargin center" data-animate="bounceIn" data-delay="200">
                    <i class="i-plain i-xlarge divcenter nobottommargin icon-map-marker"></i>
                    <div class="counter counter-lined"><span data-from="1" data-to="{{$countLocker}}" data-refresh-interval="100" data-speed="2500"></span>+</div>
                    <h5>{{ trans('index.count-locker') }}</h5>
                </div>
                <div class="col_one_third col_last nobottommargin center" data-animate="bounceIn" data-delay="400">
                    <i class="i-plain i-xlarge divcenter nobottommargin icon-box"></i>
                    <div class="counter counter-lined"><span data-from="1" data-to="{{$countCompartment}}" data-refresh-interval="25" data-speed="3500"></span>*</div>
                    <h5>{{ trans('index.count-compartment') }}</h5>
                </div>
            </div>
        </div>
    </section>
    {{-- Section Hightlights ENDs --}}
    <section id="testimonial" class="section-full">
        <div class="content-wrap">
            <div class="container clearfix">
                <div id="section-couple" class="heading-block title-center page-section">
                    <h2>{{ trans('index.testi-title') }}</h2>
                    <a href="{{ url('testimonial') }}">
                        <button class="button button-circle nearme">{{ trans('index.testi-btn') }}</button><br>
                    </a>
                    {{-- <span>{{ trans('index.testi-sub') }}</span> --}}
                </div>
                <ul class="testimonials-grid clearfix">
                    @if (!empty($testimonials))
                        @foreach ($testimonials as $element)
                             <li>
                                <div class="testimonial">
                                    <div class="testi-image">
                                        <a href="#"><img src="{{\App\Http\Helper\Helper::createImgLocal('article',$element->image)}}" alt="popbox"></a>
                                    </div>
                                    <div class="testi-content">
                                        <p>{{ strip_tags($element->content) }}</p>
                                        <div class="testi-meta">
                                            {{ $element->title }}
                                            <span>{{ $element->subtitle }}</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </section>

   

    {{-- Section BLOG --}}
    @if (!empty($news) && count($news)>0)
        <section class="section section-full">
            <h2 class="center">{{ trans('index.news-title') }}</h2>
            <div id="oc-clients-full" class="owl-carousel owl-carousel-full image-carousel carousel-widget bottommargin" data-margin="30" data-nav="true" data-pagi="false"data-items-xxs="3" data-items-xs="3" data-items-sm="5" data-items-md="6" data-items-lg="7" data-autoplay="5000" data-speed="650" data-loop="true">
                @foreach ($news as $element)
                     <div class="">
                        <a href="{{ url('article') }}/{{ date('Ymd',strtotime($element->created_date)) }}/{{ $element->short_title }}/{{ $element->id_article }}"><img src="{{\App\Http\Helper\Helper::createImgLocal('article',$element->image)}}" alt="Brands"></a>
                    </div>
                @endforeach
            </div>
        </section>
    @endif
    {{-- Section BLOG end --}}
@stop

@section('js')
    <script type="text/javascript">
        $(window).load(function() {
            $('#div-youtube').html('<iframe width="560" height="315" src="https://www.youtube.com/embed/RryQf9wHxmg" frameborder="0" allowfullscreen></iframe> ');
        });
        jQuery(document).ready(function($) {
            $('#section-paralax').css('background-image', 'url(img/parallax/counter.jpg)');
        });
    </script>
    <script type="text/javascript">
        var lockerList = {!! json_encode($lockerJkt) !!};
        var virtualLockerList = {!! json_encode($virtualLockerList) !!};
    </script>
	<script type="text/javascript">
        var markers = Array();
        var url_img = "{{ asset('img/icon/ic_box_map.svg') }}";
        var newLoc = false;

        $.each(lockerList,function (key,value) {
            var html = '<div align="center">';
            html += '<strong style="color:#FF6300;font-size:15px;">'+value.name+'</strong><br>';
            html += '<strong>'+value.address+'</strong><br>';
            html += value.address_2+'<br>';
            html += value.operational_hours;
            html+='</div>';
            var iconMarker = '{{ asset('img/map/marker-locker.png') }}'

            var locker = {
                latitude : value.latitude,
                longitude : value.longitude,
                html : html,
                icon : {
                    image : iconMarker,
                    iconsize: [30, 49],
                    shadowsize: [37, 34],
                    iconanchor: [9, 34],
                    shadowanchor: [19, 34]

                }
           }
           markers.push(locker);
        });
        @if (isset($region) && $region !='my')
            $.each(virtualLockerList,function (key,value) {
                var html = '<div align="center">';
                html += '<strong style="color:#FF6300;font-size:15px;">'+value.name+'</strong><br>';
                html += '<strong>'+value.address+'</strong><br>';
                html += value.address_2;
                html+='</div>';
                var iconMarker = '{{ asset('img/map/marker-lockerblue.png') }}'

                var locker = {
                    latitude : value.latitude,
                    longitude : value.longitude,
                    html : html,
                    icon : {
                        image : iconMarker,
                        iconsize: [30, 49],
                        shadowsize: [37, 34],
                        iconanchor: [9, 34],
                        shadowanchor: [19, 34]

                    }
               }
               markers.push(locker);
            });
        @endif
       
		$('#nearest_locker').gMap({
            address: '{{ trans('index.maps-location') }}',
            maptype: 'ROADMAP',
            zoom: {{ trans('index.maps-zoom') }},
            markers: markers,
            doubleclickzoom: false,
            controls: {
                panControl: true,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: true,
                streetViewControl: false,
                overviewMapControl: true
            }
        }).gMap('addSearchBox',{
            box : 'pac-input'
        }).gMap('addBtn',{
            location : 'Jakarta',
            text : 'Semua Loker',
            zoom : 11
        })
        $(document).ready(function() {
            $('button[class*=nearme]').on('click', function(event) {
               nearestLock();
            });

            $(".btn-register").click(function(){
                $('html, body').animate({ scrollTop: $('#registration').offset().top }, 'slow');
             })
        });
        

        function nearestLock(){
            var iconPosition = '{{ asset('img/map/market-youarehere.png') }}'
            if (navigator.geolocation) {
                if (newLoc==false) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        jQuery('#nearest_locker').gMap('addMarker', {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude,
                            icon : {
                                image : iconPosition,
                                iconsize: [40, 64],
                                iconanchor: [9, 34],
                            },
                        })
                    })
                }
                navigator.geolocation.getCurrentPosition(function(position) {
                    jQuery('#nearest_locker').gMap('centerAt', {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        zoom: 14
                    });
                    newLoc = true;
                }, function() {
                    console.log('Failed Geo Location')
                });
            }
        }

        $(document).ready(function() {

            $('#typeMap').addClass('hide');

            $('#iFindLocker').on('click', function(event) {
                if ($('#divFindLocker').hasClass('hide')) {
                    $('#divFindLocker').removeClass('hide');
                    $('#iFindLocker').removeClass('icon-angle-down');
                    $('#iFindLocker').addClass('icon-angle-up');

                    // $('#iFindLocker').removeClass('fadeInDown');
                    // $('#iFindLocker').addClass('fadeInUp');
                } else {
                    $('#divFindLocker').addClass('hide');
                    $('#iFindLocker').removeClass('icon-angle-up');
                    $('#iFindLocker').addClass('icon-angle-down');

                    // $('#iFindLocker').removeClass('fadeInUp');
                    // $('#iFindLocker').addClass('fadeInDown');
                }
            });

            $('select[id=mapCity]').on('change', function(event) {
                /* Act on the event */
                $('select[name=lockerFind]').html('');
                $.ajax({
                    url: '{{ url('lockerList') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: $('#mapLocker').serialize(),
                    success: function(data){
                        if (data.isSuccess==true) {
                            var lockerList = data.data;
                            var option = '';
                            $.each(lockerList, function(index, val) {
                                 /* iterate through array or object */
                                 var address = val.address;
                                 var address_2 = val.address_2;
                                 var district = val.district+', '+val.province;
                                 option += "<option value='"+val.name+"' lat='"+val.latitude+"' long='"+val.longitude+"'>"+val.name+"</option>"
                            });
                            $('select[name=lockerFind]').html(option);
                            $('select[name=lockerFind]').trigger('change');
                        } else {
                            $('#errorMsg').html(data.errorMsg);
                        }
                    }
                })
                .done(function() {
                    console.log("success");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });         
            });   

             $('select[name=lockerFind]').on('change', function(event) {
                /* Act on the event */
                $('#lockerSize').addClass('hide');
                $('#lockerAddress').addClass('hide');
                var locker = $(this).find(':selected');
                var lat = locker.attr('lat');
                var long = locker.attr('long');
                jQuery('#nearest_locker').gMap('centerAt', {
                    latitude: lat,
                    longitude: long,
                    zoom: 16
                });
            });
        });
	</script>
@stop
