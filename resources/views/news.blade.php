@extends('layout.main-article')
@section('content')
	<section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('img/bg/bg-testimoni.jpg'); padding: 120px 0;" data-stellar-background-ratio="0.3">
        <div class="container clearfix">
            <h1>News</h1>
        </div>
    </section>
    <section id="content">
    	<div class="content-wrap">
    		<div class="container clearfix">
    			<!-- Post Content
				============================================= -->
				<div class="postcontent nobottommargin clearfix">
					<!-- Posts
					============================================= -->
					<div id="posts" class="small-thumbs alt">
						{{-- Each news --}}
						@if (!empty($activeBlog))
							@foreach ($activeBlog as $article)
								<div class="entry clearfix">
									<div class="entry-image">
										<a href="{{ url('article') }}/{{ date('Ymd',strtotime($article->created_date)) }}/{{ $article->short_title }}/{{ $article->id_article }}" data-lightbox="image"><img class="image_fade" src="{{\App\Http\Helper\Helper::createImgUrl('article',$article->image)}}" alt="{{ $article->title }}"></a>
									</div>
									<div class="entry-c">
										<div class="entry-title">
											<h2><a href="{{ url('article') }}/{{ date('Ymd',strtotime($article->created_date)) }}/{{ $article->short_title }}/{{ $article->id_article }}">{{ $article->title }}</a></h2>
										</div>
										<ul class="entry-meta clearfix">
											<li><i class="icon-calendar3"></i>{{ date('j M Y',strtotime($article->created_date)) }}</li>
											<li><a href="#"><i class="icon-user"></i> PopBox</a></li>
											 <li><a href="#"><i class="icon-comments"></i>{{ ucfirst($article->type) }}</a></li>
										</ul>
										<div class="entry-content">
											<p>{{ $article->subtitle }}</p>
											<a href="{{ url('article') }}/{{ date('Ymd',strtotime($article->created_date)) }}/{{ $article->short_title }}/{{ $article->id_article }}"class="more-link">Read More</a>
										</div>
									</div>
								</div>
							@endforeach
						@endif
					</div><!-- #posts end -->
					<!-- Pagination
					============================================= -->
					<ul class="pager nomargin">
						{{ $activeBlog->links() }}
						{{-- <li class="previous"><a href="#">&larr; Older</a></li> --}}
						{{-- <li class="next"><a href="#">Newer &rarr;</a></li> --}}
					</ul><!-- .pager end -->
				</div><!-- .postcontent end -->
				<!-- Sidebar
				============================================= -->
				<div class="sidebar nobottommargin col_last clearfix">
					<div class="sidebar-widgets-wrap">
						
					</div>
				</div><!-- .sidebar end -->
    		</div>
    	</div>
    </section>
@stop