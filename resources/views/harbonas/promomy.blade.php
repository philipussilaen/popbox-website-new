<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>March Promo</title>
<meta name="description" content="Valentinemy by PopBox">
<meta name="author" content="Popbox Asia">
<meta property="og:url" content="<?php echo $_SERVER['SERVER_NAME']; ?>" />
<meta property="og:type" content="article" />
<meta property="og:title" content="March Promo By PopBox" />
<meta property="og:description" content="BUY online, SELECT and SEND to your nearest PopBox! Experience the convenience of shopping online and awesome deals from our merchants!" />

<meta property="og:image" content="{{ URL::asset('img/promomy/meta-data.jpg')}}" />
 <link rel="icon" type="image/png" href="{{URL::asset('img/favicon.ico')}}">
<link rel="icon" type="image/png" href="{{URL::asset('img/favicon-32x32.png')}}" sizes="32x32" />
{{-- <link rel="icon" type="image/png" href="{{URL::asset('img/favicon-16x16.png')}}" sizes="16x16" /> --}}

<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/bootstrap.css')}}">
<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/style.css')}}">
<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/promomy.css')}}">
<meta name="viewport" content="width=device-width,initial-scale=1">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="{{ elixir('js/jquery.gmap.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js')}}"></script>
<style type="text/css">    
    </style>
       <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-72128831-1', 'auto');
            ga('send', 'pageview');
        </script>
</head>
<body>
	<div class="container">
		

		<div class="row back-white">
			<div class="col-md-12">
				<img src="{{URL::asset('img/promomy/banner.png')}}" class="img-banner">
			</div>
		</div>

		<div class="row back-white">			
			<div class="col-md-4">				
				<a href="http://www.eliantomakeup.com/popbox-march-specials">
					<img src="{{URL::asset('img/promomy/ar1.png')}}" class="img-content">
				</a>
			</div>
			<div class="col-md-4">				
				<a href="http://www.yansociety.com/products.html">
					<img src="{{URL::asset('img/promomy/ar2.png')}}" class="img-content">
				</a>					
			</div>
			<div class="col-md-4">				
				<a href="http://www.bonia.com/bonia-popbox.html">
					<img src="{{URL::asset('img/promomy/ar3.png')}}" class="img-content">
				</a>					
			</div>			
		</div>

		<div class="row back-white">			
			<div class="col-md-4">				
				<a href="https://www.myohlalaa.com/">
					<img src="{{URL::asset('img/promomy/ar4.png')}}" class="img-content">
				</a>
			</div>
			<div class="col-md-4">				
				<a href="http://proppysocks.com.my">
					<img src="{{URL::asset('img/promomy/ar5.png')}}" class="img-content">
				</a>					
			</div>
			<div class="col-md-4">				
				<a href="www.pinengmalaysia.com">
					<img src="{{URL::asset('img/promomy/ar6.png')}}" class="img-content">
				</a>					
			</div>			
		</div>


		<div class="row back-white">	
			<div class="col-md-2">

			</div>		
			<div class="col-md-4">				
				<a href="https://www.madlab.my/">
					<img src="{{URL::asset('img/promomy/ar7.png')}}" class="img-content">
				</a>
			</div>
			<div class="col-md-4">		
				<a href="http://Jocom.my/festivecombosale">
					<img src="{{URL::asset('img/promomy/ar12.jpg')}}" class="img-content" style="width: 100%">
				</a>
			</div>
			<div class="col-md-2">

			</div>			
		</div>


		
		<!-- <div class="row back-white">
			<div class="col-md-2">
			</div>
			<div class="col-md-4">				
				<a href="http://www.jocom.my/">
					<img src="{{URL::asset('img/promomy/ar10.png')}}" class="img-content">
				</a>	
			</div>
			<div class="col-md-4">				
				<a href="http://www.gemfive.com/my/">
					<img src="{{URL::asset('img/promomy/ar11.png')}}" class="img-content">					
				</a>					
			</div>
			<div class="col-md-2">
			</div>
		</div>
 -->
		<div class="row back-white">
			<div class="col-md-12">
				<img src="{{URL::asset('img/promomy/aramex.png')}}">
			</div>
		</div>
	
		
		<div class="row deliver">	
			<div class="col-md-12">
				<div class="deliver-title">POPBOX LOCATION</div>
			</div>					
		</div>
		<div class="row back-white">	
		<div class="col-md-12 center btn-nearest">	
				<button type="button" class="btn btn-danger">FIND NEAREST LOCKER</button>
			</div>
		</div>
		<div class="row back-white drop-filter">	
			<div class="col-md-6 tright">
				<div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Select City <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" id="city" style="width:100%;">
                    	@foreach ($lockerBycities as $key => $value)
                        	<li>{{$key}}</li>
                        @endforeach
                    </ul>
                </div>
			</div>
			<div class="col-md-6 tleft">
				<div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    	Choose Locker <span class="caret"></span>
                    </button>
                  	<ul class="dropdown-menu" id="loker" style="width:100%;">
                   	</ul>
                </div>
			</div>					
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="map_wrapper">
			        <div id="map_canvas" class="mapping"></div>
			    </div> 
			</div>
		</div>

		<div class="row syarat back-red">	
			<div class="col-md-12">
				<div class="syarat-title"><span>*</span>Find Us at</div>
			</div>
		</div>

		<div class="row syarat back-red">	
			<div class="col-md-3 socmed">
				<a href="https://www.facebook.com/pboxasia/" target="_blank">
					<img src="{{URL::asset('img/promomy/fb.png')}}">
				</a>
			</div>
			<div class="col-md-3 socmed">
				<a href="https://twitter.com/PopBox_my" target="_blank">
					<img src="{{URL::asset('img/promomy/tw.png')}}">
				</a>
			</div>
			<div class="col-md-3 socmed">
				<a href="https://www.instagram.com/popbox_my/?hl=en" target="_blank">
					<img src="{{URL::asset('img/promomy/ins.png')}}">
				</a>
			</div>
			<div class="col-md-3 socmed">
				<a href="https://www.youtube.com/watch?v=zaACvMyFqQo" target="_blank">
					<img src="{{URL::asset('img/promomy/yt.png')}}">
				</a>
			</div>
		</div>	

		<div class="row syarat back-red">	
			<div class="col-md-12 footer-popbox-link">
				<a href="https://www.popbox.asia">www.popbox.asia</a>
			</div>
		</div>			
	</div>
	
	<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBf2v8vfQazUaI2egWcoYvwK7ljsAPh6Uk&libraries=places&callback=initialize"async defer></script>
	<script type="text/javascript">
		var lockerList = {!! json_encode($lockerJkt) !!};
		var lockerByCities = new Array();
		<?php foreach ($lockerBycities as $key => $value): ?>
			    lockerByCities['<?php echo $key ?>']= <?php echo ($value); ?>;
		<?php endforeach ?>

		var arrLockers = new Array();
		<?php foreach ($lockers as $key => $value): ?>			
			    arrLockers['<?php echo $value->name ?>']= <?php echo (json_encode($value)); ?>;
		<?php endforeach ?>
		var lockername = "";
		var newLoc = false;

		$(document).ready(function(){
			$('#city li').on('click', function(){
		        $(this).parents(".btn-group").find('.btn').html($(this).text()+" <span class=\"caret\"></span>");
		        $("#loker li").parents(".btn-group").find('.btn').html("Pilih Loker <span class=\"caret\"></span>");
		        var  str_html ='';		        	       
		        $.each(lockerByCities[$(this).text()], function( index, value ){		        	
		            str_html +='<li>'+ value.name+'</li>';
		        });
		        $("#loker").parent().removeClass('hide');
        		$("#loker").html(str_html);
			});

			 $(document).on('click', '#loker li', function(){			 	
		        $(this).parents(".btn-group").find('.btn').html($(this).text()+" <span class=\"caret\"></span>");
		        lockername = $(this).text();
		        var obj = arrLockers[lockername];		        
				var lockerMarkers = new Array();
			    var lockerInfoWindowContent = new Array()
			    lockerInfoWindowContent.push(["'" + obj.address + "'"]);
			    lockerMarkers.push(["'" + obj.name + "'", obj.latitude, obj.longitude]);		       
			    test(lockerMarkers, lockerInfoWindowContent, false);
		      
		    });

			 $('.btn-danger').on('click', function(){		
			 	//showPosition("");
			 	if (navigator.geolocation) {
			        navigator.geolocation.getCurrentPosition(showPosition);
			    } else {
			        console.log('Failed Geo Location')
			    }		
			 });

		});

		 function initialize() {
		    // digunakan untuk empty maps default lat lang
		    // var bounds = new google.maps.LatLngBounds(); 
		    // var myLatlng = {lat: -6.20, lng: 106.84};
		    // var map = new google.maps.Map(document.getElementById('map_canvas'), {
		    //   zoom: 11,
		    //   center: myLatlng
		    // });
		    
		    var markers =  new Array(); // or "var valueToPush = new Object();" which is the same
		    var infoWindowContent = new Array();    
		    <?php foreach ($lockers as $value) { ?>       
		       console.log("<?php echo $value->name; ?>");
		       var content = '<div><strong><?php echo $value->name; ?></strong></div><div><?php echo $value->address; ?></div>';
		       infoWindowContent.push(["'" + content + "'"]);
		       markers.push(["'<?php echo $value->name; ?>'", <?php echo $value->latitude; ?>, <?php echo $value->longitude; ?>]);      
		    <?php } ?>       
		    test(markers, infoWindowContent, true);   
 		}

 	

        function showPosition(position) {		    
		    var domain = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
		    var fulurl = domain + "/cnydeals?location=1";
		    $.ajax({
			  	method: "GET",
			  	url: fulurl,
			  	data: { long: position.coords.latitude, lat: position.coords.longitude }
			  	//data: { long: "101.602733", lat: "3.072519" }
			}).done(function( result ) {			
				var markers =  new Array(); // or "var valueToPush = new Object();" which is the same
		    	var infoWindowContent = new Array();    	
			    $.each(result.data, function( index, value ) {			    	
			    	var content = '<div><strong>' + value.name + '</strong></div><div>'+ value.address + '</div>';
		       		infoWindowContent.push(["'" + content + "'"]);
		       		markers.push(["'" + value.name + "'", value.latitude, value.longitude]);      
			    });
			    test(markers, infoWindowContent, true);   
			});
		}
  
  		function test(markers, infoWindowContent, isFirst){      
		    // Multiple Markers
		    var bounds = new google.maps.LatLngBounds();    
		    var mapOptions = {
		        mapTypeId: 'roadmap'
		    };
		                    
		    // Display a map on the page

		    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		    map.setTilt(45);
		        
		    // Display multiple markers on a map
		    var infoWindow = new google.maps.InfoWindow(), marker, i;
		    
		    // Loop through our array of markers & place each one on the map  
		   
		    var url_img = '{{ asset('img/map/marker-locker.png') }}';
		    var image = {
		      url: url_img,
		      origin: new google.maps.Point(0, 0),
		      anchor: new google.maps.Point(17, 34),
		      scaledSize: new google.maps.Size(20, 33)
		    };
		   
		    for( i = 0; i < markers.length; i++ ) {            
		        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);        
		        bounds.extend(position);
		        if (markers[i][0]=="current_location"){                  
		            marker = new google.maps.Marker({
		                position: position,
		                map: map,                
		                title: "Lokasi anda disini"
		            });      
		            infoWindow.setContent(infoWindowContent[i][0]);
		            infoWindow.open(map, marker);
		        }else{
		            marker = new google.maps.Marker({
		                position: position,
		                map: map,
		                icon: image,
		                title: markers[i][0]
		            });
		        }
		        
		        // Allow each marker to have an info window    
		        google.maps.event.addListener(marker, 'click', (function(marker, i) {
		            return function() {
		                infoWindow.setContent(infoWindowContent[i][0]);
		                infoWindow.open(map, marker);
		                console.log(infoWindowContent[i][0]);
		            }
		        })(marker, i));

		        // Automatically center the map fitting all markers on the screen
		        map.fitBounds(bounds);
		    }

		    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
		    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
		        if (isFirst){
		          this.setZoom(11);
		          this.setCenter(new google.maps.LatLng(-6.21, 106.81));
		        }else{
		          this.setZoom(13);
		        }
		        google.maps.event.removeListener(boundsListener);
		    });
		    
		}

		function onLink(link){
			window.location.href = link;
		}
	</script>	 

</body>
</html>
