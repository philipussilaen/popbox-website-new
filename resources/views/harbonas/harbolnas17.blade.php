<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Harbolnas 2017 By PopBox</title>
    <meta name="description" content="PopBox Harbolnas 12.12 Free Ongkir PopSend ke loker dan alamat seluruh Indonesia. Cek Promo Lainnya." />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ URL::asset('harbolnas2017/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('harbolnas2017/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('harbolnas2017/css/ionicons.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('harbolnas2017/css/style.css') }}" />

    <script src="{{ URL::asset('harbolnas2017/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ URL::asset('harbolnas2017/js/tether.min.js') }}"></script>
    <script src="{{ URL::asset('harbolnas2017/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('harbolnas2017/js/jquery.easing.min.js') }}"></script>
    <script src="{{ URL::asset('harbolnas2017/js/wow.js') }}"></script>
    <script src="{{ URL::asset('harbolnas2017/js/scripts.js') }}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBf2v8vfQazUaI2egWcoYvwK7ljsAPh6Uk&libraries=places&callback=loadMap"></script>
    <script type="text/javascript">

        var lockerList = {!! json_encode($lockerJkt) !!};
        var markersFilterDefault = [];
        var defaultMarkers = lockerList.filter(function(item){
            markersFilterDefault.push([item.name + ", " + item.address, item.latitude, item.longitude]);
        });

        function getDistrict() {
            var district = $('#district').val();
            var districtText = $("#district option:selected").text();
            markersFilter = [];
            $('#locker').empty();
            $('#locker').append('<option value="">SELECT</option>');
            var filtered= lockerList.filter(function(item){
                if(item.city==districtText) {
                    markersFilter.push([item.name + ", " + item.address, item.latitude, item.longitude]);
                    $('#locker').append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });

            var districtIndex = $("#district option:selected").index();
            var arrDistrict = [1,2,3,4,5,6,7,8];
            var checkDistrict = $.inArray( districtIndex, arrDistrict );

            if(checkDistrict >= 0) {
                var centerMap = new google.maps.LatLng(-6.21, 106.81);
                var zoomMap = 10;
            } else {
                var centerMap = new google.maps.LatLng(-2.99, 114.84);
                var zoomMap = 5;
            }

            var markers = markersFilter;

            if(districtText == "SELECT") {
                var markers = markersFilterDefault;
            }

            var mapOptions = {
                center: centerMap,
                zoom: zoomMap,
                mapTypeId: 'roadmap'
            };

            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            map.setTilt(45);
            var infoWindow = new google.maps.InfoWindow(), marker, i;
            var url_img = '{{ asset('img/map/marker-locker.png') }}';
            var image = {
                url: url_img,
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(20, 33)
            };

            for (i = 0; i < markers.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(markers[i][1], markers[i][2]),
                    map: map,
                    icon: image,
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        var content = '<div><strong>'+markers[i][0]+'</strong></div>';
                        infoWindow.setContent(content);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

        function getLocker() {
            var locker = $("#locker").val();
            markersFilter = [];
            var lat = 0;
            var long = 0;
            var filtered = lockerList.filter(function(item){
                if(item.id==locker) {
                    markersFilter.push([item.name + ", " + item.address, item.latitude, item.longitude]);
                    lat = item.latitude;
                    long = item.longitude;
                }
            });
            var markers = markersFilter;

            var mapOptions = {
                center:new google.maps.LatLng(lat, long),
                zoom: 13,
                mapTypeId: 'roadmap'
            };

            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            map.setTilt(45);
            var infoWindow = new google.maps.InfoWindow(), marker, i;
            var url_img = '{{ asset('img/map/marker-locker.png') }}';
            var image = {
                url: url_img,
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(20, 33)
            };

            for (i = 0; i < markers.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(markers[i][1], markers[i][2]),
                    map: map,
                    icon: image,
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        var content = '<div><strong>'+markers[i][0]+'</strong></div>';
                        infoWindow.setContent(content);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

        function loadMap() {

            var markers = markersFilterDefault;

            var mapOptions = {
                center:new google.maps.LatLng(-6.21, 106.81),
                zoom: 10,
                mapTypeId: 'roadmap'
            };

            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            map.setTilt(45);
            var infoWindow = new google.maps.InfoWindow(), marker, i;
            var url_img = '{{ asset('img/map/marker-locker.png') }}';
            var image = {
                url: url_img,
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(20, 33)
            };

            for (i = 0; i < markers.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(markers[i][1], markers[i][2]),
                    map: map,
                    icon: image,
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        var content = '<div><strong>'+markers[i][0]+'</strong></div>';
                        infoWindow.setContent(content);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
    </script>
    <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-72128831-1', 'auto');
            ga('send', 'pageview');
        </script>
</head>
<body onload="loadMap()">
<!--header-->
<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top sticky-navigation">
    <button class="navbar-toggler navbar-toggler-right sticknav" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <img src="{{ URL::asset('harbolnas2017/images/sticknav.png') }}" class="img-responsive">
    </button>
    <a class="navbar-brand hero-heading" href="https://www.popbox.asia/" target="_blank">
        <img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_61.png') }}" class="img-responsive">
    </a>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item mr-3">
                <a class="nav-link page-scroll" href="#harbolnas">Promo Harbolnas <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item mr-3">
                <a class="nav-link page-scroll" href="#merchant">Promo Merchant</a>
            </li>
            <li class="nav-item mr-3">
                <a class="nav-link page-scroll" href="#popbox">Cara PopBox</a>
            </li>
            <li class="nav-item mr-3">
                <a class="nav-link page-scroll" href="#popsend">Cara PopSend</a>
            </li>
            <li class="nav-item mr-3">
                <a class="nav-link page-scroll" href="#location">Lokasi PopBox</a>
            </li>
            <li class="nav-item mr-3">
                <a class="nav-link page-scroll" href="#contact">Kontak Kami</a>
            </li>
        </ul>
    </div>
</nav>

<!--main-->
<section class="bg-texture pad-0 margin-top-100 bg-landing" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-2 popsend-middle">
                <img src="{{ URL::asset('harbolnas2017/images/logo-harbolnas.png') }}" class="img-responsive">
            </div>
            <div class="col-md-4 popsend-middle">
                <img src="{{ URL::asset('harbolnas2017/images/logo-locker2.png') }}" class="img-responsive">
            </div>
            <div class="col-md-6 align-right">
                <div class="title-free-ongkir">FREE ONGKIR</div>
                <div class="title-popsend">POPSEND</div>
                <div class="title-seluruh-indonesia">SELURUH INDONESIA</div>
                <hr class="title-border">
                <span class="title-promo-code">PROMO CODE </span> <span style="font-family: GothamBlack; color: #ffffff; font-size: 2em; background: #da2638; padding: 7px;">HARBOLNAS17</span>
		<div class="title-periode">*periode hanya berlaku tanggal 12 Desember 2017 </div>
            </div>
        </div>
    </div>
</section>

<!--harbolnas-->
<section class="bg-faded" id="harbolnas">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-part-title">
                <hr class="hr-border">
                <h3>Promo Harbolnas 12.12</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 harbolnas-pad-bot-10">
                <a href="https://www.facebook.com/pboxasia/photos/a.834399519982960.1073741828.819009431521969/1497944376961801/?type=3&theater" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_15.png') }}" width="100%" class="img-responsive"></a>
                <span>Syarat & ketentuan : </span>
                <ul class="harbolnas-point">
                    <li>1. Top Up saldo PopSend, akan mendapatkan tambahan bonus saldo sebesar 50.000 </li>
                    <li>2. Minimum pembelian 100.000</li>
                    <li>3. Periode 12-13 Desember 2017</li>
                </ul>
            </div>
            <div class="col-md-6 harbolnas-pad-bot-10">
                <a href="http://bit.do/popsendqr" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_10.png') }}" width="100%" class="img-responsive"></a>
                <span>Syarat & ketentuan : </span>
                <ul class="harbolnas-point">
                    <li>1. Top Up Saldo PopSend, akan mendapatkan tambahan bonus saldo sebesar 25%</li>
                    <li>2. Tanpa minimum pembelian</li>
                    <li>3. Periode 12-13 Desember 2017</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 harbolnas-pad-bot-10">
            </div>
            <div class="col-md-6 harbolnas-pad-bot-10">
                <a href="https://www.popbox.asia/promopulsa" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017-XL_03.png') }}" width="100%" class="img-responsive"></a>
                <span>Syarat & ketentuan : </span>
                <ul class="harbolnas-point">
                    <li>1. Pembelian berlaku di seluruh loker PopBox yang terdapat reader emoney</li>
                    <li>2. Pembelian hanya dapat menggunakan e-money</li>
                    <li>3. Periode 12-14 Desember 2017</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!--merchant-->
<section class="bg-faded bg-gray" id="merchant">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-part-title">
                <hr class="hr-border">
                <h3>Promo Merchants PopBox</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3 merchant-pad-bot-10">
                        <a href="https://www.kuki-style.com/" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_21.png') }}" width="100%" class="img-responsive"></a>
                    </div>
                    <div class="col-md-3 merchant-pad-bot-10">
                        <a href="https://www.instagram.com/_ristore/" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_23.png') }}" width="100%" class="img-responsive"></a>
                    </div>
                    <div class="col-md-3 merchant-pad-bot-10">
                        &nbsp;<a href="https://www.instagram.com/valecloth.id/" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/valecloth.png') }}" width="100%" class="img-responsive" style="margin-top: -23px;"></a>
                    </div>
                    <div class="col-md-3 merchant-pad-bot-10">
                        &nbsp;<a href="https://www.asports.id/" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017-asport.png') }}" width="100%" class="img-responsive"></a>
                    </div>
                    <div class="col-md-3 merchant-pad-bot-10">
                        &nbsp;<a href=" https://www.mitre.co.id/" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017-mitre.png') }}" width="100%" class="img-responsive"></a>
                    </div>
                    <div class="col-md-3 merchant-pad-bot-10">
                        &nbsp;<a href="https://www.popbox.asia/merchant" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017-lainnya.png') }}" width="100%" class="img-responsive"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--popbox-->
<section class="bg-faded" id="popbox">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-part-title">
                <hr class="hr-border">
                <h3>Cara Pakai PopBox</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 popbox-align">
                <center>
                    <img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_30.png') }}" width="75%" class="img-responsive"><br>
                    Belanja online
                </center>
            </div>
            <div class="col-md-3 popbox-align">
                <center>
                    <img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_32.png') }}" width="75%" class="img-responsive"><br>
                    Pilih lokasi loker terdekat
                </center>
            </div>
            <div class="col-md-3 popbox-align">
                <center>
                    <img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_34.png') }}" width="75%" class="img-responsive"><br>
                    Barang sampai kamu akan menerima SMS PIN untuk buka loker
                </center>
            </div>
            <div class="col-md-3 popbox-align" style="padding-top: 60px;">
                <center>
                    <img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_36.png') }}" width="75%" class="img-responsive"><br>
                    Ambil paket dan enjoy!
                </center>
            </div>
        </div>
    </div>
</section>


<!--popsend-->
<section class="bg-gray" id="popsend">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-part-title">
                <hr class="hr-border">
                <h3>Cara Pakai PopSend</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 popsend-middle" style="text-align: center;">
                <img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_44.png') }}" class="img-responsive">
            </div>
            <div class="col-md-2 popsend-middle" style="vertical-align: middle;">
                Pilih lokasi loker terdekat
            </div>
            <div class="col-md-2 popsend-middle" style="text-align: center;">
                <img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_46.png') }}" class="img-responsive">
            </div>
            <div class="col-md-2 popsend-middle" style="vertical-align: middle;">
                Masukan detail order
            </div>
            <div class="col-md-2 popsend-middle" style="text-align: center;">
                <img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_48.png') }}" class="img-responsive">
            </div>
            <div class="col-md-2 popsend-middle">
                Masukan paket ke loker dan paket akan dikirim
            </div>
        </div>
    </div>
</section>

<section class="pad-0 bg-alt">
    <div class="container bg-popbox-download">
        <div class="row">
            <div class="col-md-6 offset-md-2">
                <div class="download-title">DOWNLOAD APLIKASINYA!</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-2 align-center">
                <a href="http://bit.do/popsendqr" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/google-play-logo.png') }}" class="img-responsive img-xs"></a>
                <a href="http://bit.do/popsendqr" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/apple_store_logo.png') }}" class="img-responsive img-xs"></a>
            </div>
        </div>
    </div>
</section>

<!--location-->
<section class="bg-white" id="location">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-part-title">
                <hr class="hr-border">
                <h3>Lokasi PopBox</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 popsend-middle align-center">
                    <div id="map" style="width: 100%; height: 300px; margin-bottom: 20px;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <div class="row">
                    <div class="col-md-6">
                        <select name="district" class="form-control" id="district" onchange="getDistrict()">
                            <option value="">SELECT</option>
                            @foreach ($citiesList as $district)
                                <option value="{{ $district }}">{{ $district }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <select name="locker" class="form-control" id="locker" onchange="getLocker()">
                            <option value="">SELECT</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--contact-->
<section id="contact" style="background: #f2f3f5;">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xs-12 align-center-xs">
                <a href="https://www.popbox.asia/" target="_blank" style="cursor: pointer;"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_61.png') }}" class="img-responsive"></a>
            </div>
            <div class="col-md-6 hidden-md hidden-xs hidden-xm">

            </div>
            <div class="col-md-4 col-xs-12 align-right align-center-xs">
                <a href="https://www.facebook.com/pboxasia/" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_64.png') }}"></a>
                <a href="https://www.instagram.com/popbox_asia/" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_66.png') }}"></a>
                <a href="https://twitter.com/PopBox_Asia" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/Landing-Page-harbolnas-2017_68.png') }}"></a>
                <a href="https://www.youtube.com/watch?v=0T-SvaXWlBw" target="_blank"><img src="{{ URL::asset('harbolnas2017/images/logo-youtube.png') }}"></a>
            </div>
        </div>
    </div>
</section>

</body>
</html>
