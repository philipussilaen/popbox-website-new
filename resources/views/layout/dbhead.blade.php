 	<title>PopBox Pay By QR</title>
  	<meta name="description" content="Belanja seru tanpa ribet dengan QR bersama PopBox!
Nikmati kemudahan berbelanja dan ambil sendiri belanjaanmu di loker PopBox terdekatmu">
    <meta name="author" content="PopBox Asia">
    <meta property="og:image" content="http://pr.popbox.asia/img/shop-meta-description.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:description" content="Belanja seru tanpa ribet dengan QR bersama PopBox!
Nikmati kemudahan berbelanja dan ambil sendiri belanjaanmu di loker PopBox terdekatmu">
    <!-- Optional theme -->
	<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" />
	<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-theme.min.css') }}" />	
	<link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.min.css') }}" />
	<link rel="stylesheet" href="{{ URL::asset('css/dashboard.css') }}" />
	
	<script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>				
	<script type="text/javascript" src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	 	
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>	
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<!-- <meta name="viewport" content="width=1024" /> -->