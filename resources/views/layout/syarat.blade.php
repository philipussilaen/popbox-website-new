{{-- Modal --}}
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Syarat dan Ketentuan</h4>
                </div>
                <div class="modal-body">
                    {{-- 1 --}}
                    <div class="col_full">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <i class="i-alt">1.</i>
                            </div>
                            <h3 style="font-size: 20px" class="font-secondary ls1 t400">Ukuran Barang</h3>
                            <p style="font-size: 18px; color: #444;" class="ls1 t300">
                                Ukuran terbesar paket yang dapat dikirimkan adalah maksimal sebesar ukuran L kompartemen loker PopBox. <br>
                                Berikut ukuran kompartemen loker PopBox : <br>
                                XS : 18 cm (H) x 19 cm (L) x 48 cm (W) <br>
                                S  : 10 cm (H) x 34 cm (L) x 48 cm (W) <br>
                                M  : 18 cm (H) x 34 cm (L) x 48 cm (W) <br>
                                L  : 31 cm (H) x 34 cm (L) x 48 cm (W) 
                            </p>
                            <img src="{{ asset('img/popbox-lockersize.jpg') }}" style="width: 75%; height: auto;">
                        </div>
                    </div>
                    {{-- 2 --}}
                    <div class="col_full">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <i class="i-alt">2.</i>
                            </div>
                            <h3 style="font-size: 20px" class="font-secondary ls1 t400">Waktu Pengambilan Barang</h3>
                            <p style="font-size: 18px; color: #444;" class="ls1 t300">
                                PopBox memberikan waktu sampai 6 hari untuk mengambil barang di dalam loker. Jika dalam 6 hari barang tersebut tidak diambil maka barang akan dikembalikan ke penjual.
                            </p>
                        </div>
                    </div>
                    {{-- 3 --}}
                    <div class="col_full">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <i class="i-alt">3.</i>
                            </div>
                            <h3 style="font-size: 20px" class="font-secondary ls1 t400">Cut Off Time</h3>
                            <p style="font-size: 18px; color: #444;" class="ls1 t300">
                                Cut off time pengiriman data adalah pukul <strong>12:00</strong>, data yang masuk sebelum pukul 12:00 diambil di hari yang sama dan yang di atas pukul 12:00 akan diambil di hari berikutnya. Pada saat data dimasukkan dalam sistem PopBox barang sudah siap diambil.
                            </p>
                        </div>
                    </div>
                    {{-- 4 --}}
                    <div class="col_full">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <i class="i-alt">4.</i>
                            </div>
                            <h3 style="font-size: 20px" class="font-secondary ls1 t400">Cross Promotion</h3>
                            <p style="font-size: 18px; color: #444;" class="ls1 t300">
                                Penjual wajib mempromosikan PopBox melalui social media serta mengikuti event dan promo yang PopBox berikan untuk menaikkan penjualan dengan PopBox delivery.
                            </p>
                        </div>
                    </div>
                    {{-- 5 --}}
                    <div class="col_full">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <i class="i-alt">5.</i>
                            </div>
                            <h3 style="font-size: 20px" class="font-secondary ls1 t400">Third Party Logistic</h3>
                            <p style="font-size: 18px; color: #444;" class="ls1 t300">
                                PopBox berhak menunjuk pihak ketika untuk melakukan pengiriman / pengambilan barang.
                            </p>
                        </div>
                    </div>
                    {{-- 6 --}}
                    <div class="col_full">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <i class="i-alt">6.</i>
                            </div>
                            <h3 style="font-size: 20px" class="font-secondary ls1 t400">Asuransi Kehilangan</h3>
                            <p style="font-size: 18px; color: #444;" class="ls1 t300">
                                Kehilangan / kerusakan barang yang diakibatkan oleh pihak PopBox, akan dijamin 10 kali dari harga barang. <br>
                                Kehilangan barang yang diakibatkan dari pihak penjual misalkan karena pemberian no handphone yang salah, bukan menjadi tanggung jawab PopBox. <br>
                                Kehilangan barang yang diakibatkan dari pihak konsumen misalkan memberikan PIN ke pihak lain, bukan menjadi tanggung jawab PopBox. 
                            </p>
                        </div>
                    </div>
                    {{-- 7 --}}
                    <div class="col_full">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <i class="i-alt">7.</i>
                            </div>
                            <h3 style="font-size: 20px" class="font-secondary ls1 t400">Penolakan Barang Kiriman</h3>
                            <p style="font-size: 18px; color: #444;" class="ls1 t300">
                                PopBox berhak menolak pengiriman apabila :  <br>
                                <strong>-</strong>Data penerima tidak lengkap <br>
                                <strong>-</strong>Ukuran barang melebihi ukuran loker <br>
                                <strong>-</strong>Barang-barang yang illegal (obat-obatan, senjata tajam, barang yang mudah meledak, binatang, tanaman hidup, pecah belah, barang/surat berharga, dll) <br>
                                <strong>-</strong>Barang yang dikirimkan mencurigakan / melanggar ketentuan syarat dan ketentuan PopBox. <br>
                            </p>
                        </div>
                    </div>
                    {{-- 8 --}}
                    <div class="col_full">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <i class="i-alt">8.</i>
                            </div>
                            <h3 style="font-size: 20px" class="font-secondary ls1 t400">Validasi Data</h3>
                            <p style="font-size: 18px; color: #444;" class="ls1 t300">
                                Data order yang dikirimkan ke pihak PopBox harus merupakan data yang valid. Nomor handphone yang dikirimkan adalah nomor telepon pembeli langsung dan nomor yang aktif digunakan.
                            </p>
                        </div>
                    </div>
                    {{-- 9 --}}
                    <div class="col_full">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <i class="i-alt">9.</i>
                            </div>
                            <h3 style="font-size: 20px" class="font-secondary ls1 t400">Pengemasan Barang</h3>
                            <p style="font-size: 18px; color: #444;" class="ls1 t300">
                                Barang harus dikemas dengan baik dan diberikan label. Informasi penting yang harus tertempel pada paket adalah : <br>
                                <strong>-</strong>Nomor order <br>
                                <strong>-</strong>Nomor handphone konsumen <br>
                                <strong>-</strong>Nama lokasi loker (misalkan: Grand Slipi Tower) <br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>