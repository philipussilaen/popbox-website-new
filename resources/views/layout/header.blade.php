 <header id="header" class="split-menu transparent-header dark semi-transparent" data-sticky-class="not-dark" data-responsive-class="not-dark">
    <div id="header-wrap">
        <div class="container clearfix">
            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
            {{-- Logo --}}
            <div id="logo">
                <a href="{{ url('/') }}" class="standard-logo" data-dark-logo="{{ asset('img/logo/logo-dark.png') }}">
                    <img src="{{ asset('img/logo/logo.png') }}" alt="PopBox Logo">
                </a>
                <a href="{{ url('/') }}" class="retina-logo" data-dark-logo="{{ asset('img/logo/logo-dark@2x.png') }} ">
                    <img src="{{ asset('img/logo/logo@2x.png') }}" alt="PopBox Logo">
                </a>
            </div>
            {{-- #logo end --}}
            {{-- Primary Navigation --}}
            <nav id="primary-menu" class="with-arrows clearfix not-dark">
                <ul class="one-page-menu" data-easing="easeInOutExpo" data-speed="1250" data-offset="160">
                    <li>
                         <a href="#" data-toggle="modal" class="modalTracking" data-target="#modalTracking">
                            <div>{{ trans('layout.tracking') }}</div>
                        </a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#modalLocker">
                            <div>{{ trans('layout.avaibility-btn') }}</div>
                        </a>
                    </li>
                    <li>
                        <a href="#" data-href="#section-nextgen">
                            <div>{{ trans('layout.service') }}</div>
                        </a>
                        <ul>
                            <li>
                                <a href="#" onclick="onGoto('#section-services')" >
                                    <div>PopBox Logistic</div>
                                </a>
                            </li>
                            <li>
                                <!-- <a href="https://popsend.popbox.asia/" target="_blank"> -->
                                <a href="#" onclick="onGoto('#section-services')" >
                                    <div>PopSend</div>
                                </a>
                            </li>
                            <li>
                                <a href="#" onclick="onGoto('#section-services')" >
                                    <div>PopSafe</div>
                                </a>
                            </li>
                            <!-- <li>
                                <a href="{{ url('popboxagent') }}"  target="_blank">
                                    <div>PopBox Agent</div>
                                </a>
                            </li> -->
                            <!-- <li class="sub-menu">
                                <a href="#" data-href="#section-nextgen"  class="sf-with-ul">
                                    <div>PopBox on Demand</div>
                                </a>
                                <ul style="display: none;">
                                    <li>
                                        <a href="{{ url('omaisu') }}" target="_blank" >
                                            <div>Omaisu</div>
                                        </a>
                                    </li>
                                     {{-- <li>
                                        <a href="{{ url('vcleanshoes') }}" target="_blank" >
                                            <div>VCleanShoes</div>
                                        </a>
                                         </li> --}}
                                     <li>
                                        <a href="{{ url('tayaka') }}" target="_blank">
                                            <div>Tayaka</div>
                                        </a>                                        
                                    </li>
                                    <li>
                                        <a href="http://popbox.labalaba.co.id/" target="_blank">
                                            <div>LabaLaba</div>
                                        </a>
                                    </li>
                                </ul>
                            </li> -->
                            
                            
                        </ul>
                    </li>
                    <li>
                        <a href="#" data-href="#section-nextgen">
                            @php 
                                $locale = \Session::get('app.region','id');
                            @endphp
                            <span class="flag-icon flag-icon-{{ $locale }} flag-icon-squared" style="display: inline-flex"></span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('language') }}/id" data-href="#section-nextgen">
                                    <div><span class="flag-icon flag-icon-id flag-icon-squared" style="display: inline-flex;"></span> Indonesia</div>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('language') }}/my" data-href="#section-stunning-graphics">
                                    <div><span class="flag-icon flag-icon-my flag-icon-squared" style="display: inline-flex;"></span> Malaysia</div>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="one-page-menu" data-easing="easeInOutExpo" data-speed="1250" data-offset="160">
                    <li>
                        <a href="{{ url('blog') }}">
                            <div>{{ trans('layout.blog-url') }}</div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/') }}/#nearest_locker">
                            <div>{{ trans('layout.location') }}</div>
                        </a>
                    </li>
                    <li class="menu-item-emphasis">
                        <a href="{{ url('promo') }}">
                            <div>{{ trans('layout.promo') }}</div>
                        </a>
                    </li>
                    <!-- <li>  
                        <a href="#authmodal" data-lightbox="inline" class="openlogin">
                            <div>Login</div>
                        </a>
                    </li> -->
                    {{-- <li class="menu-item-emphasis">
                        <a href="{{ url('/') }}#client">
                            <div>{{ trans('layout.merchant') }}</div>
                        </a>
                    </li> --}}
                </ul>
            </nav>
            <!-- #primary-menu end -->
        </div>
    </div>
</header>
<script type="text/javascript">
     function onGoto(idelement){
        $('html,body').animate({ scrollTop: $(idelement).offset().top }, 'slow');
    }
</script>
