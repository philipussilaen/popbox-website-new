<footer id="footer" class="dark">
	<div class="container">
        <!-- Footer Widgets
        ============================================= -->
        <div class="footer-widgets-wrap clearfix">
        	<div class="row clearfix">
        		<div class="col-md-5">
                    <div class="widget widget_links clearfix">
                        <div class="row clearfix">
                            <div class="col-md-8 bottommargin-sm clearfix" style="color:#888;">
                                <img src="{{ asset('img/footer-logo.png') }}" alt="PopBox Logo" style="display: block;" class="bottommargin-sm">
                                <p>{{ trans('layout.tag') }}</p>
                                <a href="https://www.facebook.com/pboxasia" class="social-icon si-small si-borderless si-colored si-rounded si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="https://twitter.com/popbox_asia" class="social-icon si-small si-borderless si-colored si-rounded si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="https://instagram.com/popbox_asia" class="social-icon si-small si-borderless si-colored si-rounded si-instagram">
                                    <i class="icon-instagram"></i>
                                    <i class="icon-instagram"></i>
                                </a>
                                <a href="https://www.youtube.com/channel/UCYGHLg4Xdc_IHD5DHdDIRTA" class="social-icon si-small si-borderless si-colored si-rounded si-youtube">
                                    <i class="icon-youtube"></i>
                                    <i class="icon-youtube"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/pt-popbox-asia-services" class="social-icon si-small si-borderless si-colored si-rounded si-linkedin">
                                    <i class="icon-linkedin"></i>
                                    <i class="icon-linkedin"></i>
                                </a>
                                <a href="#" class="social-icon si-small si-borderless si-colored si-rounded si-line">
                                    <i class="icon-line"></i>
                                    <i class="icon-line"></i>
                                </a>
                            </div>
                        </div>
                        <div>
                            {{ trans('layout.callus') }}<br>
                            <a href="tel:{{ trans('contact.contact-phone') }}"><i class="icon-call i-alt"></i>&nbsp; {{ trans('contact.contact-phone') }}</a><br>
                            <!-- <a href="tel:{{ trans('contact.contact-phone2') }}"><i class="icon-call i-alt"></i>&nbsp; {{ trans('contact.contact-phone2') }}</a><br> -->
                            <a href="mailto:info@popbox.asia"><i class="icon-mail i-alt"></i>&nbsp; info@popbox.asia</a>
                        </div>
                    </div>
                </div>
        		<div class="col-md-7">
        			<div class="row clearfix">
        				<div class="col-md-4 bottommargin-sm">
        					<div class="widget widget_links app_landing_widget_link clearfix">
        					</div>
        				</div>
        				<div class="col-md-4 bottommargin-sm">
        					<div class="widget widget_links clearfix">
        						<h4>{{ trans('layout.our-service') }}</h4>
        						<ul>
                                    <li><a href="https://popsend.popbox.asia">PopSafe</a></li>
        							<li><a href="https://popsend.popbox.asia">PopSend</a></li>
        							<!-- <li><a href="http://shop.popbox.asia/">PopShop</a></li> -->
        						</ul>
        					</div>
        				</div>
        				<div class="col-md-4 bottommargin-sm">
        					<div class="widget widget_links clearfix">
        						<h4>PopBox</h4>
        						<ul>
                                    <li><a href="{{ url('blog') }}">{{ trans('layout.blog-url') }}</a></li>
        							<li><a href="{{ url('contact') }}">{{ trans('layout.contact-url') }}</a></li>
        							<li><a href="{{ url('career') }}">{{ trans('layout.career-url') }}</a></li>
        							<li><a href="{{ url('faq') }}">{{ trans('layout.faq-url') }}</a></li>
        							<li><a href="{{ url('merchant') }}">{{ trans('layout.merchant-url') }}</a></li>
        							<li><a href="{{ url('lostandfound') }}">Lost & Found</a></li>
         						</ul>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
    </div>
    <!-- Copyrights
    ============================================= -->
    <div id="copyrights" class="nobg notoppadding">
    	<div class="container clearfix">
    		<div class="col_half">
    			&copy; 2016 All Rights Reserved by PopBox Asia Services
    			<br>
    			{{-- <div class="copyright-links"><a href="#">{{ trans('layout.terms') }}</a> / <a href="#">{{ trans('layout.privacy') }}</a></div> --}}
                <div class="copyright-links">
                    <a href="http://www.geoplugin.com/geolocation/" target="_new">IP Geolocation</a> by <a href="http://www.geoplugin.com/" target="_new">geoPlugin</a>
                </div>
    		</div>
    	</div>
    </div>
    <!-- #copyrights end -->
</footer>
<!-- #footer end -->

<script>
              window.fcSettings = {
                token: "889509b7-d3b7-4524-a6c4-35f960913593",
                host: "https://wchat.freshchat.com",
                tags : ["website"]
              };
            </script>
            <script src="https://wchat.freshchat.com/js/widget.js" async></script>

