<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>PopBox - Parcel Locker Indonesia</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="alternate" hreflang="id" href="https://www.popbox.asia/" />
        <meta name="google-site-verification" content="XKHAQF5mGFp8MPepA5H819Q4-d8J4HGZx11uSIO1oNk" />
        <meta name="author" content="PopBox Asia Team" />
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="img/favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        {{-- Standard --}}
        <meta name="description" content="PopBox is an automated parcel locker that allows you to send, receive, and return a parcel conveniently.">
        <meta name="keywords" content="parcel locker, electronic locker, loker otomatis, loker pintar, smart locker, kirim barang, logistik, delivery, fast delivery, parcel delivery, logistic, parcel, parcel locker indonesia, pengiriman cepat, kurir">
        <meta name="Robots" content="index, follow">
        {{-- Schema.org for G+ --}}
        <!-- Schema.org markup for Google+ -->
        <meta itemprop="name" content="PopBox Asia">
        <meta itemprop="description" content="PopBox is an automated parcel locker that allows you to send, receive, and return a parcel conveniently.">
        <meta itemprop="image" content="{{ asset('img/metaimage-popbox.jpg') }}">
        {{-- Twitter Card data --}}
        <meta name="twitter:card" content="PopBox is an automated parcel locker that allows you to send, receive, and return a parcel conveniently">
        <meta name="twitter:site" content="@PopBox_Asia">
        <meta name="twitter:title" content="PopBox - Parcel Collection Made Easy">
        <meta name="twitter:description" content="PopBox is an automated parcel locker that allows you to send, receive, and return a parcel conveniently.">
        <meta name="twitter:creator" content="@PopBox_Asia">
        <!-- Twitter summary card with large image must be at least 280x150px -->
        <meta name="twitter:image:src" content="{{ asset('img/metaimage-popbox.jpg') }}">
        {{-- OpenGraph --}}
        <meta property="og:title" content="PopBox - Parcel Collection Made Easy">
        <meta property="og:image" content="{{ asset('img/metaimage-popbox.jpg') }}">
        <meta property="og:description" content="PopBox is an automated parcel locker that allows you to send, receive, and return a parcel conveniently.">
        <meta property="og:url" content="{{ url('/') }}" />

        <link rel="stylesheet" href="{{ elixir('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/style.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/dark.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/app-landing.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/font-icons.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/animate.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/components/bs-switches.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/responsive.css') }}">

        <link rel="stylesheet" href="{{ elixir('css/swiper.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/css2/fonts.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/flag-icon.min.css') }}">
        <link rel="stylesheet" href="{{ elixir('css/custom.css') }}">
        <link rel="stylesheet" href="{{URL::asset('css/modify.css')}}">
        <link rel="stylesheet" href="/css/swc.css">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!--[if lt IE 9]>
            <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="{{ elixir('css/css2/colors.css') }}">
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-72128831-1', 'auto');
            ga('send', 'pageview');
        </script>
        <style type="text/css">
            .note-register{
                font-size: 12px;
                margin-top: -18px;
            }
        </style>
        <script type="application/ld+json">
            {
              "@context": "http://schema.org",
              "@type": "Organization",
              "url": "https://www.popbox.asia/",
              "logo": "https://www.popbox.asia/img/logo/logo.png",
              "contactPoint": [{
                "@type": "ContactPoint",
                "telephone": "+6221 2902 2537/8",
                "contactType": "customer service"
              }],
              "sameAs": [
                "https://www.facebook.com/pboxasia",
                "https://www.instagram.com/popbox_asia/",
                "https://www.linkedin.com/company-beta/9496334/",
                "https://twitter.com/popbox_asia",
                "https://www.youtube.com/channel/UCYGHLg4Xdc_IHD5DHdDIRTA"
              ]
            }
        </script>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-M48GKGL');</script>
        <!-- End Google Tag Manager -->

    </head>
    <body class="stretched">

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M48GKGL"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        {{-- Wrapper --}}
        <div id="wrapper" class="clearfix">
            {{-- header --}}
            @include('layout.header')
            {{-- Content --}}
            @yield('content')

            @php
                if (empty($citiesList)) {
                    $citiesList = \App\Http\Helper\Helper::getCities();
                }
            @endphp

            {{-- Modal Locker Avaibility --}}
            <div class="modal fade" id="modalLocker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-body">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">{{trans('layout.avaibility-btn') }}</h4>
                            </div>
                            <div class="modal-body">
                                <form id="availabilityForm" class="nobottommargin">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-process"></div>
                                    <span id="errorMsg" class="hide" style="color: red"></span>
                                    <div>
                                        <select class="form-control" name="cityName" id="modalCity">
                                            <option value="">{{ trans('layout.city-select') }}</option>
                                            @foreach ($citiesList as $element)
                                                <option value="{{ $element }}">{{ $element }}</option>
                                            @endforeach
                                        </select>
                                    </div> <br>
                                    <div class="hide" id="lockerDiv">
                                        <select class="form-control" name="lockerName">
                                            
                                        </select>
                                    </div> <br>
                                    <div class="hide" align="center" id="lockerAddress">
                                        <strong><span id="avail-address"></span></strong><br>
                                        <span id="avail-address_2"></span> <br>
                                        <span id="avail-district"></span>
                                    </div><br>
                                    <div class="hide row" id="lockerSize">
                                        <div class="col-md-1 col-lg-1 hidden-xs hidden-xm"></div>
                                        <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2" align="center">
                                            <img src="{{ asset('img/locker/box-xs.png') }}" class="imgLockerSize">
                                            <strong><span size="xs"></span></strong>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2" align="center">
                                            <img src="{{ asset('img/locker/box-s.png') }}" class="imgLockerSize">
                                            <strong><span size="s"></span></strong>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2" align="center">
                                            <img src="{{ asset('img/locker/box-m.png') }}" class="imgLockerSize">
                                            <strong><span size="m"></span></strong>
                                        </div>
                                        <div class="col-xs-2 col-sm-2 hidden-md hidden-lg"></div>
                                         <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2" align="center">
                                            <img src="{{ asset('img/locker/box-l.png') }}" class="imgLockerSize">
                                            <strong><span size="l"></span></strong>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2" align="center">
                                            <img src="{{ asset('img/locker/box-xl.png') }}" class="imgLockerSize">
                                            <strong><span size="xl"></span></strong>
                                        </div>
                                        <div class="col-md-1 col-lg-1 hidden-xs hidden-xm"></div>

                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Modal Tracking Parcel --}}
            <div class="modal fade" id="modalTracking" tabindex="-1" role="dialog" aria-labelledby="modalTrackingLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-body">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalTrackingLabel">Tracking Parcel</h4>
                            </div>
                            <div class="modal-body">
                                <form class="nobottommargin" id="trackForm" action="{{ url('track') }}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div align="center"><strong><span id="form-track-error" style="color:red"></span></strong></div>
                                    <div class="">
                                        <label for="name">{{ trans('tracking.parcel') }} <small>*</small></label>
                                        <input type="text" id="order" name="order" value="" class="sm-form-control required" />
                                    </div>
                                    <br>
                                    <div align="center">
                                        <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">{{ trans('tracking.parcel-btn') }}</button>
                                    </div>
                                    <div class="table-responsive hide" id="track-result">
                                        <table class="table cart">
                                            <tbody>
                                                <tr class="cart_item">
                                                    <td class="notopborder cart-product-name">
                                                        <strong>Locker</strong>
                                                    </td>

                                                    <td class="notopborder cart-product-name">
                                                        {{-- Locker Name --}}
                                                        <span class="amount" id="track-name"></span>
                                                    </td>
                                                </tr>
                                                <tr class="cart_item">
                                                    <td class="cart-product-name">
                                                        <strong>Status</strong>
                                                    </td>

                                                    <td class="cart-product-name">
                                                        {{-- Status --}}
                                                        <span class="amount color lead" id="track-status"><strong></strong></span>
                                                    </td>
                                                </tr>
                                                <tr class="cart_item">
                                                    <td class="cart-product-name">
                                                        <strong>Store Time</strong>
                                                    </td>

                                                    <td class="cart-product-name">
                                                        <span class="amount" id="track-store"></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                @if (isset($activateact))
                <div class="modal-on-load" data-target="#authchangepasswd"></div>
                
                <div class="modal1 mfp-hide" id="authchangepasswd">
                        <div class="block divcenter" style="background-color: #FFF; max-width: 700px;">
                            <div class="row nomargin clearfix">
                                <div class="col-sm-6" data-height-lg="456" data-height-md="456" data-height-sm="456" data-height-xs="0" data-height-xxs="0" style="background-image: url({{URL::asset("img/dashboard/img-login.jpg")}}); background-size: cover;"></div>
                                <div class="col-sm-6 col-padding" data-height-lg="456" data-height-md="456" data-height-sm="456" data-height-xs="456" data-height-xxs="456">
                                        <div class="change_password">
                                            <h4 class="uppercase ls1">Ubah Kata Sandi</h4>
                                            <div>Masukkan kata sandi kamu yang baru lalu coba untuk login kembali ke akun kamu</div>
                                            <form  class="clearfix" style="max-width: 300px;">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="col_full">                                                    
                                                    <input type="hidden" id="template-op-form-password" name="activate" value="{{$activation}}" class="sm-form-control" />
                                                </div><br/>

                                                <div class="col_full">
                                                    <label for="" class="capitalize t600">Kata Sandi Baru:</label>
                                                    <input type="password" id="template-op-form-password" name="password" value="" class="sm-form-control" />
                                                </div>
                                                <div class="col_full">
                                                    <label for="" class="capitalize t600">Ulang Kata Sandi:</label>
                                                    <input type="password" id="template-op-form-password" name="password_confirmation" value="" class="sm-form-control" />
                                                </div>                                                
                                                <div class="col_full nobottommargin">
                                                    <button type="button" class="button button-rounded button-small button-dark nomargin btn-change-pass" value="submit">UBAH</button>
                                                </div>
                                            </form>                                            
                                           <p class="nobottommargin">
                                                <small class="t300">
                                                    <div class="alert alert-success" role="alert">
                                                        abc
                                                    </div>
                                                </small></p>
                                        </div>                                       
                                                                 
                                </div>
                            </div>
                        </div>
                </div>
                @endif
                <div class="modal1 mfp-hide" id="authmodal">
                    <div class="block divcenter" style="background-color: #FFF; max-width: 700px;">
                        <div class="row nomargin clearfix">
                            <div class="col-sm-6" data-height-lg="456" data-height-md="456" data-height-sm="456" data-height-xs="0" data-height-xxs="0" style="background-image: url({{URL::asset("img/dashboard/img-login.jpg")}}); background-size: cover;"></div>
                                <div class="col-sm-6 col-padding" data-height-lg="456" data-height-md="456" data-height-sm="456" data-height-xs="456" data-height-xxs="456">
                                   
                                    <div class="login">
                                            <h4 class="uppercase ls1">Masuk untuk memulai order</h4>
                                            <form  class="clearfix" style="max-width: 300px;" id="formLogin">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="col_full">
                                                    <label for="" class="capitalize t600"> Email:</label>
                                                    <input type="text" id="template-op-form-email" name="email" value="" class="sm-form-control" name="email" />
                                                </div>
                                                <div class="col_full">
                                                    <label for="" class="capitalize t600">Kata Sandi:</label>
                                                    <input type="password" id="template-op-form-password" name="password" value="" class="sm-form-control" />
                                                </div>                                              

                                                <div class="row col_full">
                                                    <div class="col-md-6">
                                                        <label class="remind">
                                                            <input type="checkbox" name="remember" value="1" /> Ingat saya
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6 forgot-pass">
                                                        <label class="remind">
                                                            Lupa password
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col_full nobottommargin">
                                                    <button type="button" class="button button-rounded button-small button-dark nomargin btn-register" value="submit">Login</button>
                                                </div>
                                            </form>
                                            <div class="note-register">
                                                Punya toko online dan ingin kerjasama? <a href="/merchant/register">DAFTAR DI SINI</a>
                                            </div>
                                            <div class="nobottommargin alert alert-success" role="alert">     abc
                                                <div class="close-notif">close</div>
                                            </div>                                                
                                            
                                        </div>
                                        <div class="forgot">
                                            <h4 class="uppercase ls1">Lupa password ?</h4>
                                            <form class="clearfix" style="max-width: 300px;" id="formForgot">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="col_full">
                                                    Masukkan alamat email kamu, PopBox akan mengirimkan panduan untuk mengganti password kamu.
                                                </div>
                                                <div class="col_full">
                                                    <label for="" class="capitalize t600"> Email:</label>
                                                    <input type="text" id="template-op-form-email" name="credit_email" value="" class="sm-form-control" name="email" />
                                                </div>
                                                <div class="col_full nobottommargin">
                                                    <button type="button" class="button button-rounded button-small button-dark nomargin btn-forgot" value="submit">Kirim</button>
                                                </div>
                                            </form>
                                            <p class="nobottommargin">
                                                <small class="t300">
                                                    <div class="alert alert-success" role="alert">
                                                        abc
                                                    </div>
                                                </small></p>
                                        </div>
                                                                     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		<!--
		<div id="boxes">
                <div style="top: 50%; left: 50%; display: none;" id="dialog" class="window">
                    <div id="san">
                        <a href="#" class="agree"><img src="/img/close-icon.png" width="25" style="float:right; margin-right: -25px; margin-top: -20px; z-index: 99999999;"></a>
                        <img src="/img/hubungi-kami-pop-up-website.png" class="img-responsive" height="auto">
                    </div>
                </div>
                <div style="width: 2478px; font-size: 32pt; color:white; height: 1202px; display: none; opacity: 0.8;" id="mask"></div>
            </div>
		-->

            @include('layout.footer')

            <script type="text/javascript" src="{{ elixir('js/jquery.js') }}"></script>
            <script type="text/javascript" src="{{ elixir('js/plugins.js') }}"></script>
            <script type="text/javascript" src="{{ elixir('js/jquery.vmap.js') }}"></script>
            <script type="text/javascript" src="{{ elixir('js/vmap/jquery.vmap.world.js') }}"></script>
            <script type="text/javascript" src="{{ elixir('js/functions.js') }}"></script>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAijkBYFlPmwsLEAfvljffdrnhM7EStYF4&libraries=places"></script>
            <script type="text/javascript" src="{{ elixir('js/jquery.gmap.js') }}"></script>
            <script type="text/javascript" src="{{ elixir('js/validation.js') }}"></script>
            <script type="text/javascript" src="js/swc.js"></script>
            {{-- <script type="text/javascript" src="{{ elixir('js/instafeed.min.js') }}"></script> --}}
            {{-- <script type="text/javascript">
                $(window).load(function() {
                    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
                    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
                    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
                    $.src="//v2.zopim.com/?3CMq6rSa2wBbjBh1EFcIn7iXdVvaFeiK";z.t=+new Date;$.
                    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
                });
            </script> --}}
            
            @yield('js')

            <script type="text/javascript">
                jQuery(document).ready(function($) {

                    $(".forgot-pass").click(function(){
                        $(".login").hide();
                        $(".forgot").show();                
                    })

                    $(".openlogin").click(function(){                 
                        $(".login").show();
                        $(".forgot").hide();       
                        $(".alert").text("");
                        $(".alert").hide();         
                    })

                    $(".btn-change-pass").click(function(){
                        var activate = $("input[name='activate']").val();
                        var password1 = $("input[name='password']").val();
                        var password2 = $("input[name='password_confirmation']").val();
                        if (password1!=password2){
                            $(".alert").removeClass("alert-danger");
                            $(".alert").removeClass("alert-success");
                            $(".alert").addClass("alert-danger");
                            $(".alert").text("password tidak sama");
                            $(".alert").show();    
                            return;
                        }
                        var token = $("input[name='_token']").val();
                        var dataString = 'activate='+ activate + '&password=' + password1 + "&password_confirmation=" + password2 + "&_token=" + token;
                        console.log(dataString);
                        $.ajax({
                            url: "/user/doreset",
                            data: dataString,
                            type: "POST",
                            success: function(result){
                                console.log(result);
                                if (result.status=="success"){
                                     window.location = result.url;
                                }else{
                                    $(".alert").removeClass("alert-success");
                                    $(".alert").addClass("alert-danger");
                                    $(".alert").text(result.message);
                                    $(".alert").show();    
                                }
                            },
                            error: function (){}
                        });
                    })

                    $(".btn-forgot").click(function(){
                        var email = $("input[name='credit_email']").val();     
                        var token = $("input[name='_token']").val();          
                        var dataString = 'credit_email='+ email + "&_token=" + token;       
                        var dataString = $('#formForgot').serialize();            
                        $.ajax({
                            url: "/user/request",
                            data: dataString,
                            type: "POST",
                            success: function(result){                        
                                if (result.status=="success"){
                                    $(".alert").removeClass("alert-danger");
                                    $(".alert").removeClass("alert-success");
                                    $(".alert").addClass("alert-success");
                                    $(".alert").text(result.message);
                                    $(".alert").show();
                                }else{
                                    $(".alert").removeClass("alert-danger");
                                    $(".alert").removeClass("alert-success");
                                    $(".alert").addClass("alert-danger");
                                    $(".alert").text(result.message);
                                    $(".alert").show();
                                }
                            },
                            error: function (){}
                        });
                    })

                     $(".btn-register").click(function(){
                        var email = $("input[name='email']").val();
                        var password = $("input[name='password']").val();
                        var token = $("input[name='_token']").val();
                        var dataString = 'email=' + email + '&password=' + password + '&_token=' + token;
                        if ($("input[name='remember']").is(':checked') ) {
                            dataString = dataString + '&remember=' + $("input[name='remember']").val(); 
                            //remember = remember + '&remember=' + $("input[name='remember']").val();                        
                        }                        
                        var dataString = $('#formLogin').serialize();      
                        
                        $.ajax({
                            url: "/user/signin",
                            data: dataString,
                            type: "POST",
                            success: function(result){                        
                                console.log(result);
                                if (result.status=="success"){
                                    window.location = result.url;
                                }else{
                                    $(".alert").removeClass("alert-success");
                                    $(".alert").addClass("alert-danger");
                                    $(".alert").text(result.message);
                                    $(".alert").show();                            
                                }
                            },
                            error: function (){}
                        });
                    })

                    $(".close-notif").click(function(){
                        $(".alert").hide();
                    })

                    // City on change get locker list
                    $('select[id=modalCity]').on('change', function(event) {
                        /* Act on the event */
                        $('#errorMsg').addClass('hide');
                        $('select[name=lockerName]').html('');
                        $('#lockerDiv').addClass('hide');
                        $('#lockerSize').addClass('hide');
                        $('#lockerAddress').addClass('hide');

                        $.ajax({
                            url: '{{ url('lockerList') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: $('#availabilityForm').serialize(),
                            success: function(data){
                                if (data.isSuccess==true) {
                                    $('#contactform').find('.form-process').fadeOut();
                                    var lockerList = data.data;
                                    var option = '';
                                    $.each(lockerList, function(index, val) {
                                         /* iterate through array or object */

                                         var address = val.address;
                                         var address_2 = val.address_2;
                                         var district = val.district+', '+val.province;
                                         option += "<option value='"+val.locker_id+"' address='"+address+"' address_2='"+address_2+"' district='"+district+"'>"+val.name+"</option>"
                                    });
                                    $('select[name=lockerName]').html(option);
                                    $('#lockerDiv').removeClass('hide');
                                    $('select[name=lockerName]').trigger('change');
                                } else {
                                    $('#contactform').find('.form-process').fadeOut();
                                    $('#errorMsg').html(data.errorMsg);
                                    $('#errorMsg').removeClass('hide');
                                }
                            }
                        })

                        .done(function() {
                            $('#availabilityForm').find('.form-process').fadeOut();
                            console.log("success");
                        })
                        .fail(function() {
                            $('#availabilityForm').find('.form-process').fadeOut();
                            console.log("error");
                        })
                        .always(function() {
                            $('#availabilityForm').find('.form-process').fadeOut();
                            console.log("complete");
                        });         
                    });   

                    // Locker on change get size list
                    $('select[name=lockerName]').on('change', function(event) {
                        /* Act on the event */
                        $('#lockerSize').addClass('hide');
                        $('#lockerAddress').addClass('hide');

                        var locker = $(this).find(':selected');
                        var address = locker.attr('address');
                        var address_2 = locker.attr('address_2');
                        var district = locker.attr('district');

                        $.ajax({
                            url: '{{ url('lockerAvailabilityByID') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: $('#availabilityForm').serialize(),
                            success: function(data){
                                if (data.isSuccess==true) {
                                    $('#contactform').find('.form-process').fadeOut();
                                    $('span[size=xs]').html(data.data.XS);
                                    $('span[size=s]').html(data.data.S);
                                    $('span[size=m]').html(data.data.M);
                                    $('span[size=l]').html(data.data.L);
                                    $('span[size=xl]').html(data.data.XL);
                                    $('#avail-address').html(address);
                                    $('#avail-address_2').html(address_2);
                                    $('#avail-district').html(district);
                                    $('#lockerAddress').removeClass('hide');
                                    $('#lockerSize').removeClass('hide');
                                } else {
                                    $('#contactform').find('.form-process').fadeOut();
                                    if (data.errorMsg==undefined) {
                                        $('#errorMsg').html('Data Locker is Empty.');
                                    } else {
                                        $('#errorMsg').html(data.errorMsg);
                                    }
                                    $('#errorMsg').removeClass('hide');
                                }
                            }
                        })
                        .done(function() {
                            $('#availabilityForm').find('.form-process').fadeOut();
                            console.log("success");
                        })
                        .fail(function() {
                            $('#availabilityForm').find('.form-process').fadeOut();
                            $('#errorMsg').html('Failed to get Data Locker');
                            console.log("error");
                        })
                        .always(function() {
                            $('#availabilityForm').find('.form-process').fadeOut();
                            console.log("complete");
                        });
                    });

                    // Tracking Locker
                    $('#trackForm').on('submit', function(event) {
                        event.preventDefault();
                        $('#track-result').addClass('hide');
                        $.ajax({
                            url: '{{ url('track') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: $('#trackForm').serialize(),
                            success: function(data){
                                if (data.isSuccess==true) {
                                    data = data.data;
                                    $('#trackForm').find('.form-process').fadeOut();
                                    $('#track-name').html(data.locker_name);
                                    $('#track-status').html(data.status);
                                    $('#track-store').html(data.storetime);
                                    $('#track-result').removeClass('hide');
                                } else {
                                    $('#trackForm').find('.form-process').fadeOut();
                                    $('#form-track-error').html(data.errorMsg);
                                    $('#form-track-error').removeClass('hide');
                                }
                            }
                        })
                        .done(function() {
                            $('#trackForm').find('.form-process').fadeOut();
                            console.log("success");
                        })
                        .fail(function() {
                            $('#trackForm').find('.form-process').fadeOut();
                            console.log("error");
                        })
                        .always(function() {
                            $('#trackForm').find('.form-process').fadeOut();
                            console.log("complete");
                        });    
                    });
                });
            </script>
        </div> 
        {{-- End Wrapper --}}
    </body>
</html>
