@extends('layout.main-article')
@section('content')
	<section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('img/bg/bg-faq.jpg'); padding: 120px 0;" data-stellar-background-ratio="0.3">
        <div class="container clearfix">
            <h1>FAQ</h1>
            <span>_______</span>
        </div>
    </section>
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix divcenter">
                <!-- Post Content
                ============================================= -->
                <div class="postcontent nobottommargin clearfix">
                    <ul id="portfolio-filter" class="portfolio-filter clearfix">
                        {{-- <li><a href="#" data-filter="all">All</a></li> --}}
                        <li class="activeFilter"><a href="#" data-filter=".faq-general">PopBox</a></li>
                        <li><a href="#" data-filter=".faq-parcel">{{ trans('faq.ambil-btn') }}</a></li>
                        @if ($lang == 'my')
                            <li><a href="#" data-filter=".faq-deliver">Deliver Parcel</a></li>
                        @endif
                        <li><a href="#" data-filter=".faq-return">{{ trans('faq.return-btn') }}</a></li>
                        @if ($lang == 'id')
                            <!-- <li><a href="#" data-filter=".faq-cod">COD</a></li>
                            <li><a href="#" data-filter=".faq-popshop">PopShop</a></li>
                            <li><a href="#" data-filter=".faq-popsend">PopSend</a></li> -->
                        @endif
                    </ul>
                    <div class="clear"></div>
                    <div id="faqs" class="faqs">
                        <div class="toggle faq faq-general">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.apaitu-title') }}</div>
                            <div class="togglec">{{ trans('faq.apaitu-content') }}</div>
                        </div>
                        <div class="toggle faq faq-general">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.kenapa-title') }}</div>
                            <div class="togglec">
                                <ul>
                                    @if ($lang == 'my')
                                        <li>{{ trans('faq.kenapa-sub-conv') }}</li>
                                        <p>{{ trans('faq.kenapa-sub-conv-content') }}</p>
                                    @endif
                                    <li>{{ trans('faq.kenapa-sub-1') }}</li>
                                    <p>{{ trans('faq.kenapa-sub-1-content') }}</p>
                                    <li>{{ trans('faq.kenapa-sub-2') }}</li>
                                    <p>{{ trans('faq.kenapa-sub-2-content') }}</p>
                                    <li>{{ trans('faq.kenapa-sub-3') }}</li>
                                    <p>{{ trans('faq.kenapa-sub-3-content') }}</p>
                                </ul>
                            </div>
                        </div>
                        <div class="toggle faq faq-general">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.dimana-title') }}</div>
                            <div class="togglec">{!! trans('faq.dimana-content') !!}</div>
                        </div>
                        <div class="toggle faq faq-general">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.ukuran-title') }}</div>
                            <div class="togglec">
                                {!! trans('faq.ukuran-content') !!}
                                <img src="{{ asset('img/popbox-lockersize.jpg') }}">
                            </div>
                        </div>
                        <div class="toggle faq faq-general">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.asuransi-title') }}</div>
                            <div class="togglec">{{ trans('faq.asuransi-content') }}</div>
                        </div>
                        <div class="toggle faq faq-general">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.aman-title') }}</div>
                            <div class="togglec">{{ trans('faq.aman-content') }}</div>
                        </div>
                        
                        @if ($lang == 'my')
                        <div class="toggle faq faq-general">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.aman-title-2') }}</div>
                            <div class="togglec">{{ trans('faq.aman-content-2') }}</div>
                        </div>
                        <div class="toggle faq faq-general">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.aman-title-3') }}</div>
                            <div class="togglec">
                            {!! trans('faq.aman-content-3') !!}
                            </div>
                        </div>
                        @endif

                        {{-- Parcel --}}
                        <div class="toggle faq faq-parcel" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.beli-title') }}</div>
                            <div class="togglec">{!! trans('faq.beli-content') !!}</div>
                        </div>
                        <div class="toggle faq faq-parcel" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.status-title') }}</div>
                            <div class="togglec">{{ trans('faq.status-content') }}</div>
                        </div>

                        @if ($lang == 'my')
                            <div class="toggle faq faq-parcel" style="display: none;">
                                <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.parcel-bigger-title') }}</div>
                                <div class="togglec">{{ trans('faq.parcel-bigger-content') }}</div>
                            </div>
                        
                            <div class="toggle faq faq-parcel" style="display: none;">
                                <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.pin-code-sms-title') }}</div>
                                <div class="togglec">{!! trans('faq.pin-code-sms-content') !!}</div>
                            </div>
                            
                            <div class="toggle faq faq-parcel" style="display: none;">
                                <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.wrong-code-title') }}</div>
                                <div class="togglec">{!! trans('faq.wrong-code-content') !!}</div>
                            </div>

                            <div class="toggle faq faq-parcel" style="display: none;">
                                <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.why-notpin-title') }}</div>
                                <div class="togglec">{{ trans('faq.why-notpin-content') }}</div>
                            </div>
                        @endif

                        <div class="toggle faq faq-parcel" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.lama-title') }}</div>
                            <div class="togglec">{{ trans('faq.lama-content') }}</div>
                        </div>
                        {{-- Return --}}
                        <div class="toggle faq faq-return" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.merchant-return-title') }}</div>
                            <div class="togglec">{{ trans('faq.merchant-return-content') }}</div>
                        </div>
                        <div class="toggle faq faq-return" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.cara-return-title') }}</div>
                            <div class="togglec">{!! trans('faq.cara-return-content') !!}</div>
                        </div>
                        <div class="toggle faq faq-return" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.biaya-return-title') }}</div>
                            <div class="togglec">{{ trans('faq.biaya-return-content') }}</div>
                        </div>
                        <div class="toggle faq faq-return" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.status-return-title') }}</div>
                            <div class="togglec">{{ trans('faq.status-return-content') }}</div>
                        </div>
                        {{-- COD --}}
                        <div class="toggle faq faq-cod" style="display:none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.apa-cod-title') }}</div>
                            <div class="togglec">{{ trans('faq.apa-cod-content') }}</div>
                        </div>
                        <div class="toggle faq faq-cod" style="display:none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.loker-cod-title') }}</div>
                            <div class="togglec">{{ trans('faq.loker-cod-content') }}</div>
                        </div>
                        <div class="toggle faq faq-cod" style="display:none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.bayar-cod-title') }}</div>
                            <div class="togglec">{!! trans('faq.bayar-cod-content') !!}</div>
                        </div>
                        <div class="toggle faq faq-cod" style="display:none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.cara-cod-title') }}</div>
                            <div class="togglec">{!! trans('faq.cara-cod-content') !!}</div>
                        </div>
                        <div class="toggle faq faq-cod" style="display:none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.lama-cod-title') }}</div>
                            <div class="togglec">{{ trans('faq.lama-cod-content') }}</div>
                        </div>
                        {{-- PopShop --}}
                        <div class="toggle faq faq-popshop" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.apa-popshop-title') }}</div>
                            <div class="togglec">{!! trans('faq.apa-popshop-content') !!}</div>
                        </div>
                        <div class="toggle faq faq-popshop" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.cara-popshop-title') }}</div>
                            <div class="togglec">{!! trans('faq.cara-popshop-content') !!}</div>
                        </div>
                        {{-- PopSend --}}
                        <div class="toggle faq faq-popsend" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.apa-popsend-title') }}</div>
                            <div class="togglec">{!! trans('faq.apa-popsend-content') !!}</div>
                        </div>
                        <div class="toggle faq faq-popsend" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.cara-popsend-title') }}</div>
                            <div class="togglec">{!! trans('faq.cara-popsend-content') !!}</div>
                        </div>
                        <div class="toggle faq faq-popsend" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.lama-popsend-title') }}</div>
                            <div class="togglec">{{ trans('faq.lama-popsend-content') }}</div>
                        </div>

                        {{-- Delivery --}}
                        <div class="toggle faq faq-deliver" style="display: none;">
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.deliver-parcel-title-1') }}</div>
                            <div class="togglec">{{ trans('faq.deliver-parcel-content-1') }}</div>
                            
                            <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>{{ trans('faq.deliver-parcel-title-2') }}</div>
                            <div class="togglec">{{ trans('faq.deliver-parcel-content-2') }}</div>
                        </div>
                    </div>
                </div>
                <!-- .postcontent end -->
            </div>
        </div>
    </section>
    <!-- #content end -->
@stop
@section('js')
	<script type="text/javascript">
        jQuery(document).ready(function($){
            var $faqItems = $('#faqs .faq');
            if( window.location.hash != '' ) {
                var getFaqFilterHash = window.location.hash;
                console.log(getFaqFilterHash);
                var hashFaqFilter = getFaqFilterHash.split('#');
                console.log(hashFaqFilter)
                if( $faqItems.hasClass( hashFaqFilter[1] ) ) {
                    $('#portfolio-filter li').removeClass('activeFilter');
                    $( '[data-filter=".'+ hashFaqFilter[1] +'"]' ).parent('li').addClass('activeFilter');
                    var hashFaqSelector = '.' + hashFaqFilter[1];
                    $faqItems.css('display', 'none');
                    if( hashFaqSelector != 'all' ) {
                        $( hashFaqSelector ).fadeIn(500);
                    } else {
                        $faqItems.fadeIn(500);
                    }
                }
            }

            $('#portfolio-filter a').click(function(){
                $('#portfolio-filter li').removeClass('activeFilter');
                $(this).parent('li').addClass('activeFilter');
                var faqSelector = $(this).attr('data-filter');
                $faqItems.css('display', 'none');
                if( faqSelector != 'all' ) {
                    $( faqSelector ).fadeIn(500);
                } else {
                    $faqItems.fadeIn(500);
                }
                return false;
           });
        });
    </script>
@stop