@extends('layout.main-article')
@section('content')
    <link rel="icon" type="image/png" href="img/favicon.ico">
    <section class="page-title-dark" style="background-image: url('{{ asset('img/return/new_return_banner.jpg') }}'); width: 100%; height: 414px;" data-stellar-background-ratio="0.3">
        <div class="container clearfix">
            <h1 style="color: #fff; font-size: 40px; font-weight: 600; padding-top: 150px; letter-spacing: 2px;">PopBox Return</h1>
        </div>
    </section>
    <style type="text/css">
    	@media only screen and (min-width:480px){
    		.merchant{
    			margin-right: 0px; 
    			width: 33.3333%; 
    		}
    	}
    	.card{
	        width: 100%;
	        min-height: 400px;
	        padding: 15px 15px;
	        border: 1px solid #DDDDDD;
	        border-radius: 10px;
	        margin-top: 30px;
	        text-align: center;
	        background: #fff;
	   }

	   .landing-page .section-popbox .card{
	        width: 100%;
	        min-height: 340px;
	        padding: 10px 0 0px 0;
	        border: 1px solid #DDDDDD;
	        border-radius: 0px;
	        margin-top: 30px;
	        text-align: center;
	        background: #FFFFFF;
	   }

	   .landing-page .section-benefit .card{
	        width: 100%;
	        min-height: 340px;
	        padding: 10px 0 0px 0;
	        border-radius: 0px;
	        margin-top: 30px;
	        text-align: center;
	   }

	   .card .icon i{
	        font-size: 63px;
	        color: #9a9a9a;
	        width: 85px;
	        height: 85px;
	        line-height: 80px;
	        text-align: center;
	        border-radius: 50%;
	        margin: 0 auto;
	        margin-top: 20px;
	   }

	    .card.card-blue{
	      border-color: #0C87CC;
	    }
	    .card.card-blue .icon i{
	      color:#00bbff;
	    }

	    .landing-page .section-popbox .card.card-white{
	      border-color: #FFFFFF;
	    }
	    .landing-page .section-popbox .card.card-blue{
	      border-color: #0C87CC;
	      background-color: #0C87CC;
	      color: #ffffff;
	    }
	    .card.card-white .icon i{
	      color:#00bbff;
	    }

	    .card.card-green{
	      border-color: #BDF8C0;
	    }
	    .card.card-green .icon i{
	      color:#3ABE41;
	    }
	    .card.card-orange{ 
	      border-color: #FAD9CD;
	    }
	    .card.card-orange .icon i{
	      color:#ff967f;
	    }
	     .card.card-red{ 
	      border-color: #FFBFC3;
	    }
	    .card.card-red .icon i{
	      color:#E01724;
	    }
	    .card  h4{
	        font-weight: 300;
	        font-size: 18px;
	        margin: 20px 0 15px;
	        color: #000;
	   }

	   .card  p{
	        font-size: 14px;
	        line-height: 22px;
	        font-weight: 400;
	        color: #000;
	   }
        .gMap-controls{
            margin-top: 10px;
            border: 1px solid transparent;
            border-radius: 25px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            width: 30%;
        }

        .gMap-controls:focus{
            box-shadow: 0 0 5px rgba(12, 135, 204, 1);
        }

        #pac-input{
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
        }

    </style>
    <section id="content">
	    <div class="content-wrap">
			<div class="container clearfix">
				<div class="nobottommargin">
					<div class="col_full clearfix" align="center">
						<h2>GRATIS! PENGEMBALIAN BARANG VIA POPBOX</h2>
						<h4>
							Tidak perlu khawatir lagi jika barang yang kamu beli salah ukuran, salah warna, atau masalah lainnya. Kamu cukup kembalikan barang yang kemu beli lewat PopBox. Mudah dan seluruh proses tidak memakan waktu lebih dari 5 detik.
						</h4>
					</div>
					<div class="row" align="center">
						<h3>PopBox bekerjasama dengan eCommerce berikut untuk pengembalian barang.</h3>
						<div class="col-md-4">
							 <div class="feature-box media-box">
		                        <div class="fbox-media">
		                            <a href="http://blibli.popbox.asia/"><img src="{{ asset('img/return/return-blibli.png') }}" style="width: 80%;"></a>
		                        </div>
		                    </div>
						</div>
						<!-- <div class="col-md-6">
							 <div class="feature-box media-box">
		                        <div class="fbox-media">
		                            <a href="http://pr.popbox.asia/mm"><img src="img/return/mataharimall.png" style="width: 80%;"></a>
		                        </div>
		                    </div>
						</div> -->
						<!-- <div class="col-md-6">
							 <div class="feature-box media-box">
		                        <div class="fbox-media">
		                            <a href="http://lazada.popbox.asia/"><img src="img/return/lazada.png" style="width: 80%;"></a>
		                        </div>
		                    </div>
						</div> -->
                        <!-- <div class="col-md-3">
							 <div class="feature-box media-box">
		                        <div class="fbox-media">
		                            <a href="#"><img src="img/return/return-farmaku.png" style="width: 80%;"></a>
		                        </div>
		                    </div>
						</div> -->
                        <div class="col-md-4">
							 <div class="feature-box media-box">
		                        <div class="fbox-media">
		                            <a href="{{ url('return') }}#"><img src="{{ asset('img/return/return-javamifi.png') }}" style="width: 80%;"></a>
		                        </div>
		                    </div>
						</div>
						<div class="col-md-4">
							 <div class="feature-box media-box">
		                        <div class="fbox-media">
		                            <a href="{{ url('return') }}#"><img src="{{ asset('img/return/return-zalora.png') }}" style="width: 80%;"></a>
                                    <!-- http://pr.popbox.asia/zalora -->
		                        </div>
		                    </div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		{{-- Cara Pengembalian --}}
        <div class="section section-white section-features">
            <div class="container">
                <h2 class="header-text text-center howto-title">Cara Pengembalian Barang Melalui PopBox</h2>
                <div class="row">
                    <div class="col-md-3">
                        <div class="card card-white">
                            <div class="icon">
                                <img src="img/return/langkah1.png">
                            </div>
                            <div class="text">
                                <h4><strong>Langkah 1</strong></h4>
                                <p>Lengkapi formulir pengembalian produk di toko online tempat kamu melakukan transaksi
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card card-white">
                            <div class="icon">
                                <img src="img/return/langkah2.png">
                            </div>
                            <h4><strong>Langkah 2</strong></h4>
                            <p>
                                Kemas produk dengan rapi lengkap dengan pelindung. Tempel label pengembalian/ beri label pengiriman berisi No pengembalian dan No handphone Anda.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card card-white">
                            <div class="icon">
                                <img src="img/return/langkah3.png">
                            </div>
                            <h4><strong>Langkah 3</strong></h4>
                            <p>Kirim produk yang ingin dikembalikan ke kantor Blibli.com melalui loker PopBox terdekat. <br> <br> Pilih menu <strong></strong>“Mengembalikan Barang”</strong> dan lanjutkan proses untuk meletakkan paket di loker.</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card card-white">
                            <div class="icon">
                                <img src="img/return/langkah4.png">
                            </div>
                            <h4><strong>Langkah 4</strong></h4>
                            <p>Setelah pintu loker ditutup, Anda akan mendapatkan SMS bukti pengiriman. <br><br> Kurir PopBox akan mengambil paket dan mengantar paket ke alamat pengembalian</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Proses Drop --}}
        <div class="section section-clients section-gray ">
            <div class="container">
                <h2 class="header-text text-center howto-title">Proses Pengembalian Di Loker</h2>
                <div class="row">
                    {{-- Step 1 --}}
                    <div class="col-md-4" style="">
                        <img src="{{ asset('img/return/1.jpg') }}" style="max-width: 100%;">
                    </div>
                    {{-- Step 2 --}}
                    <div class="col-md-4" style="">
                        <img src="{{ asset('img/return/2.jpg') }}" style="max-width: 100%;">
                    </div>
                    {{-- Step 3 --}}
                    <div class="col-md-4" style="">
                        <img src="{{ asset('img/return/3.png') }}" style="max-width: 100%;">
                    </div>
                </div> <br>
                <div class="row">
                	{{-- Step 4 --}}
                    <div class="col-md-4" style="">
                        <img src="{{ asset('img/return/4.jpg') }}" style="max-width: 100%;">
                    </div>
                    {{-- Step 5 --}}
                    <div class="col-md-4" style="">
                        <img src="{{ asset('img/return/5.jpg') }}" style="max-width: 100%;">
                    </div>
                    {{-- Step 6 --}}
                    <div class="col-md-4" style="">
                        <img src="{{ asset('img/return/6.jpg') }}" style="max-width: 100%;">
                    </div>
                </div> <br>
                <div class="row">
                	{{-- Step 7 --}}
                    <div class="col-md-4" style="">
                        <img src="{{ asset('img/return/7.jpg') }}" style="max-width: 100%;">
                    </div>
                    {{-- Step 8 --}}
                    <div class="col-md-4" style="">
                        <img src="{{ asset('img/return/8.jpg') }}" style="max-width: 100%;">
                    </div>
                    {{-- Step 9 --}}
                    <div class="col-md-4" style="">
                        <img src="{{ asset('img/return/9.jpg') }}" style="max-width: 100%;">
                    </div>
                </div>
            </div>
        </div>
    </section>

    @php
        if (empty($citiesList)) {
            $citiesList = \App\Http\Helper\Helper::getCities();
        }
    @endphp
    <!-- 
    <div class="section section-gray section-popbox">
        <div class="row" id="divFindLocker">
            <div class="col-md-12 text-center">
                <h2 class="header-text text-center howto-title">Lokasi Loker PopBox</h2>
                <h3>Silahkan cari yang terdekat dengan lokasi Anda</h3>
                <form id="mapLocker">
                    {{ csrf_field() }}
                    <div class="hidden-xs hidden-sm col-md-2 col-lg-2"></div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <select class="form-control" id="mapCity" name="cityName">
                            <option value="">Semua Kota</option>
                            @foreach ($citiesList as $element)
                                <option value="{{ $element }}">{{ $element }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <select class="form-control" name="lockerFind">

                        </select>
                    </div>
                    <div class="hidden-xs hidden-sm col-md-2 col-lg-2"></div>
                </form>
            </div>
        </div> <br>
        
        <div class="row">
            <div class="col-md-12">
                <input id="pac-input" class="gMap-controls" type="text" placeholder="Cari Loker">
                <div id="nearest_locker" style="height: 500px;" class="gmap"></div>
            </div>
        </div>
    </div>
 -->

    {{-- Section Locker Nearest --}}
    <section id="section-location" class="section-full">
        <div class="heading-block center">
            <h2>{{ trans('index.location-title') }}</h2>
            {{-- <span>{{ trans('index.location-sub') }}</span> --}}
            {{-- <a href="https://location.popbox.asia/"> --}}
            <button class="button button-circle nearme">{{ trans('index.location-btn') }}</button><br>
            <span id="btnFindLocker"><i class="icon-angle-down infinite animated" id="iFindLocker" style="font-size: 150%"></i></span>
            {{-- <button class="btn btn-sm" id="btnFindLocker"></button> <br> <br> --}}
            {{-- </a> --}}
            <div class="row hide" id="divFindLocker">
                <form id="mapLocker">
                    {{ csrf_field() }}
                    <div class="hidden-xs hidden-sm col-md-2 col-lg-2"></div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <select class="form-control" id="mapCity" name="cityName">
                            <option value="">{{ trans('layout.city-select') }}</option>
                            @foreach ($citiesList as $element)
                                <option value="{{ $element }}">{{ $element }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <select class="form-control" name="lockerFind">
                                                
                        </select>
                    </div>
                    <div class="hidden-xs hidden-sm col-md-2 col-lg-2"></div>
                </form>
            </div>
        </div>
        <input id="pac-input" class="gMap-controls" type="text" placeholder="{{ trans('index.location-search') }}">
        <div id="nearest_locker" style="height: 500px; margin-bottom: 20px;" class="gmap"></div>
    </section>

    <div class="content-wrap">
        <div class="container">
            <h2 class="header-text text-center howto-title">Menjadi Partner PopBox</h2>
            <h4>
                Ingin bekerjasama dengan PopBox untuk layanan kamu, silahkan hubungi tim kami di <font color="red">info@popbox.asia</font>  dengan subject "Partnership Return (Nama Brand) <> PopBox"
            </h4>
        </div>
    </div>
@stop

    @section('js')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAijkBYFlPmwsLEAfvljffdrnhM7EStYF4&libraries=places"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.gmap.js') }}"></script>
    <script type="text/javascript">
        var lockerList = {!! json_encode($lockerJkt) !!};
    </script>
    <script type="text/javascript" async>
        var markers = Array();
        var url_img = "{{ asset('img/box/ic_box_map.svg') }}";
        var newLoc = false;

        $.each(lockerList,function (key,value) {
            var html = '<div align="center">';
            html += '<strong style="color:#FF6300;font-size:15px;">'+value.name+'</strong><br>';
            html += '<strong style="font-size:14px;">'+value.address+'</strong><br>';
            html += value.address_2+'<br>';
            html += value.operational_hours;
            html+='</div>';
            var iconMarker = '{{ asset('img/map/marker-locker.png') }}'

            var locker = {
                latitude : value.latitude,
                longitude : value.longitude,
                html : html,
                icon : {
                    image : iconMarker,
                    iconsize: [30, 49],
                    shadowsize: [37, 34],
                    iconanchor: [9, 34],
                    shadowanchor: [19, 34]
                }
            }
            markers.push(locker);
        });

        $('#nearest_locker').gMap({
            address: 'Jakarta',
            maptype: 'ROADMAP',
            zoom: 11,
            markers: markers,
            doubleclickzoom: false,
            controls: {
                panControl: true,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: true,
                streetViewControl: false,
                overviewMapControl: true
            }
        }).gMap('addSearchBox',{
            box : 'pac-input'
        }).gMap('addBtn',{
            location : 'Jakarta',
            text : 'Semua Loker',
            zoom : 11
        })
        $(document).ready(function() {
            $('button[class*=nearme]').on('click', function(event) {
                nearestLock();
            });

            $(".btn-register").click(function(){
                $('html, body').animate({ scrollTop: $('#registration').offset().top }, 'slow');
            })
        });


        function nearestLock(){
            var iconPosition = '{{ asset('img/map/market-youarehere.png') }}'
            if (navigator.geolocation) {
                if (newLoc==false) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        jQuery('#nearest_locker').gMap('addMarker', {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude,
                            icon : {
                                image : iconPosition,
                                iconsize: [40, 64],
                                iconanchor: [9, 34],
                            },
                        })
                    })
                }
                navigator.geolocation.getCurrentPosition(function(position) {
                    jQuery('#nearest_locker').gMap('centerAt', {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        zoom: 14
                    });
                    newLoc = true;
                }, function() {
                    console.log('Failed Geo Location')
                });
            }
        }

        $(document).ready(function() {

            $('#iFindLocker').on('click', function(event) {
                if ($('#divFindLocker').hasClass('hide')) {
                    $('#divFindLocker').removeClass('hide');
                    $('#iFindLocker').removeClass('icon-angle-down');
                    $('#iFindLocker').addClass('icon-angle-up');

                    // $('#iFindLocker').removeClass('fadeInDown');
                    // $('#iFindLocker').addClass('fadeInUp');
                } else {
                    $('#divFindLocker').addClass('hide');
                    $('#iFindLocker').removeClass('icon-angle-up');
                    $('#iFindLocker').addClass('icon-angle-down');

                    // $('#iFindLocker').removeClass('fadeInUp');
                    // $('#iFindLocker').addClass('fadeInDown');
                }
            });

            $('select[id=mapCity]').on('change', function(event) {
                /* Act on the event */
                $('select[name=lockerFind]').html('');
                $.ajax({
                    url: '{{ url('lockerList') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: $('#mapLocker').serialize(),
                    success: function(data){
                        if (data.isSuccess==true) {
                            var lockerList = data.data;
                            var option = '';
                            $.each(lockerList, function(index, val) {
                                /* iterate through array or object */
                                var address = val.address;
                                var address_2 = val.address_2;
                                var district = val.district+', '+val.province;
                                option += "<option value='"+val.name+"' lat='"+val.latitude+"' long='"+val.longitude+"'>"+val.name+"</option>"
                            });
                            $('select[name=lockerFind]').html(option);
                            $('select[name=lockerFind]').trigger('change');
                        } else {
                            $('#errorMsg').html(data.errorMsg);
                        }
                    }
                })
                    .done(function() {
                        console.log("success");
                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });
            });

            $('select[name=lockerFind]').on('change', function(event) {
                /* Act on the event */
                $('#lockerSize').addClass('hide');
                $('#lockerAddress').addClass('hide');
                var locker = $(this).find(':selected');
                var lat = locker.attr('lat');
                var long = locker.attr('long');
                jQuery('#nearest_locker').gMap('centerAt', {
                    latitude: lat,
                    longitude: long,
                    zoom: 16
                });
            });
        });
    </script>
@stop
