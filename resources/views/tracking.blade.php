@extends('layout.main-article')
@section('content')
    <section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url({{ url('img/banner.png') }}); padding: 120px 0;" data-stellar-background-ratio="0.3">
        <div class="container clearfix">
            <h1>{{ trans('tracking.title') }}</h1>
            <span>{{ trans('tracking.sub') }}</span>
        </div>
    </section>
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <!-- Postcontent -->
				<div class="nobottommargin">
                <h3>{{ trans('tracking.search-awb') }}</h3>
                <div class="contact-widget">
                    <div class="contact-form-result"></div>
                    <form class="nobottommargin" id="trackForm" name="template-contactform" action="{{ url('track') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-process"></div>

                        <div class="col-sm-12 col-md-9">
                            <label for="order">{{ trans('tracking.parcel') }} <small>*</small></label>
                            <input type="text" id="order" name="order" value="" class="sm-form-control required" style="margin-bottom:25px">
                        </div>
                        
                        <div class="col-sm-12 col-md-3" style="margin-top: 30px;">
                            <button class="button button-3d nomargin" type="submit" id="btn-track" name="btn-track" value="submit">{{ trans('tracking.parcel-btn') }}</button>
                        </div>

                    </form>
                </div>
                @if ($isExist == true)
                <div class="line"></div>
                    @if ($parcel->order_number == '')
                        <h3>Sorry, your parcel is not found. Please check your order number</h3>
                    @else
                <div id="info-detail" class="col-sm-12 col-md-9">
                    <h3>Informasi Parcel</h3>
                    <div class="table-responsive" id="track-result">
                        <table class="table cart">
                            <tbody>
                                <tr class="cart_item">
                                    <td class="notopborder cart-product-name">
                                        <strong>No Parcel</strong>
                                    </td>
                                    <td class="notopborder cart-product-name">
                                        {{-- Locker Name --}}
                                        <span class="amount" id="track-no">{{ $parcel->order_number }}</span>
                                    </td>
                                </tr>
                                <tr class="cart_item">
                                    <td class="cart-product-name">
                                        <strong>Locker</strong>
                                    </td>
                                    <td class="cart-product-name">
                                        {{-- Locker Name --}}
                                        <span class="amount" id="track-name">{{ $parcel->locker_name }}</span>
                                    </td>
                                </tr>
                                <tr class="cart_item">
                                    <td class="cart-product-name">
                                        <strong>Status</strong>
                                    </td>
                                    <td class="cart-product-name">
                                        {{-- Status --}}
                                        <span class="amount color lead" id="track-status"><strong>{{ $parcel->status }}</strong></span>
                                    </td>
                                </tr>
                                <tr class="cart_item">
                                    <td class="cart-product-name">
                                        <strong>Store Time</strong>
                                    </td>
                                    <td class="cart-product-name">
                                        <span class="amount" id="track-store">{{ $parcel->storetime }}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                    @endif
                @else
                @endif
                </div>
                <!-- .postcontent end -->
            </div>
        </div>
    </section>
@stop
@section('js')
<script>
// $('#info-detail').addClass('hide');
// Tracking Locker
$('#trackForm').on('submit', function(event) {
    event.preventDefault();
    var order_number = $('#order').val();
    return window.location.href = '{{ url('tracking') }}'+'/'+order_number;
    // $('#track-result').addClass('hide');
    // $.ajax({
    //         url: '{{ url('tracking') }}'+'/'+order_number,
    //         type: 'GET',
    //         dataType: 'json',
    //         data: $('#trackForm').serialize(),
    //         success: function(data) {
    //             console.log('{{ url('tracking') }}'+'/'+order_number);
    //             return window.location.href = '{{ url('tracking') }}'+'/'+order_number;
                /* if (data.isSuccess == true) {
                    $('#info-detail').show();
                    $('#info-detail').removeClass('hide');
                    data = data.data;
                    $('#trackForm').find('.form-process').fadeOut();
                    $('#track-no').html(data.order_number);
                    $('#track-name').html(data.locker_name);
                    $('#track-status').html(data.status);
                    $('#track-store').html(data.storetime);
                    // $('#track-result').removeClass('hide');
                } else {
                    $('#trackForm').find('.form-process').fadeOut();
                    // $('#form-track-error').html(data.errorMsg);
                    // $('#form-track-error').removeClass('hide');
                } */
        //     }
        // })
        // .done(function() {
        //     $('#trackForm').find('.form-process').fadeOut();
        //     console.log("success");
        // })
        // .fail(function() {
        //     $('#trackForm').find('.form-process').fadeOut();
        //     console.log("error");
        // })
        // .always(function() {
        //     $('#trackForm').find('.form-process').fadeOut();
        //     console.log("complete");
        // });
});
</script>
@stop