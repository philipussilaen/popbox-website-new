@extends('layout.main-article')
@section('content')
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="nobottommargin">
				<form class="nobottommargin" id="trackForm" action="{{ url('track') }}" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-process"></div>
                    <div class="">
                        <label for="name">{{ trans('tracking.parcel') }} <small>*</small></label>
                        <input type="text" id="order" name="order" value="" class="sm-form-control required" />
                    </div>
                    <br>
                    <div align="center">
                    	<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">{{ trans('tracking.parcel-btn') }}</button>
                    </div>
				</form>
			</div>
		</div>
	</div>
</section>
@if (isset($parcel))
	@if (!empty($parcel))
		<section id="result">
			<div class="content-wrap">
				<div class="container clearfix">
					<div class="nobottommargin">
						<div class="panel panel-default">
							<div class="panel-title">
								Parcel Status
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table cart">
										<tbody>
											<tr class="cart_item">
												<td class="notopborder cart-product-name">
													<strong>Locker</strong>
												</td>

												<td class="notopborder cart-product-name">
													<span class="amount">{{ $parcel->locker_name }}</span>
												</td>
											</tr>
											<tr class="cart_item">
												<td class="cart-product-name">
													<strong>Status</strong>
												</td>

												<td class="cart-product-name">
													<span class="amount color lead"><strong>{{ str_replace('_',' ', $parcel->status) }}</strong></span>
												</td>
											</tr>
											<tr class="cart_item">
												<td class="cart-product-name">
													<strong>Store Time</strong>
												</td>

												<td class="cart-product-name">
													<span class="amount">{{ date('l, j M Y H:i:s',strtotime($parcel->storetime)) }}</span>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	@endif
@endif
@stop

@section('js')
	<script type="text/javascript">
		jQuery(document).ready(function($) {
	    	$('#trackForm').submit(function(event) {
	    		var rules = Array();
                rules['order'] = 'required';
                var isValid = validate($(this),rules);
                if (isValid==false) {
		    		event.preventDefault();
                    return;
                }
	    	});
	    });
	</script>
@stop