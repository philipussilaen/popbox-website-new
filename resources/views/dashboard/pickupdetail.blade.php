@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
    <style type="text/css" media="print">
     	   @media print {
			    @page { margin: 0px 6px; }
  				body  { margin: 0px 6px; }
			}
     </style>
@stop
@section('content')
 <link rel="stylesheet" href="{{ asset('css/dashboardmerchant.css') }}" type="text/css" /> 
 <div class="row row-upload">
        <div class="col-md-2 left-content">
        <div class="row">
            <div class="col-md-12">
                 <a href="/dashboards/uploadpickup"><button class="button button-circle" type="button">BUAT ORDER BARU</button>
            </div>
        </div>
        <section id="menu">
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/myshop">
                        Toko Saya
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/listuploadpickup">
                        Data Order
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/panduan">
                        Panduan
                    </a>
                </div>
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/tracking">
                        Tracking                    
                    </a>                
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-9">  
        <div class="title-right-content">Detail Order</div>        
                        <div class="feature-box fbox-center fbox-border fbox-bg fbox-effect" style="margin-top: 0px; padding: 25px 30px 30px;text-align: left;">     
                            @if (empty($page))                       
                                <div class="alert alert-success">Order Anda berhasil disimpan, mohon cetak label dan tempelkan pada paket</div>
                            @endif
                            <label>Nama Konsumen :</label>
                            <label>{{$merchant->customer_name}}</label><br/>
                            <label>Nomor HP Konsumen :</label>
                            <label>{{$merchant->customer_phone}}</label><br/>
                            <label>LOKER :</label>
                            <label>{{$merchant->popbox_location}}</label><br/>
                            <label>EMAIL KONSUMEN :</label>
                            <label>{{$merchant->customer_email}}</label><br/>
                            <label>ALAMAT PENGAMBILAN :</label>
                            <label>{{$merchant->detail_items}}</label><br/>
                            <label>DETAIL ITEMS :</label>
                            <label>{{$merchant->customer_phone}}</label><br/>
                            <label>HARGA :</label>
                            <label>{{$merchant->price}}</label>
                            <div>                                
                                 <button class="button button-circle" onclick="printDiv('printableArea')"  type="button">Cetak Label</button>                                
                            </div>
                        </div>                        
                    
                    </section>
    </div>     
    <div class="col_full">                
                        <div class="row" id="printableArea" style="display: none" >
                            <div style="width: 400px; font-size: 16px;float:left;text-align: left;">
                                <div>
                                    <div style="font-size: 18px;font-weight: bold;">{{strtoupper($merchant->merchant_name)}}</div>
                                    Nama Order : <br/>
                                    <div style="font-size: 20px;font-weight: bold;">{{$merchant->order_number}}</div>
                                    Nama Konsumen : <br/>
                                    <div style="font-size: 18px;font-weight: bold;">{{$merchant->customer_name}}</div>
                                    No Hp penerima :<br/>
                                    <span style="font-size: 18px;font-weight: bold;" id="qr-phone"> {{$merchant->customer_phone}}</span><br/>
                                    Loker : <br/>
                                    <span style="font-size: 18px;font-weight: bold;" id="qr-loker">{{$merchant->popbox_location}}</span><br/>                                                                    
                                </div>
                            </div>
                            <div style="text-align: center;width: 150px;float: left;">                                
                                <div>                   
                                    <span style="font-size: 18px;font-weight: bold;">PopBox</span><br/>
                                </div>
                                <div style="width: 150px;padding: 10px 0px;">
                                    <img src="data:image/png;base64,{{$qrcode}}" alt="Image" width="100%">
                                </div>
                                <div style="width:150px;text-align: center">
                                    <span style="font-size: 16px;" id="spancode">{{$merchant->order_number}}</span><br/>                   
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <div style="width: 550px;font-size: 14px;font-weight: bold;text-align: center">
                                CS tlp. 021 2902 2537/8 atau email: logistic@popbox.asia
                            </div>
                        </div>
    </div>
</div>

{{-- Modal --}}


        
@stop

@section('js')
<!-- Select-Boxes Plugin --> 
<script type="text/javascript" src="{{ asset('js/components/select-boxes.js') }}"></script> 
<!-- Bootstrap Switch Plugin --> 
<script type="text/javascript" src="{{ asset('js/components/bs-switches.js') }}"></script> 

 <script type="text/javascript"> 
    jQuery(document).ready(function($) { 
        $('#city').select2(); 
        $(".bt-switch").bootstrapSwitch(); 
    }); 

    function printDiv(divName) {		
	    var printContents = document.getElementById(divName).innerHTML;
	    var originalContents = document.body.innerHTML;
	    document.body.innerHTML = printContents;
	    window.print();
	    document.body.innerHTML = originalContents;	 
	}
</script> 
@stop