@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
    <style type="text/css" media="print">
     	   @media print {
			    @page { margin: 0px 6px; }
  				body  { margin: 0px 6px; }
			}
     </style>
@stop
@section('content')
 <link rel="stylesheet" href="{{ asset('css/dashboardmerchant.css') }}" type="text/css" /> 

<div class="row row-upload">
    <div class="col-md-2 left-content">
        <div class="row">
            <div class="col-md-12">
                 <a href="/dashboards/uploadpickup"><button class="button button-circle" type="button">BUAT DATA ORDER</button>
            </div>
        </div>
        <section id="menu">
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/myshop">
                        Toko Saya
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/listuploadpickup">
                        Data Order
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/panduan">
                        Panduan
                    </a>
                </div>
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/tracking">
                        Tracking                    
                    </a>                
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-9">        
        <div class="title-right-content">LIST DATA ORDER</div>
        <section id="section-form">                                
              <div class="alert xleft" style="display: none;">
                    <ul><li></li></ul>
              </div>
                 <!--    <div class="row">
                        <div class="col-md-2">
                            <a href="/dashboards/listuploadpickup?status=1">
                                <button type="button" class="btn btn-primary">Need store ({{$count_need_store}})</button>
                             </a>
                        </div>
                        <div class="col-md-2">
                            <a href="/dashboards/listuploadpickup?status=2">
                                <button type="button" class="btn btn-primary">Need store ({{$count_in_store}})</button>
                            </a>
                        </div>
                        <div class="col-md-2">
                            <a href="/dashboards/listuploadpickup?status=3">
                                <button type="button" class="btn btn-primary">Customer taken ({{$count_in_store}})</button>
                            </a>                            
                        </div>
                        <div class="col-md-2">
                            <a href="/dashboards/listuploadpickup?status=4">
                                <button type="button" class="btn btn-primary">Customer taken ({{$count_in_store}})</button>
                            </a>                            
                        </div>
                        <div class="col-md-2">
                            <a href="/dashboards/listuploadpickup?status=5">
                                <button type="button" class="btn btn-primary">Operator taken ({{$count_in_store}})</button>
                            </a>                                                        
                        </div>                        
                        <div class="col-md-2">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Filter</button>
                        </div>                        
                    </div>      -->               
                    <div class="col_full" style="margin-top: 2%;">
                        <div class="feature-box fbox-center fbox-border fbox-bg fbox-effect" style="margin-top: 0px; padding: 25px 30px 30px;text-align: left;" id="printableArea">
                            <table class="table">
                                <thead>                                    
                                  <th>No</th>                                  
                                  <th>Order Number</th>
                                  <th>Customer Name</th>
                                  <th>Customer Phone</th>
                                  <th>Popbox Location</th>
                                  <th>Pickup Location</th>                                  
                                  <th>Customer Email</th>
                                  <th>Detail Items</th>
                                  <th>status</th>
                                  <th>Action</th>
                                </thead>
                                <tbody>
                                    <?php $i=1 ?>
                                     @foreach ($rowData as $row)
                                        <tr>
                                            <td>{{$i++}}</td>                                            
                                            <td>{{ $row->order_number }}</td>
                                            <td>{{ $row->customer_name }}</td>
                                            <td>{{ $row->customer_phone }}</td>
                                            <td>{{ $row->popbox_location }}</td>
                                            <td>{{ $row->pickup_location }}</td>
                                            <td>{{ $row->customer_email }}</td>
                                            <td>{{ $row->detail_items }}</td>
                                            <td>{{ $row->status }}</td>
                                            <td>
                                                <a href="/dashboards/pickupdetail?order_no={{$row->order_number}}&page=detail">Detail</a>
                                            </td>       
                                        </tr>                                                               
                                    @endforeach     
                                </tbody>
  
                            </table>
                        </div>
                    </div>                             
                               
            
        </section>
    </div>
</div>
 


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Masukkan filter</h4>
      </div>
      <form action="/dashboards/listuploadpickup" method="get">
          {{csrf_field()}}
          <div class="modal-body">        
              <div class="form-group">
                <label for="recipient-name" class="control-label">Order Number</label>
                <input type="text" name="order_number" class="form-control">
              </div>
              <div class="form-group">
                <label for="message-text" class="control-label">Customer Name:</label>
                <input type="text" name="customer_name" class="form-control">
              </div>
              <div class="form-group">
                <label for="message-text" class="control-label">Customer Phone:</label>
                <input type="text" name="phone" class="form-control">
              </div>
              <div class="form-group">
                <label for="message-text" class="control-label">Pickup Location:</label>
                <input type="text" name="pickup_location" class="form-control">
              </div>
              <div class="form-group">
                <label for="message-text" class="control-label">Customer Email:</label>
                <input type="text" name="customer_email" class="form-control">
              </div>
              <div class="form-group">
                <label for="message-text" class="control-label">Status:</label>
                <select name="status" class="form-control">
                    <option value="1">Need Store</option>
                    <option value="2">In Store</option>
                    <option value="3">Customer Taken</option>
                    <option value="4">Courier Taken</option>
                    <option value="5">Operator Taken</option>
                </select>                
              </div>            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">find</button>
          </div>
          </form>
    </div>
    
  </div>
</div>


        
@stop

@section('js')
<!-- Select-Boxes Plugin --> 
<script type="text/javascript" src="{{ asset('js/components/select-boxes.js') }}"></script> 
<!-- Bootstrap Switch Plugin --> 
<script type="text/javascript" src="{{ asset('js/components/bs-switches.js') }}"></script> 

 <script type="text/javascript"> 
    jQuery(document).ready(function($) { 
        @if (empty($merchant))
            $(".alert").addClass("alert-warning");            
            $(".alert ul li").html("Selamat! Anda sudah mendaftar partner PopBox.  Mohon lengkapi data anda dengan mengklik  <a href='/dashboards/myshop/edit'>di sini</a>");
            $(".alert").show();
        @elseif (empty($user->active))
            $(".alert").addClass("alert-warning");            
            $(".alert ul li").html("Cek email Anda dan segera lakukan aktivasi");
            $(".alert").show();
        @endif  

        $('#city').select2(); 
        $(".bt-switch").bootstrapSwitch(); 
    }); 

    function printDiv(divName) {		
	    var printContents = document.getElementById(divName).innerHTML;
	    var originalContents = document.body.innerHTML;
	    document.body.innerHTML = printContents;
	    window.print();
	    document.body.innerHTML = originalContents;	 
	}
</script> 
@stop