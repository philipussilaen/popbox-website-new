@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')
<link rel="stylesheet" href="{{ asset('css/dashboardmerchant.css') }}" type="text/css" /> 

<div class="row row-upload">
    <div class="col-md-2 left-content">
        <div class="row">
            <div class="col-md-12">
                 <a href="/dashboards/uploadpickup"><button class="button button-circle" type="button">BUAT ORDER BARU</button>
            </div>
        </div>
        <section id="menu">
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/myshop">
                        Toko Saya
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/listuploadpickup">
                        Data Order
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/panduan">
                        Panduan
                    </a>
                </div>
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/tracking">
                        Tracking                    
                    </a>                
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-9">
        <div class="title-right-content">Toko Saya</div>
        <section id="section-tab" class="right-content">
            <div class="row row-edit-merchant">
                <div class="col-md-10">
                    <div class="left"><img src="{{config('config.api')}}img/merchant/{{$merchant->picture}}" class="img-logo" /></div>
                    <div class="left title-shop-name">{{$merchant->merchant_name}}</div>
                </div>
                <div class="col-md-2">
                    <a href="/dashboards/myshop/edit"><div class="edit-btn">Edit</div></a>
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Prefix 
                </div>
                <div class="col-md-9">  
                    : {{$merchant->prefix}}
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Nama Pemilik Toko
                </div>
                <div class="col-md-9">  
                    : {{$merchant->owner_name}}
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Email
                </div>
                <div class="col-md-9"> 
                    : {{$merchant->email}} 
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Nomor Handphone
                </div>
                <div class="col-md-9">         
                    : {{$merchant->phone}}
                </div>
            </div>   
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Link Website
                </div>
                <div class="col-md-9">         
                    : {{$merchant->website}}
                </div>
            </div>

            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Kota
                </div>
                <div class="col-md-9">                    
                    : {{$merchant->city}}
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Detail Alamat
                </div>
                <div class="col-md-9">                    
                    : {{$merchant->address}}, {{$merchant->detail_address}}
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Waktu Pickup
                </div>
                <div class="col-md-9"> 
                    : {{$merchant->time_pickup}}                   
                </div>
            </div>

            @if ($merchant->time_pickup_new)
                <div class="row row-edit-merchant">
                    <div class="col-md-3">                        
                    </div>
                    <div class="col-md-9"> 
                        <div class="note-pickup-time">
                            Pickup time baru: {{$merchant->time_pickup_new}} <br/>
                            Berlaku mulai BESOK
                        </div>
                    </div>
                </div>
            @endif

    </div>
</div>


@extends('layout.syarat') 
@stop

@section('js')


 <script type="text/javascript"> 
    jQuery(document).ready(function() {         
        $("#syarat").click(function(){
            $(".bs-example-modal-lg").modal("show");                
        })        
    }); 
</script> 
@stop