@extends('layout.main-article')
@section('content')
<section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('img/clients/bg-merchant.jpg'); padding: 120px 0;" data-stellar-background-ratio="0.3">
	<div class="container clearfix">
		<h1>{{ trans('merchant.title') }}</h1>
		<span>{{ trans('merchant.sub') }}</span>
	</div>
</section>
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="nobottommargin">
				<ul class="clients-grid grid-4 nobottommargin clearfix">
                    @if (!empty($merchantList))
                        @foreach ($merchantList as $merchant)
							@if (empty($merchant->url))
								<li><img src="{{\App\Http\Helper\Helper::createImgUrl('merchant',$merchant->image)}}" alt="{{ $merchant->name }}"></li>
							@else
								<li><a href="{{ $merchant->url }}" target="_blank"><img src="{{\App\Http\Helper\Helper::createImgUrl('merchant',$merchant->image)}}" alt="{{ $merchant->name }}"></a></li>
							@endif
                        @endforeach
                    @endif
                    </ul>
			</div>
		</div>
	</div>	
</section>
@stop