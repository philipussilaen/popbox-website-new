@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="PopBox Asia" />
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.ico') }}">
    <title>Sunway PopBox - Neubodi</title>
</head>

 <style type="text/css">
     .active{
        border: 1px solid #1abc9c !important;
    }

    #section-welcome{
    	height: auto;
    }

 </style>
 <section id="slider" class="boxed-slider" style="display:none">
            <div class="container clearfix">
                <img src="{{ asset('img/merchant/banner-merchant-howto.jpg') }}" alt="Image">
            </div>
 </section>
 	<br>
 <section id="section-welcome">
    <div class="container clearfix">        
        <div class="col_full">
            <div class="center">
                <h3>Terms and Conditions</h3>
            </div>
            <div align="left">
                <h5>1. The campaign is organized by Neubodi Holdings Sdn. Bhd. and supported by Sunway PopBox Sdn. Bhd. It runs from 27 September to 4 November 2018.</h5>
            </div>
            <div align="left">
                <h5>2. The campaign is open to all females who are living in Malaysia.</h5>
            </div>
            <div align="left">
                <h5>
                    3. Multiple entries are allowed within the campaign period and you’re required to submit your personal details for each entry.
                </h5>
            </div>
            <div align="left">
                <h5>
                4. The Pre-loved items that you donate should be clean and well packaged in a non-transparent bag or a small rectangular box. Please check the compartments with S and M size before packaging. [S size: 34cm (L) x 10cm (H); M size)
                </h5>
            </div>
            <div align="left">
                <h5>
                5. The campaign offers the reward of 25% off e-voucher code to all the eligible donors after the campaign ends.
                </h5>
            </div>
            <div align="left">
                <h5>
                6. The e-voucher is not transferable, redeemable or exchangeable for cash or credit of any kind.
                </h5>
            </div>
            <div align="left">
                <h5>
                7. The organizer has the right to withhold the e-voucher if the donors do not provide accurate personal details.
                </h5>
            </div>
            <div align="left">
                <h5>
                8. The organizer reserves the right to postpone, change or cancel the campaign without prior notice.
                </h5>
            </div>
        </div>

        <div class="col_full" style="display:none">
            <div class="center">
                <h3>Panduan lengkap untuk manual order dengan upload dari excel</h3>
            </div>
            <div align="left">
                <h5>1. Download template email.</h5>
            </div>
            <div align="left">
                <h5>
                    2. Lengkapi informasi template sesuai judul kolom. (jangan sampe merubah format template) dan save
                </h5>
            </div>
            <div align="left">
                <h5>3. Klik upload untuk dan pilih file excel yang kita buat sebelumnya.</h5>
            </div>
            <div align="left">
                <h5>5. Tekan submit upload.</h5>
            </div>            
            <div align="left">
                <h5>6. Akan keluar informasi berhasi atau gagalnya upload dari excel.</h5>
            </div>            
        </div> 
    </div>
 </section>
      

        
@stop

