@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="PopBox Asia" />
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.ico') }}">
    <title>Sunway PopBox - Neubodi</title>
</head>

 <style type="text/css">
     .active{
        border: 1px solid #1abc9c !important;
    }
    #section-welcome{
    	height: auto;
    }
 </style>

 <section id="slider" class="boxed-slider" style="display:none">
    <div class="container clearfix">
        <img src="{{ asset('img/merchant/banner-merchant-howto.jpg') }}" alt="Image">
    </div>
 </section>

 <section id="section-welcome">
    <div class="container clearfix">
        <div class="col_full">
            <div class="center">
                <h1>Thank You</h1>
            </div>
            <div align="center">
                <h4>
                Thank you for submitting your details.<br><br>
                You’ll receive an email contain QR code and drop off guidance shortly.<br>
                Should you have further enquiries, <br><br>
                Please contact us at ‘PopBox Malaysia’ Facebook messenger.<br>
                </h4>
            </div>
        </div>
    </div>
 </section>
 
<div style="margin-top: 10px;" align="center">
    <a href="/csr" class="btn btn-info" role="button">CLOSE</a>
</div><br>

@stop

