@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')
 <style type="text/css">
     .active{
        border: 1px solid #1abc9c !important;
    }

    #section-welcome{
    	height: 300px;
    }

    .left{
        float: left;
    }

 </style>
 <section id="slider" class="boxed-slider">
    <div class="container clearfix">                
        <div>
            <a href="https://www.popbox.asia">
                <img src="{{ asset('img/merchant/register-merchant-success_01.png') }}" alt="Image">
            </a>
        </div>                
        <div class="left" style="width: 71%">
            <a href="https://www.popbox.asia">
                <img src="{{ asset('img/merchant/register-merchant-success_02.png') }}" alt="Image">
            </a>
        </div>          
        <div class="left" style="width: 10%">
            <div>
                <a href="https://www.popbox.asia">
                    <img src="{{ asset('img/merchant/register-merchant-success_03.png') }}" alt="Image">
                </a>
            </div>
            <div>
                <a href="https://www.facebook.com/pboxasia/?fref=ts&ref=br_tf">
                    <img src="{{ asset('img/merchant/register-merchant-success_05.png') }}" alt="Image">
                </a>
            </div>
        </div>                
         <div class="left"  style="width: 10%">
            <div>
                <a href="https://twitter.com/PopBox_Asia">
                    <img src="{{ asset('img/merchant/register-merchant-success_04.png') }}" alt="Image">
                </a>
            </div>
            <div>
                <a href="https://www.instagram.com/popbox_asia/?hl=id">
                    <img src="{{ asset('img/merchant/register-merchant-success_06.png') }}" alt="Image">
                </a>
            </div>
        </div>    
    </div>
 </section>
 	<br>
 <section id="section-welcome">
    <div class="container clearfix">
        <div class="center">
            <h3>Selamat! Anda telah terdaftar sebagai partner PopBox. Tim kami sedang melakukan verifikasi data 2-3 hari kerja. Mohon cek email Anda dan segera lakukan aktivasi.</h3>
        </div>
    </div>
 </section>
      

        
@stop

