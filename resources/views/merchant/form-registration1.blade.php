@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')

<link rel="stylesheet" href="{{ asset('css/dashboardmerchant.css') }}" type="text/css" /> 
<section id="slider" class="boxed-slider">
    <div class="container clearfix">
        <img src="{{ asset('img/merchant/mereg-banner01.png') }}" alt="Image">
    </div>
</section><br>
<section id="section-welcome">
    <div class="container clearfix">
        <div class="center">
            <h3>Lengkapi data berikut ini</h3>
        </div>
    </div>
</section>
<section id="section-form">
    <div class="container clearfix">
        <form method="post" action="{{ url('/user/create') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="col_full">
                    <div class="feature-box fbox-center fbox-border fbox-bg fbox-effect login-box" >
                        <div class="col-full">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger xleft">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (\Session::has('error'))
                                <div class="alert alert-danger xleft">
                                    <ul>
                                        <li>{{ \Session::get('error') }}</li>
                                    </ul>
                                </div>
                            @endif
                            @if (\Session::has('success'))
                                <div class="alert alert-success xleft">
                                    <ul>
                                        <li>{{ \Session::get('success') }}</li>
                                    </ul>
                                </div>
                            @endif
                        </div>
                        
                        <label class="lbl-register">Email : *</label>
                        <input type="text" name="email" id="email" class="sm-form-control" required="true" value="{{ old('email') }}">
                        <label class="lbl-register">Password : *</label>
                        <input type="password" name="password" id="password" class="sm-form-control" required="true" value="{{ old('password') }}">
                        <label class="lbl-register">Comfirm password : *</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" class="sm-form-control" required="true" value="{{ old('password_confirmation') }}">
                            <div align="center">
                                <button class="button button-circle" type="submit">Daftar Sekarang</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>



        
@stop

@section('js')
<!-- Select-Boxes Plugin --> 
<script type="text/javascript" src="{{ asset('js/components/select-boxes.js') }}"></script> 
<!-- Bootstrap Switch Plugin --> 
<script type="text/javascript" src="{{ asset('js/components/bs-switches.js') }}"></script> 

 <script type="text/javascript"> 
    jQuery(document).ready(function($) { 
        $('#city').select2(); 
        $(".bt-switch").bootstrapSwitch(); 
    }); 
</script> 
@stop