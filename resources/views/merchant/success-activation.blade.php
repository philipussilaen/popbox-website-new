@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')
 <style type="text/css">
     .active{
        border: 1px solid #1abc9c !important;
    }

    #section-welcome{
        height: auto;
    }


 </style>
 <section id="slider" class="boxed-slider">
            <div class="container clearfix">
                <img src="{{ asset('img/merchant/mereg-banner02.png') }}" alt="Image">
            </div>
 </section>
    <br>
 <section id="section-welcome">
    <div class="container clearfix">        
        <div class="col_full">
            <div class="center">
                    <h3>
                        Terima kasih telah memverifikasi email Anda. <br>
                        Tim partnership PopBox akan melakukan verifikasi data, 2-3 hari setelah Anda melakukan aktivasi akun. <br>
                        PopBox akan mengirimkan akses dan panduan untuk melakukan order pengiriman barang.
                    </h3>
            </div>
        </div>

        
    </div>
 </section>
      

        
@stop

